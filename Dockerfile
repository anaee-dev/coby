## GitLab Project : https://forgemia.inra.fr/anaee-dev/coby/

## Compile Coby with it's all dependencies 

########################
# Step : Compile Coby ##
########################

FROM maven:3.6.0-jdk-8-alpine AS compilation_stage

ARG user
ARG password

ADD ./. /tmp

WORKDIR /tmp

RUN apk add --update curl  && \
    apk add git            && \
    apk add ncurses  # for tput

RUN export TERM=xterm && ./01_coby_standard_builder.sh i=coby i=jaxy user=$user password=$password

##############################################
# Step : Create docker image with Coby LIBS ##
##############################################

FROM harisekhon/debian-java

MAINTAINER RYA ( rachid.yahiaoui@inra.fr )

USER root

RUN export TERM=xterm-256color 

RUN echo " ** Update Distrib "                  && \
    echo " "                                    && \
    echo "root:root_pass" | chpasswd            && \
    adduser --disabled-password --gecos '' coby && \
    echo "coby:coby_password" | chpasswd        && \
    apt-get -y update                           && \
    apt-get -y install procps                   && \
    apt-get -y install curl                     && \
    apt-get -y install lsof                     && \
    apt-get -y install maven                    && \
    apt-get -y install gawk                     && \
    apt-get -y install locales                  && \
    apt-get -y install postgresql               && \
    apt-get -y install xxd                      && \
    apt-get clean  

# COPY COBY SOURCES TO : /opt/coby-src
RUN mkdir /opt/coby-src
COPY ./.  /opt/coby-src
RUN chown -R coby:coby /opt/coby-src
RUN chmod -R 777 /opt/coby-src

# COPY COBY PIPELINE to : /opt/coby/
RUN mkdir /opt/coby
COPY --from=compilation_stage /tmp/coby_bin/. /opt/coby
RUN chown -R coby:coby /opt/coby/
RUN chmod -R 777 /opt/coby/


RUN echo "fr_FR.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen fr_FR.UTF-8                     && \
    dpkg-reconfigure locales                   && \
    usr/sbin/update-locale LANG=fr_FR.UTF-8

ENV LC_ALL fr_FR.UTF-8

USER coby 

WORKDIR /opt/coby

CMD /opt/coby/jaxy-server/run_server.sh  && tail -f /dev/null


# docker rmi -f ecoinfo/coby:1.5
# docker build --no-cache -t coby . 
# docker tag coby ecoinfo/coby:1.5 ; docker rmi -f coby 
# docker login -u ecoinfo
# docker push ecoinfo/coby:1.5
  
# docker run -it --net host                                                \
#            --name=coby   --rm                                            \
#            --memory-swappiness=0                                         \
#            --ulimit nproc=20000:50000                                    \
#            -v `pwd`/src/orchestrators/.:/opt/coby/pipeline/orchestrators \
#            -v `pwd`/src/SI:/opt/coby/pipeline/SI --rm ecoinfo/coby:1.5   \
#            /opt/coby/pipeline/orchestrators/synthesis_extractor_entry.sh
            

# docker run -it --net host                                                \
#            --name=coby                                                   \
#            --memory-swappiness=0                                         \
#            --ulimit nproc=20000:50000                                    \
#            -v `pwd`/src/orchestrators/.:/opt/coby/pipeline/orchestrators \
#            -v `pwd`/src/SI:/opt/coby/pipeline/SI --rm ecoinfo/coby:1.5  

