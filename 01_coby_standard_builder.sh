#!/bin/bash

 set -e 

 cd $( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

 GET_ABS_PATH() {
    # $1 : relative filename
    echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
 }

 while [[ "$#" > "0" ]] ; do
 
  case $1 in
  
      (*=*) KEY=${1%%=*}
      
            VALUE=${1#*=}
            
            case "$KEY" in
            
                ("i")   if   [ "$VALUE" == "coby" -o "$VALUE" == "COBY" ]; then
                                INSTALL_COBY="true"
                        elif [ "$VALUE" == "jaxy" -o "$VALUE" == "JAXY" ]; then
                                INSTALL_JAXY="true"
                        fi                        
                ;;        
                
                ("user")     FORGEMIA_USER=$VALUE               
                ;;
                ("password") FORGEMIA_PASSWORD=$VALUE               
                
            esac
      ;;
      help | HELP | H | h | -h | -H | -help | -HELP )  
            echo
            echo " Install Cmd Exp  : "
            echo "  1- Generate Only Coby Package ( without downloading and compiling projects ) :  $0     "
            echo "  2- install only coby              :  $0 i=coby                                         "
            echo "  3- install only jaxy              :  $0 i=jaxy                                         "
            echo "  4- install coby + jaxy            :  $0 i=coby i=jaxy                                  "
            echo "  5- install coby + jaxy Using Auth :  $0 i=coby i=jaxy user=theUser password=myPassword "
            echo
      	    exit ;
  esac
  
  shift
  
 done   

 # Prepare Installation folder 

 COBY_SOURCES="src"
 COBY_CORE="$COBY_SOURCES/core"

 COBY_BINARY_ROOT="coby_bin"
 COBY_BINARY="$COBY_BINARY_ROOT/pipeline"
 COBY_LIBS="$COBY_BINARY/libs"

 SCRIPTS_PATH_SOURCE="$COBY_CORE/pipeline/scripts"
 SCRIPTS_PATH_DESTINATION="$COBY_BINARY/scripts"

 rm    -rf $COBY_BINARY_ROOT
 mkdir     $COBY_BINARY_ROOT
 
 if [ -z "$INSTALL_COBY" -a -z "$INSTALL_JAXY" ] ; then 

   mkdir -p  $COBY_BINARY  
   mkdir     $COBY_LIBS
   mkdir     $COBY_LIBS/logs
   mkdir     $COBY_BINARY/work-tmp

   # Copy all scripts to the Binary Directory 
   tput setaf 2
   echo 
   echo " ###########################                             "
   echo " ##### Copy scripts core ###                             "
   echo 
   echo -e "\e[90m Location   : $SCRIPTS_PATH_SOURCE       \e[32m "
   echo -e "\e[90m Destination : $SCRIPTS_PATH_DESTINATION \e[32m "
   echo
   echo " ###########################                             "
   echo 
   
   sleep 0.1
   
   tput setaf 7

   cp -rf $SCRIPTS_PATH_SOURCE $SCRIPTS_PATH_DESTINATION

   echo -e "\e[90m Copied. Done. \e[32m "
   echo
   echo -e "\e[95m ################################################################################## \e[32m "
   echo -e "\e[95m ###  Coby Package successfully Deployed in the DIRECTORY  -->  $COBY_BINARY_ROOT   \e[32m "  
   echo -e "\e[95m ################################################################################## \e[32m "
   echo
   echo " Next Step :  Install libs "
   echo " Command : ./$SCRIPTS_PATH_DESTINATION/00_install_libs.sh help "
   
 fi

 if [ "$INSTALL_COBY" == "true" ] ; then 

   mkdir -p  $COBY_BINARY  
   mkdir     $COBY_LIBS
   mkdir     $COBY_LIBS/logs
   mkdir     $COBY_BINARY/work-tmp

   # Copy all scripts to the Binary Directory 
   tput setaf 2
   echo 
   echo " ######################################                  "
   echo " ######### Copy scripts core ##########                  "
   echo 
   echo -e "\e[90m Location   : $SCRIPTS_PATH_SOURCE       \e[32m "
   echo -e "\e[90m Destination : $SCRIPTS_PATH_DESTINATION \e[32m "
   echo
   echo " #####################################                   "
   echo 
   
   sleep 0.1
   
   tput setaf 7

   cp -rf $SCRIPTS_PATH_SOURCE $SCRIPTS_PATH_DESTINATION

   echo -e "\e[90m Copied. Done.     \e[32m "
   echo

   echo -e "\e[90m Run Instalation ..\e[32m "

    if [ ! -z "$FORGEMIA_USER"  ] ; then 

       ./$SCRIPTS_PATH_DESTINATION/00_install_libs.sh user=$FORGEMIA_USER password=$FORGEMIA_PASSWORD

    else 

       ./$SCRIPTS_PATH_DESTINATION/00_install_libs.sh 

    fi
   
 fi

 if [ "$INSTALL_JAXY" == "true" ] ; then   

   CURRENT_PATH=`pwd`
 
   # Prepare Installation folder 

   JAXY_JAVA_PROJECT_PATH="$COBY_CORE/jaxy-ws"

   # jaxy-quarkus Web service for COBY 

   # Dependencies 
   JAVA_PROJECT_JAXY_COBY="$JAXY_JAVA_PROJECT_PATH/jaxy-quarkus"
   JAVA_PROJECT_JAXY_CLIENT="$JAXY_JAVA_PROJECT_PATH/client/java-client"
 
   JAXY_SERVER_PATH="$COBY_BINARY_ROOT/jaxy-server"
   JAXY_SERVER_NAME="coby-quarkus-1.0-runner.jar"
   JAXY_SERVER_NAME_TARGET="coby-quarkus-1.0.jar"
   
   JAXY_CLIENT_PATH="$COBY_BINARY_ROOT/client/java-client"
   
   SCRIPT_CLIENT_SOURCE_PATH="$JAXY_JAVA_PROJECT_PATH/client/script-client"
   SCRIPT_CLIENT_TARGET_PATH="$COBY_BINARY_ROOT/client/"
   
   JAXY_CLIENT_NAME="jaxy-client.jar"

   mkdir -p  $JAXY_SERVER_PATH
   mkdir -p  $JAXY_CLIENT_PATH

   TMP_COMPILATION_FOLDER="$COBY_BINARY_ROOT/tmp"
 
   mkdir -p $TMP_COMPILATION_FOLDER/
  
 
   ############################
   ### Compile Web Serivces ###
   ############################

   # 4- Compile coby-quarkus 

   tput setaf 2
   echo 
   echo " #################################################### "
   echo " ### Install Jaxy-Quarkus web service for coby ###### "
   echo 
   echo -e "\e[90m Location :  $TMP_COMPILATION_FOLDER \e[32m  "
   echo
   echo " #################################################### "
   echo 
   
   sleep 0.1
   
   tput setaf 7

   cp -a $JAVA_PROJECT_JAXY_COBY/.  $TMP_COMPILATION_FOLDER/ 
   cd  $TMP_COMPILATION_FOLDER/ &&  mvn clean package && cd $CURRENT_PATH

   mv $TMP_COMPILATION_FOLDER/target/$JAXY_SERVER_NAME $JAXY_SERVER_PATH/$JAXY_SERVER_NAME_TARGET 
 
   cp -r $JAVA_PROJECT_JAXY_COBY/db-script              $JAXY_SERVER_PATH
   cp    $JAVA_PROJECT_JAXY_COBY/run_server.sh          $JAXY_SERVER_PATH
   cp    $JAVA_PROJECT_JAXY_COBY/coby_config.properties $JAXY_SERVER_PATH
   cp    $JAVA_PROJECT_JAXY_COBY/users.properties       $JAXY_SERVER_PATH

   echo 
    
   rm -rf $TMP_COMPILATION_FOLDER/{,.[!.],..?}*
    
   echo 
   echo " ###############################                      "
   echo " ##### Copy Script-Client ######                      "
   echo 
   echo -e "\e[90m Location :  $JAXY_JAVA_PROJECT_PATH/client/script-cient \e[32m  "
   echo
   echo " ##############################                       "
   echo 
   
   cp -r $SCRIPT_CLIENT_SOURCE_PATH $SCRIPT_CLIENT_TARGET_PATH

   tput setaf 2
   
   echo 
   echo " ###############################                      "
   echo " ##### Install Jaxy-Client #####                      "
   echo 
   echo -e "\e[90m Location :  $TMP_COMPILATION_FOLDER \e[32m  "
   echo
   echo " ##############################                       "
   echo 
   
   sleep 0.1
   
   tput setaf 7

   cp -a $JAVA_PROJECT_JAXY_CLIENT/. $TMP_COMPILATION_FOLDER/ 
   cd  $TMP_COMPILATION_FOLDER/  && mvn clean compile assembly:single && cd $CURRENT_PATH
   
   cp $TMP_COMPILATION_FOLDER/target/$JAXY_CLIENT_NAME $JAXY_CLIENT_PATH 
   rm -rf $TMP_COMPILATION_FOLDER/{,.[!.],..?}*

   rm -rf $TMP_COMPILATION_FOLDER
   
   echo 
    
 fi
  
if [ ! -z "$INSTALL_COBY" ] ; then   
     
     echo -e "\e[95m ####################################################################################### \e[32m "
     echo -e "\e[95m ###  Coby successfully INSTALLED & Deployed in the DIRECTORY  -->  $COBY_BINARY_ROOT    \e[32m "  
     echo -e "\e[95m ####################################################################################### \e[32m "
     
     tput setaf 2
     echo 
     echo " Before you start using COBY, be sure to provide your 'ORCHESTRATORS' and 'SI' folders "
     echo
     echo "   =>>  Example of 'ORCHESTRATORS' ( use_cases ) & 'SI' ( modelizations ) : ###        "
     echo "         --> SI              :   cp -r src/SI/ coby_bin/pipeline/                      "   
     echo "         --> ORCHESTRATORS   :   cp -r src/orchestrators/ coby_bin/pipeline/           "
     echo 
     echo "   =>>  Example of Running The 'synthesis_extractor' ORCHESTRATORS :  ###              "
     echo "         --> ./coby_bin/pipeline/orchestrators/synthesis_extractor_entry.sh            " 
     
     sleep 0.1
     
     tput setaf 7
 fi

 echo 
 echo " Done !"
 echo 

