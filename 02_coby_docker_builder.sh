#!/bin/bash

 set -e 

 while [[ "$#" > "0" ]] ; do
  
     case $1 in
     
         (*=*) KEY=${1%%=*}
         
               VALUE=${1#*=}
               
               case "$KEY" in
               
                    ("user")      FORGE_MIA_USER=$VALUE
                    ;;
                    ("password")  FORGE_MIA_PASSWORD=$VALUE
                    ;;
                    ("version")   VERSION=$VALUE
                esac
         ;;

         help | HELP | h | H )  echo
                                echo " Total Arguments :  2                "
                                echo 
                                echo "   user=         :  ForgeMia User    "
                                echo "   password=     :  ForgeMia Pasword "
                                 echo "  version=      :  COBY VERSION     "
                                echo 
                         exit
         ;;
         
     esac
     
     shift
     
  done   

 VERSION=${VERSION:-"untagged"}
  
 docker rmi -f ecoinfo/coby:$VERSION
  
 docker build --no-cache --build-arg user=$FORGE_MIA_USER          \
                         --build-arg password=$FORGE_MIA_PASSWORD -t coby .

 docker tag coby ecoinfo/coby:$VERSION
 
 docker tag coby ecoinfo/coby:latest
 
 docker rmi -f coby 

 # docker login -u ecoinfo
 # docker push ecoinfo/coby:1.6
  
 # docker run -it --net host                                                \
 #            --name=coby   --rm                                            \
 #            --memory-swappiness=0                                         \
 #            --ulimit nproc=20000:50000                                    \
 #            -v `pwd`/src/orchestrators/.:/opt/coby/pipeline/orchestrators \
 #            -v `pwd`/src/SI:/opt/coby/pipeline/SI --rm ecoinfo/coby:1.5   \
 #            /opt/coby/pipeline/orchestrators/synthesis_extractor_entry.sh
            

 # docker run -it --net host                                                \
 #            --name=coby                                                   \
 #            --memory-swappiness=0                                         \
 #            --ulimit nproc=20000:50000                                    \
 #            -v `pwd`/src/orchestrators/.:/opt/coby/pipeline/orchestrators \
 #            -v `pwd`/src/SI:/opt/coby/pipeline/SI --rm ecoinfo/coby:1.5  

