
# Git Commands :


## 1. Clone Coby Project ( include all submodules ) :

 
 ```
 
    git clone --recurse-submodules https://forgemia.inra.fr/anaee-dev/coby.git 
    
    cd coby


 ```

 
## 2. Git Commit - Push :

 
## +  **2.1 On Coby Project ( NOT Submodules ) :**
  
  
```
 
    git add -A
 
    git commit -m "my_commit_coby"
 
    git push origin master
 
```
 
## +  **2.2 On Submodules :**
 
 
- ## 2.2.1 [*Ontop-Materialize*](https://forgemia.inra.fr/anaee-dev/ontop-matarializer) :
   
   
+  ### **2.2.1.1 Before Modification** 

   
```
   cd src/core/pipeline/java_projects/ontop-matarializer/
   
   git checkout master

```

+ ### **2.2.1.2 After Modification**


```
   git add -A
 
   git commit -m "my_commit_OntopMaterializer"
 
   git push origin master

```

- ## 2.2.2 [*CoreseInfer*](https://forgemia.inra.fr/anaee-dev/coreseinfer) :
   
   
+  ### **2.2.2.1 Before Modification** 

   
```
   cd src/core/pipeline/java_projects/CoreseInfer/
   
   git checkout master

```

+ ### **2.2.2.2 After Modification**


```
   git add -A
 
   git commit -m "my_commit_coreseInfer"
 
   git push origin master

```

- ## 2.2.3 [*yedGen*](https://forgemia.inra.fr/anaee-dev/yedgen) :
   
   
+  ### **2.2.3.1 Before Modification** 

   
```
   cd src/core/pipeline/java_projects/yedGen/
   
   git checkout master

```

+ ### **2.2.3.2 After Modification**


```
   git add -A
 
   git commit -m "my_commit_yedGen"
 
   git push origin master

```

