# coby

### Clone the project :

   ` git clone -b coby-1.7 https://forgemia.inra.fr/anaee-dev/coby.git . `
  
  [![Watch the video](https://user-images.githubusercontent.com/7684497/36728847-7668397a-1bc2-11e8-9050-27858bb3b343.png)](https://www.youtube.com/embed/ruZTuK-ui2s)

----------------------------------------------------------------------------------

## 1. Coby standard_build ( 4 Options ) 

### if you want to :

#### 1.1 Consult help Install : 

 ` ./01_coby_standard_builder.sh help `
 
#### 1.2 Generate the Only Coby Package ( without downloading and compiling the java projects )

   `  ./01_coby_standard_builder.sh `
    
  - to **install libs**, just run the command : 
   
   `  ./coby_bin/pipeline/scripts/00_install_libs.sh `
   

#### 1.3 install only coby

   `  ./01_coby_standard_builder.sh -i coby `


#### 1.4 install only jaxy  ( secured service web )

   `  ./01_coby_standard_builder.sh i=jaxy user=userName password=Password `
   
#### 1.5 install  both coby & jaxy 

   `  ./01_coby_standard_builder.sh i=coby i=jaxy user=userName password=Password `
    
      
 <p><a href="https://www.youtube.com/embed/l08JIPcqgrI" rel="nofollow"><img src="https://user-images.githubusercontent.com/7684497/36728847-7668397a-1bc2-11e8-9050-27858bb3b343.png" alt="Intro" data-canonical-src="https://i.ytimg.com/vi/20KVZ0ZnCl4/mqdefault.jpg" style="max-width:10%;"></a></p>
  
   
#####  Coby standard_build Example :

-  Make sure Copy **"SI"** + **"orchestrators"** to the **coby_standard_bin directory** ( created by the previous script )

   `    cp -r src/SI/ coby_standard_bin/pipeline/ `
   
   `   cp -r src/orchestrators/ coby_standard_bin/pipeline/`
   
   
   `  ./coby_standard_bin/pipeline/orchestrators/synthesis_extractor_entry.sh `  

----------------------------------------------------------------------------------

### 2. Coby DOCKER - Orcherstrators ( located in src ) + SI modelization ( locate in src ) : 

Note : Run Coby Docker Image by providing SI Directory + orchestrators Directory


#####  2.1 Call Synthesis_Extractor Orcherstrator : 

  
   ```  
   
   docker run -d                                                            \
              --net host                                                    \
              --name=coby --rm                                              \
              --memory-swappiness=0                                         \
              --ulimit nproc=20000:50000                                    \
              -v `pwd`/src/orchestrators/.:/opt/coby/pipeline/orchestrators \
              -v `pwd`/src/SI:/opt/coby/pipeline/SI --rm ecoinfo/coby:1.0   \
              /opt/coby/pipeline/orchestrators/synthesis_extractor_entry.sh
   ```  

#####  2.2 Running coby Docker webService  : 
  
   ```  
  
    docker run -d                                                            \
               --net host                                                    \
               --name=coby                                                   \
               --memory-swappiness=0                                         \
               --ulimit nproc=20000:50000                                    \
               -v `pwd`/src/orchestrators/.:/opt/coby/pipeline/orchestrators \
               -v `pwd`/src/SI:/opt/coby/pipeline/SI --rm ecoinfo/coby:1.0  
               

    => Coby webService UI     : http://localhost:8181/index.xhtml
    => Auth                   : coby / coby 
    
    => Job Submitter EndPoint : http://localhost:8181/rest/resources/coby
        
   ```
   
----------------------------------------------------------------------------------

### 3. Example of how calling scripts that are provided by coby Using Coby standard_build :

#### - 06_gen_mapping.sh

-  Without CSV

   ` ./coby_standard_bin/pipeline/scripts/06_gen_mapping.sh input=$(pwd)/Demo/Demo_1/ output=$(pwd)/Demo/Demo_1/mapping.obda `
-  USING CSV 

   ` ./coby_standard_bin/pipeline/scripts/06_gen_mapping.sh input=$(pwd)/Demo/Demo_2/physicochimie/ output=$(pwd)/Demo/Demo_2/mapping.obda class="physico chimie" column="12" csvFile="$(pwd)/Demo/Demo_2/csv/semantic_si.csv"
 `

----------------------------------------------------------------------------------

## 4. Linked Projects :

   - **Ontop-Materializer** : https://forgemia.inra.fr/anaee-dev/ontop-matarializer
    
   - **CoreseInfer**        : https://forgemia.inra.fr/anaee-dev/coreseinfer
   
   - **yedGen**             : https://forgemia.inra.fr/anaee-dev/yedgen

   - **data-Riv**           : https://forgemia.inra.fr/anaee-dev/data-Riv

## 5. Use Case :

   - Formation_Anaee_Pipelines : https://forgemia.inra.fr/anaee-dev/formation_anaee_pipelines
