#!/bin/bash
 
  set -e 
    
  CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  cd $CURRENT_PATH
  ROOT_PATH="${CURRENT_PATH/}"
  PARENT_DIR="$(dirname "$ROOT_PATH")"
     
  EXIT() {
   if [ $PPID = 0 ] ; then exit ; fi
   # parent_script=`ps -ocommand= -p $PPID | awk -F/ '{print $NF}' | awk '{print $1}'` 
   parent_script=`ps -o comm= $PPID`
   if [ $parent_script = "bash" ] ; then
       echo; echo -e " \e[90m exited by : $0 \e[39m " ; echo
       exit 2
   else
       if [ $parent_script != "java" ] ; then        
          echo ; echo -e " \e[90m exited by : $0 \e[39m " ; echo
          kill -9 `ps --pid $$ -oppid=` ;         
          exit 2
       fi
       echo " Coby Exited "
       exit 2
   fi
  } 
    

  while [[ "$#" > "0" ]] ; do
  
     case $1 in
     
         (*=*) KEY=${1%%=*}
         
               VALUE=${1#*=}
               
               case "$KEY" in
               
                    ("owl")               OWL=$VALUE
                    ;;
                    ("obda")              OBDA=$VALUE
                    ;; 
                    ("type")              TYPE=$VALUE
                    ;;
                    ("output")            OUTPUT=$VALUE
                    ;;
                    ("page_size")         PAGESIZE=$VALUE
                    ;; 
                    ("fragment")          FRAGMENT=$VALUE
                    ;; 
                    ("flush_count")       FLUSHCOUNT=$VALUE
                    ;; 
                    ("xms")               XMS=$VALUE
                    ;; 
                    ("xmx")               XMX=$VALUE
                    ;; 
                    ("log_level")         LOG_LEVEL=$VALUE
                    ;;
                    ("csv_separator")     CSV_SEPARATOR=$VALUE
                    ;; 
                    ("csv_directory")     CSV_DIRECTORY=$VALUE
                    ;;
                    ("jdbc_url")          JDBC_URL=$VALUE
                    ;;  
                    ("jdbc_user")         JDBC_USER=$VALUE
                    ;;  
                    ("jdbc_password")     JDBC_PASSWORD=$VALUE
                    ;;
                    ("my_sql_version")    MY_SQL_VERSION=$VALUE
                    ;;
                    ("parallelism")       PARALLELISM=$VALUE                          # Number of parallel extraction 
                    ;;
                    ("e.peek")            ENTAILMENT_PEEK=$VALUE                      # Number of TTL Files Per Chunk which are processed by Corese
                    ;;
                    ("e.parallelism")     ENTAILMENT_PARALLELISM=$VALUE               # Number of Chunks which are processed on parallel by Corese
                    ;;
                    ("e.rule")            ENTAILMENT_RULE_LEVEL="-e.rule \"$VALUE\" " # OWL_RL_FULL , STD , OWL_RL , OWL_RL_LITE 
                    ;;
                    ("must_not_be_empty") MUST_NOT_BE_EMPTY_NODES="-must_not_be_empty \"$VALUE\" "
                    ;;
                    ("db_x")              DB_X="-db_x \"$VALUE\" "
                    ;;
  
               esac
	     ;;
	     
         help)  echo
                echo " Total Arguments      :  29                                                                      "
                echo 
                echo "   owl=               :  Path of the Ontology                                                    "
                echo "   obda=              :  OBDA File Path                                                          "
                echo "   type=              :  Type of Connection : CSV_Q , CSV_H2, CSV_PG, DB_PG                      "
                echo "   output=            :  Output ttl Files                                                        "
                echo "   page_size=         :  Number of rows which are extracted from DB. Ex : page_size=200000       "
                echo "   fragment=          :  Number of triples by file  "
                echo "   flush_count=       :  total line in memory before flush to file. Ex flush_count=500000        "
                echo "   parallelism=       :  Parallel Extraction                                                     "
               
                echo "   must_not_be_empty= :  total line in memory before flush to file. Ex flush_count=500000        "
               
                echo "   jdbc_url=          :  JDBC URL Connection - in case of ( DB_PG )                              "
                echo "   jdbc_user=         :  JDBC USER           - in case of ( DB_PG )                              "
                echo "   jdbc_password=     :  JDBC_PASSWORD       - in case of ( DB_PG )                              "

                echo "   log_level=         :  Display ( or not ) logs. Ex : log_level=ALL, log_level=OFF. Def : INFO  "
                echo "   debug=             :  Enable DEBUG ( Dev Mode )                                               "
                echo "   xms=               :  XMS. Ex : xms=500m                                                      "
                echo "   xmx=               :  XMX. Ex : xmx=2g                                                        "
                echo
                EXIT
         ;;
         
         debug)                  DEBUG=" -Xdebug -Xrunjdwp:transport=dt_socket,address=11555,server=y,suspend=y "  # Enable Debugging
         ;;
         e)                      ENTAILMENT="-e"                           # Enable Entailment ( Corese )
         ;;
         e.out_ontology)         ENTAILMENT_OUT_ONTOLOGY="-e.out_ontology" # Output Infered Ontology in TTL Format : Processed by Corese )
         ;;
         e.rm)                   ENTAILMENT_RM="-e.rm"                     # Remode TTL Files ( generated by Datariv ) After Inference
         ;;
         e.rm_on_load)           ENTAILMENT_RM_ON_LOAD="-e.rm_on_load"     # Remode TTL Files ( generated by Datariv ) After Load By Corese
         ;;
         index_columns)          INDEX_COLUMNS="-index_columns"            # Enable Indexing Columns ( in case of CSV_PG, PG_H2, CSV_MYSQL )
         ;;
         e.disable_cache_graph ) ENTAILMENT_DISABLE_CACHE_GRAH="-e.disable_cache_graph"
         ;;
         only_entailment)        ONLY_ENTAILMENT="-e.only_entailment"      # Generate Only Entailement
         ;;
         e.skolem)               ENTAILMENT_SKOLEM="-e.skolem"             # URIfy Blank nodes
         ;;
         e.ignore_blank_nodes)   ENTAILMENT_IGNORE_BLANK_NODES="-e.ignore_blank_nodes"
         ;;

     esac
     
     shift
     
  done   

  RELATIVE_PATHPATH_OWL="ontology/ontology.owl"
  RELATIVE_PATHPATH_OBDA="work-tmp/obda_tmp/mapping.obda"
  RELATIVE_PATHPATH_OUTPUT="output/02_ontop/dataRiv.ttl"
 
  DEFAULT_PAGE_SIZE="30000"
  DEFAULT_FRAGMENT="100000"
  DEFAULT_FLUSH_COUNT="20000"

  DEFAULT_XMS="8g"
  DEFAULT_XMX="8g"

  SELECTED_SI="conf/SELECTED_SI_INFO"
  
  if [ ! -f $SELECTED_SI ]  ; then
    echo
    echo -e "\e[91m Missing $SELECTED_SI ! \e[39m "
    echo -e "\e[91m You can use the command [[ ./scripts/01_use_si.sh si=WhichSI ]] to set the var WhichSI ! \e[39m "    
    EXIT   
  fi
 
  SI=$(head -1 $SELECTED_SI)        
 
  PARENT_SI="$(dirname "$SI")"
   
  # OWL=${OWL:-""}
  OWL=${OWL:-"$PARENT_SI/$RELATIVE_PATHPATH_OWL"}

  OBDA=${OBDA:-"$PARENT_DIR/$RELATIVE_PATHPATH_OBDA"}
  OUTPUT=${OUTPUT:-"$SI/$RELATIVE_PATHPATH_OUTPUT"}
  
  PAGESIZE=${PAGESIZE:-"$DEFAULT_PAGE_SIZE"}
  FRAGMENT=${FRAGMENT:-"$DEFAULT_FRAGMENT"}
  FLUSHCOUNT="-flush_count "${FLUSHCOUNT:-"$DEFAULT_FLUSH_COUNT"}
  XMS="-Xms"${XMS:-"$DEFAULT_XMS"}
  XMX="-Xmx"${XMX:-"$DEFAULT_XMX"}
  
  PARALLELISM=${PARALLELISM:-"1"}
  
  LOG_LEVEL=${LOG_LEVEL:-"ERROR"}

  CSV_SEPARATOR=${CSV_SEPARATOR:-";"}
  CSV_DIRECTORY=${CSV_DIRECTORY:-""}
  
  ENTAILMENT_PEEK="-e.peek "${ENTAILMENT_PEEK:-" 3 "}
  ENTAILMENT_PARALLELISM=${ENTAILMENT_PARALLELISM:-" 1 "}
    
  MY_SQL_VERSION="-my_sql_version "${MY_SQL_VERSION:-" v8_0_17 "}
  
  DEBUG=${DEBUG:-""}

  tput setaf 2
  
  echo 
  echo -e " ################################################################   "
  echo -e " #################### Info DataRiv ##############################   "
  echo -e " ----------------------------------------------------------------   "
  echo -e "\e[90m$0      \e[32m                                                "
  echo
  echo -e " ##  OWL                           : $OWL                           "
  echo -e " ##  OBDA                          : $OBDA                          "
  echo -e " ##  TYPE                          : $TYPE                          "
  echo -e " ##  OUTPUT                        : $OUTPUT                        "
  echo -e " ##  FLUSHCOUNT                    : $FLUSHCOUNT                    "
  echo -e " ##  PAGESIZE ( LIMIT )            : $PAGESIZE                      "
  echo -e " ##  FRAGMENT                      : $FRAGMENT                      "
  echo -e " ##  PARALLELISM                   : $PARALLELISM                   "
  echo -e " ##  MUST_NOT_BE_EMPTY_NODES       : $MUST_NOT_BE_EMPTY_NODES       "
  echo
  echo
  echo -e " ## CSV_SEPARATOR                  : $CSV_SEPARATOR                 "
  echo -e " ## CSV_DIRECTORY                  : $CSV_DIRECTORY                 "
  echo -e " ## INDEX_COLUMNS                  : $INDEX_COLUMNS                 "
  echo 
  echo -e " ##  DEBUG                         : $DEBUG                         "
  echo -e " ##  LOG_LEVEL                     : $LOG_LEVEL                     "
  echo  
  echo -e " ##  JDBC_URL                      : $JDBC_URL                      "
  echo -e " ##  JDBC_USER                     : $JDBC_USER                     "
  echo -e " ##  JDBC_PASSWORD                 : *****                          "
  echo
  echo -e " ##  MY_SQL_VERSION                : $MY_SQL_VERSION                "
  echo
  echo -e " ##  ENTAILMENT                    : $ENTAILMENT                    "
  echo -e " ##  ENTAILMENT_PARALLELISM        : $ENTAILMENT_PARALLELISM        "
  echo -e " ##  ENTAILMENT_OUT_ONTOLOGY       : $ENTAILMENT_OUT_ONTOLOGY       "
  echo -e " ##  ENTAILMENT_PEEK               : $ENTAILMENT_PEEK               "
  echo -e " ##  ENTAILMENT_PAR                : $ENTAILMENT_PAR                "
  echo -e " ##  ENTAILMENT_RM                 : $ENTAILMENT_RM                 "
  echo -e " ##  ENTAILMENT_RM_ON_LOAD         : $ENTAILMENT_RM_ON_LOAD         "
  echo -e " ##  ONLY_ENTAILMENT               : $ONLY_ENTAILMENT               "
  echo -e " ##  ENTAILMENT_IGNORE_BLANK_NODES : $ENTAILMENT_IGNORE_BLANK_NODES "
  echo -e " ##  ENTAILMENT_RULE_LEVEL         : $ENTAILMENT_RULE_LEVEL         "
  echo -e " ##  ENTAILMENT_DISABLE_CACHE_GRAH : $ENTAILMENT_DISABLE_CACHE_GRAH "
  echo 
  echo -e " ##  DB_X                          : $DB_X                          "
  echo
  echo
  echo -e " ##  XMS                           : $XMS                           "
  echo -e " ##  XMX                           : $XMX                           "
  echo
  echo -e " ################################################################   "
  echo 
  
  sleep 0.1
  
  tput setaf 7

  if [ ! -f $OWL -a "$ENTAILMENT" == "-e" ] ; then
     echo -e "\e[91m Missing OWL File [[ $OWL ]] ! \e[39m "
     EXIT
  fi

  if [ ! -f "$OBDA" -a  ! -f "$OWL" ]  ; then
     echo -e "\e[91m Missing OBDA or Ontology File [[ OBDA : $OBDA ]] [[ ONTOLOGY : $OWL ]] ! \e[39m "
     EXIT
  fi
 
  if [ ! -f "../libs/dataRiv.jar" ]  ; then
     echo -e "\e[91m dataRiv.jar Not found !! \e[39m "
     EXIT
  fi
 
  echo -e "\e[90m Starting Generation... \e[39m "
  echo
 
  COMMAND=" java -DcoreseLogLevel=OFF  
                 $DEBUG  $XMS  $XMX -jar ../libs/dataRiv.jar 
                 -owl   \"$OWL\"    
                 -obda  \"$OBDA\"   
                 -type  \"$TYPE\"   
                 -out   \"$OUTPUT\"                   
                 -log_level      \"$LOG_LEVEL\"        
                 -csv_separator  \"$CSV_SEPARATOR\"    
                 -csv_directory  \"$CSV_DIRECTORY\"    
                 -jdbc_url       \"$JDBC_URL\"    
                 -jdbc_user      \"$JDBC_USER\"    
                 -jdbc_password  \"$JDBC_PASSWORD\"    
                 -page_size   $PAGESIZE     
                 -fragment    $FRAGMENT   
                 -parallelism $PARALLELISM  
                 $FLUSHCOUNT    
                 $MUST_NOT_BE_EMPTY_NODES
                 $ENTAILMENT
                 $ENTAILMENT_PEEK
                 -e.parallelism $ENTAILMENT_PARALLELISM
                 $ENTAILMENT_RM  
                 $ENTAILMENT_RM_ON_LOAD 
                 $ENTAILMENT_OUT_ONTOLOGY
                 $ENTAILMENT_DISABLE_CACHE_GRAH 
                 $ENTAILMENT_RULE_LEVEL 
                 $ENTAILMENT_SKOLEM 
                 $INDEX_COLUMNS    
                 $ENTAILMENT_IGNORE_BLANK_NODES
                 $ONLY_ENTAILMENT
                 $MY_SQL_VERSION  
                 $DB_X  "
 
  eval $COMMAND

  exitValue=$? 

  if [ $exitValue != 0 ] ; then 
     echo 
     echo " CODE ERROR --> $exitValue "
     EXIT
  fi 

  sleep 0.1
  
  echo 
  
  OUT_FOLDER="$(dirname $(readlink -f $OUTPUT ))"
  
  if [ "$(ls -A $OUT_FOLDER )" ]; then
    echo -e "\e[36m Triples Generated in : $OUT_FOLDER \e[39m "
  else
    echo -e "\e[36m No Triples Generated in : $OUT_FOLDER \e[39m "
  fi
    
  echo
        
