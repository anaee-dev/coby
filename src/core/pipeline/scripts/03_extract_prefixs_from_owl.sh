#!/bin/bash

    ################################################################################
    ################################################################################
    ## Extract Prefixs from the Ontology located at ../SI/ontology/ontology.owl ####
    ###############  and output them in ../SI/ontology/prefix.txt"  ################
    ################################################################################
    ################################################################################

    EXIT() {
        if [ $PPID = 0 ] ; then exit ; fi
        # parent_script=`ps -ocommand= -p $PPID | awk -F/ '{print $NF}' | awk '{print $1}'`
        parent_script=`ps -o comm= $PPID`
        if [ $parent_script = "bash" ] ; then
            echo; echo -e " \e[90m exited by : $0 \e[39m " ; echo
            exit 2
        else
            echo ; echo -e " \e[90m exited by : $0 \e[39m " ; echo
            kill -9 `ps --pid $$ -oppid=`;
            exit 2
        fi    
    }
  
    includePrefixIfNotExists() {
       local prefixName="$1"
       local prefix="$2"
       local prefixFile="$3"
       
       prefixNumLine=$(grep -m1 -n "PREFIX $prefixName:" "$prefixFile" | awk -F  ":" '{print $1}')
            
       if [ -z "$prefixNumLine" ]; then 
               prefixNumLine="-1"
       fi
             
       if [ "$prefixNumLine" -lt 0 ]; then 
       
           echo "PREFIX $prefixName: $prefix" >> $prefixFile
       fi    
    }
    
    CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    cd $CURRENT_PATH
    
    while [[ "$#" > "0" ]] ; do
    
     case $1 in
     
         (*=*) KEY=${1%%=*}
         
               VALUE=${1#*=}
               
               case "$KEY" in
               
                    ("ontology")        ontology=$VALUE                    
                    ;;
                    ("prefixFile")      PrefixFile=$VALUE
                    ;;  
                    ("prefixFileExtra") PrefixFileExtra=$VALUE
                    ;;
               esac
         ;;
         help)  echo
                echo " Total Arguments : Two                    "
                echo
                echo "   ontology=     : Ontology file path     "
                echo "   prefixFile=   : output File of prefixs "
                echo
                EXIT
     esac
     shift
    done    
    
    ontology=${ontology:-"../SI/ontology/ontology.owl"}
    PrefixFile=${PrefixFile:-"../SI/ontology/prefix.txt"}
    PrefixFileExtra=${PrefixFileExtra:-"../SI/ontology/prefix_extra.txt"}
      
    tput setaf 2
    echo 
    echo -e " ################################################# "
    echo -e " ######### Info Prefixer ######################### "
    echo -e " ------------------------------------------------- "
    echo -e " \e[90m$0               \e[32m                     "
    echo
    echo -e " \e[91m Ontology          -->  $ontology \e[32m    "
    echo -e " \e[91m Out File          -->  $PrefixFile \e[32m  "
    if [ -f "$PrefixFileExtra" ]; then
    echo -e " \e[91m Prefix_File_Extra -->  $PrefixFileExtra    "
    fi
    echo
    echo -e " ################################################# "
    echo 
    sleep 0

    if [ ! -f $ontology ]  ; then
        echo
        echo -e "\e[91m Ontology $ontology not found ! \e[39m "
        EXIT
    fi
    
    grep '<rdf:RDF xmlns="' $ontology  | \
    sed -e 's/<rdf:RDF xmlns="//'      | \
    sed -e 's/"//g'                    | \
    sed 's/[[:blank:]]//g'             | \
       
    while read -r base ; do        
        echo "PREFIX : $base" > $PrefixFile              
    done
 
    grep '<owl:imports rdf:resource=' $ontology  | \
    sed -e 's/<owl:imports rdf:resource="//'     | \
    sed -e 's/"\/>//'                            | \
    sed 's/[[:blank:]]//g'                       | \
    
    if [ ! -f "$PrefixFileExtra" ]; then 

      while read -r ontology ; do
        
        name="${ontology##*/}"
        
        name_without_extension="$(cut -d'.' -f1 <<<"$name")"
        
        echo "PREFIX $name_without_extension: $ontology#"  >> $PrefixFile
        
        done

    else     
        
        echo ; echo " Extra_File_Prefix found at : $PrefixFileExtra" ; echo
        
        while read -r ontology ; do

            name="${ontology##*/}"
            
            name_without_extension="$(cut -d'.' -f1 <<<"$name")"

            extraPrefixNumLine=$(grep -m1 -n "PREFIX $name_without_extension:" "$PrefixFileExtra" | awk -F  ":" '{print $1}')
            
            if [ -z "$extraPrefixNumLine" ]; then 
                  extraPrefixNumLine="-1"
            fi
            
            if [ "$extraPrefixNumLine" -ge 0 ]; then 
            
                  extraPrefix=$(sed -n $extraPrefixNumLine'p' $PrefixFileExtra | sed -e 's/^[[:space:]]*//' )
                   
                  echo "$extraPrefix" >> $PrefixFile

            else
                  echo "PREFIX $name_without_extension: $ontology#"  >> $PrefixFile
            fi
            
        done

        while IFS= read -r lineInPrefixFileExtra; do
            
            ontoPrefixInFileExtra=$(echo "$lineInPrefixFileExtra" | cut -d':' -f1 | sed "s/PREFIX //" | sed -e 's/^[[:space:]]*//' )

            if [ -z "$ontoPrefixInFileExtra" ]; then
                 continue
            fi

            prefixNumLine=$(grep -m1 -n "$ontoPrefixInFileExtra:" "$PrefixFile" | awk -F  ":" '{print $1}')

            if [ -z "$prefixNumLine" ]; then 
                 prefixNumLine="-1"
            fi
                    
            if [ "$prefixNumLine" -lt 0 ]; then 
                 echo "$lineInPrefixFileExtra" >> $PrefixFile
            fi

        done < $PrefixFileExtra
    
    fi
   
    tput setaf 2
    echo 
    echo " Info : $PrefixFile created "
    echo     
    tput setaf 7

    ## Include some other Prefixs if not exists
    includePrefixIfNotExists "rdf"  "http://www.w3.org/1999/02/22-rdf-syntax-ns#" "$PrefixFile"
    includePrefixIfNotExists "rdfs" "http://www.w3.org/2000/01/rdf-schema#"       "$PrefixFile"
    includePrefixIfNotExists "xsd"  "http://www.w3.org/2001/XMLSchema#"           "$PrefixFile"
