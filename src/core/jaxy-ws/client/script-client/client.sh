#!/bin/bash                                       

#  ./client.sh host=http://localhost:8080/rest/resources login=admin password=admin service=time

: ' 

 #### Command Example ####
 
 Coby Synthesis : 

 
    ./client.sh host=http://localhost:8181/rest/resources  \
                service=coby_synthesis                     \
                login=admin                                \
                password=admin                             \
                params="FORET" 
 
      
 Clean output ( if no jobId then clean all Directories ) :
 
    ./client.sh host=http://localhost:8181/rest/resources  \
                service=coby_clean_output                  \
                login=admin                                \
                password=admin                             \
                job=job_01                                 ;
                     
 
 Coby Data Extrator :
 
    ./client.sh host=http://localhost:8181/rest/resources                        \
                service=coby                                                     \
                login=admin                                                      \
                password=admin                                                   \
                job=job_01                                                       \
                params=" SI = SOERE FORET & CLASS = meteo sh & year = 2013_2013" ;
                       
 
 Get logs :
 
    ./client.sh host=http://localhost:8181/rest/resources  \
                service=coby_logs                          \
                login=admin                                \
                password=admin                             ;
 

 Get Status :
 
    ./client.sh host=http://localhost:8181/rest/resources \
                service=coby_status                       \
                login=admin                               \
                password=admin    

 
 Coby Cancell All Jobs : 
  
    ./client.sh host=http://localhost:8181/rest/resources \
                service=coby_cancel_all                   \
                login=admin                               \
                password=admin                            ;
 
 
 Not Implemented Yet ! :
 
 ./client.sh host=http://localhost:8181/rest/resources service=coby                                  \
             login=admin   password=admin job=job-0                                                  \
             params=" SI = FORET & CLASS = meteo sh ; physico chimie                               & \
                      variable = http://www.anaee-france.fr/ontology/anaee-france_ontology#WaterPH & \
                      localSiteName = Léman ; TOTO                                                 & \
                      site = http://www.anaee-france.fr/ontology/anaee-france_ontology#GenevaLake  & \
                      year = 2011_2017 & localVariableName = Azote ammoniacal ; pH ; azote nitreux & \
                      SELECT_VARS = variable ; localVariableName ; anaeeVariableName ; anaeeUnitName \
                                   ; site ; infraName ;  year ; category ; unit ; anaeeSiteName  
 '

 urlencode() {
    # urlencode <string>
    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf "$c" ;;
            *) printf '%s' "$c" | xxd -p -c1 |
                   while read c; do printf '%%%s' "$c"; done ;;
        esac
    done
 }

 # urlencode() {
     # urlencode <string>
 #   old_lc_collate=$LC_COLLATE
 #   LC_COLLATE=C
    
 #   local length="${#1}"
 #   for (( i = 0; i < length; i++ )); do
 #       local c="${1:i:1}"
 #       case $c in
 #           [a-zA-Z0-9.~_-]) printf "$c" ;;
 #           *) printf '%%%02X' "'$c" ;;
 #       esac
 #   done
    
 #   LC_COLLATE=$old_lc_collate
 # } 
  
 urldecode() {
    # urldecode <string>
    local url_encoded="${1//+/ }"
    printf '%b' "${url_encoded//%/\\x}"
 } 

 while [[ "$#" > "0" ]] ; do                     
     case $1 in                                       
         (*=*) KEY=${1%%=*}                           
               VALUE=${1#*=}                          
                                                      
               case "$KEY" in                       
                                                      
                    ("host")       Host=$VALUE      
                    ;;                                
                    ("service")    Service=$VALUE   
                    ;;                                
                    ("login")      Login=$VALUE     
                    ;;                                
                    ("password")   Password=$VALUE  
                    ;;                                
                    ("job")        Job=$VALUE  # cpichot
                    ;;                                
                    ("params")     Params=$VALUE    
                    ;;                                
               esac 
         ;;
         help)  echo
                echo " Total Arguments : Six                                                    "
                echo 
                echo "   host       :  Hostname / IP                                            "
                echo "   login=     :  Login of the user. Ex : login=admin                      "
                echo "   password=  :  Extension of Graphs                                      "
                echo "   job=       :  job number (used to name the output directory)           " # cpichot
                echo "   service=   :  service name. Ex : service=InfoService                   "
                echo "   params=    :  Prams passed to the service. Ex :  params=SI = SOERE OLA "
                echo
                exit ;
     esac                                             
     shift                                            
 done                                              

 Host=${Host:-"http://www.si.com/coby?PARAMS"} 
 Login=${Login:-"LOGIN"}                        
 Password=${Password:-"PASSWORD"}               
 Job=${Job:-"JOB"}  # cpichot               
 Service=${Service:-"coby"}                  
 Params=${Params:-""}                               
 TimeStamp=$(date +%s)                             

  Hashed_Login="$Login"
  
  # Hashed_Password=` echo -n $Password | sha256sum  | cut -d ' ' -f 1`
  Hashed_Password=` echo -n $Password | md5sum  | cut -d ' ' -f 1` 
  Hashed_Password=` echo $Hashed_Password | sed 's/^0*//'`
  
  Hashed_TimeStamp="$TimeStamp"
  
  SIGNE=`echo -n $Hashed_Login$Hashed_Password$Hashed_TimeStamp | sha256sum  | cut -d ' ' -f 1 ` 
  SIGNE=` echo $SIGNE | sed 's/^0*//' ` 

  echo                                               
  echo -e " ###################################### "
  echo -e " ######## Info Client ################# "
  echo -e " -------------------------------------  "
  echo -e "\e[90m$0            \e[32m              "
  echo                                             
  echo -e " ##  Host     : $Host                   "
  echo -e " ##  Service  : $Service                "
  echo -e " ##  Params   : $Params                 "
  echo -e " ##  Login    : $Login                  "
  echo -e " ##  Password : $Password               "
  echo -e " ##  Job      : $Job                    "  # cpichot
  echo                                                
  echo  -e " ##################################### "
  echo                                               

  Params=$( urlencode  "$Params" )                 
  
  # curl -k -H "accept: xml/plain"    \
  #         -H "cipher: AES_256_CBC " \
  #         -H "API-key-Token: $Login $TimeStamp $SIGNE" $Host/$Service?$Params
  
  curl -k -H "accept: xml/plain"    \
          -H "cipher: AES_256_CBC " \
          -H "API-key-Token: $Login $TimeStamp $SIGNE $Job" $Host/$Service?$Params  #cpichot

