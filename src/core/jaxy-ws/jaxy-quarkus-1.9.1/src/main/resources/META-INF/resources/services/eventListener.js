 class EventListenerService {

     static serviceConnection

     constructor() {
         EventListenerService.serviceConnection = new ServiceConnection()
         this.connectedCallback()
     }

     connectedCallback() {

         window.addEventListener("authentication", function(event) {

             this.login     = event.detail.login
             this.password  = event.detail.password

             this.endpoint = event.detail.endpoint;

             if (EventListenerService.serviceConnection == null) EventListenerService.serviceConnection = new ServiceConnection()

             EventListenerService.serviceConnection.connect( endpoint, login, password )

             var connexion = "" 

             var tryConn = () => {

                 return new Promise((resolve, reject) => {

                     const intervalId = setInterval(() => {

                         if ( EventListenerService.serviceConnection.getConnectionState() === "OK" ) {
                             
                             clearInterval(intervalId)
                             console.log( "OK_AUTHENTICATION" )
                             resolve( "OK" )
                         } else if (EventListenerService.serviceConnection.getConnectionState() === "KO") {
                             console.log("KO AUTHENTICATION")
                             clearInterval(intervalId)
                             resolve("KO")
                         }
                     }, 50 )
                 })
             };


             tryConn().then(connState => {

                 console.log(" Connection State : " + connState )

                 if (connState == "OK") {

                     window.dispatchEvent(new Event("clear-logs", {
                         bubbles:  true ,
                         composed: true
                     }));

                     console.log( "Yeaah Connected ! " )

                     document.getElementsByTagName("coby-auth")[0].shadowRoot
                             .getElementById("authentication-bar")
                             .getElementsByTagName("tr")[1]
                             .getElementsByTagName("th")[0].innerHTML = "Connected as [ " + event.detail.login + " ]"

                     document.getElementsByTagName("coby-auth")[0]
                             .shadowRoot.getElementById("authentication-bar")
                             .getElementsByTagName("tr")[0].style.display = "none"

                     document.getElementsByTagName("coby-auth")[0]
                             .shadowRoot.getElementById("connected-bar").style.display = "block";

                     Swal.fire({
                         position: 'center'              ,
                         type:     'success'             ,
                         title: 'Signed in Successfully' ,
                         showConfirmButton: false        ,
                         timer: 2500
                     } ) ;


                 } else {

                     console.log( "Connection Failed !!" )
                     EventListenerService.serviceConnection = null

                     window.dispatchEvent(new CustomEvent("open-tab", {
                         bubbles:  true,
                         composed: true,
                         detail: {
                             buttonID:  "buttonLogsId",
                             contentID: "contentLogId"
                         }
                     }));
                 }

             });

         });

         window.addEventListener(onJobsEvent, function(event) {

             console.log('Clicked Jobs !!')

             if (EventListenerService.serviceConnection == null) {

                 Swal.fire( "Not Already Connected !! "    ,
                            'Missing_Connection_Exception' ,
                            'error'
                 );
                 return ;
             }

             EventListenerService.serviceConnection.get_jobs()
         });


         window.addEventListener(onLogsEvent, function(event)    {

             console.log('Clicked Logs !!')

             if (EventListenerService.serviceConnection == null) {

                 Swal.fire( "Not Already Connected !! "    ,
                           'Missing_Connection_Exception' ,
                           'error'
                 ) ;
                 return ;
             }

             EventListenerService.serviceConnection.get_logs()
         });

         window.addEventListener(onClearLogsEvent, function(event) {

             window.dispatchEvent(new CustomEvent(onClearLogs, {
                 bubbles:  true,
                 composed: true
             }));
         });

         window.addEventListener(onClearJobsEvent, function(event) {

             window.dispatchEvent(new CustomEvent(onClearJobs, {
                 bubbles:  true,
                 composed: true
             }));
         });

         window.addEventListener("coby-eventType-BuilderMode", function(event) {

             var mode = event.detail.message
             
             var visibleContainer = document.getElementsByClassName('container')[0].offsetWidth > 0 &&
                                    document.getElementsByClassName('container')[0].offsetHeight > 0 ;

             if (visibleContainer) {

                 displayComponent('container', !visibleContainer, 0, true    )
                 displayComponent('builder'  , visibleContainer, 1000, false )

                 document.querySelector('article').style.gridTemplateColumns = "6fr"
                 document.querySelector('article').style.gridTemplateRows = "6fr"

                 document.getElementById("coby-mode").innerHTML = " Build Mode"

             } else {

                 displayComponent('builder', visibleContainer, 0, true       )
                 displayComponent('container', !visibleContainer, 1000, false)

                 document.querySelector('article').style.gridTemplateColumns = " 6fr 1fr" 
                 document.querySelector('article').style.gridTemplateRows = ".2fr auto" 

                 document.getElementById("coby-mode").innerHTML = " Pipeline Mode"
             }

         });

         window.addEventListener("onDisconnect", function(event) {

             Swal.fire({
                 position: 'center'                  ,
                 type:     'info'                    ,
                 title:    'Signed out Successfully' ,
                 showConfirmButton: false            ,
                 timer: 2500
             });
         });

         window.addEventListener("generate-example-submission-query", function(event) {

             var expQuerySubmission = " SI = SOERE OLA ; SOERE FORET & year = 2011_2017 & CLASS = meteo ; physico chimie & localSiteName = Léman & localVariableName = Azote ammoniacal ; pH ; azote nitreux"

             document.querySelector("#contentSubmissionJob > coby-job-submission")
                     .shadowRoot.querySelector("div.submitContainerId > coby-textarea")
                     .shadowRoot.querySelector("#_my_id").value = expQuerySubmission  ;
         });

         window.addEventListener("clear-logs-open-tab", function(event) {

             console.log("Open TabLogs on clear logs ! ")

             window.dispatchEvent(new CustomEvent("open-tab", {
                 bubbles: true,
                 composed: true,
                 detail: {
                     buttonID:  "buttonLogsId",
                     contentID: "contentLogId"
                 }
             }));

             window.dispatchEvent(new Event("clear-logs", {
                 bubbles:  true,
                 composed: true
             }));

         });

         window.addEventListener("close_logs", function(event) {

             window.dispatchEvent(new CustomEvent("open-tab", {
                 bubbles:  true ,
                 composed: true ,
                 detail: {
                     buttonID:  "buttonLogsId",
                     contentID: "contentLogId"
                 }
             }));

         });

         window.addEventListener("clear-jobs", function(event) {

             window.dispatchEvent(new CustomEvent("open-tab", {
                 bubbles:  true ,
                 composed: true ,
                 detail: {
                     buttonID:  "buttonJobsId",
                     contentID: "contentJobsId"
                 }
             }));

         });

         window.addEventListener("display-submission-extraction", function(event) {

             window.dispatchEvent(new CustomEvent("open-tab", {
                 bubbles:  true,
                 composed: true,
                 detail: {
                     buttonID:  "buttonSubmissionJobId",
                     contentID: "contentSubmissionJob"
                 }
             }));

         });
     }

     removeInstance(instance)     {
         alert("remove instance") ;
         instance = null          ;
     }
 }


 function load_js(script) {
     var head   = document.getElementsByTagName('head')[0]
     var script = document.createElement('script')
     script.src = script
     head.appendChild(script)
 }

 function displayComponent(clazz, visibleContainer, time, immediate) {

     if (visibleContainer) {
         for (let el of document.getElementsByClassName(clazz)) {
             if (immediate) {
                 el.style.display = ''
             } else {
                 fadeIn(el, time)
             }
         }
     } else {
         for (let el of document.getElementsByClassName(clazz)) {
             if (immediate) {
                 el.style.display = 'none'
             } else {
                 fadeOut(el, time)
             }
         }
     }

 }

 function fade(what, duration) {
     what.opct = 100
     what.ih = window.setInterval(function() {
         what.opct--
         if (what.opct) {
             what.MozOpacity   = what.opct / 100
             what.KhtmlOpacity = what.opct / 100
             what.filter       = "alpha(opacity=" + what.opct + ")"
             what.opacity      = what.opct / 100
         } else {
             window.clearInterval(what.ih)
             what.style.display = 'none'
         }
     }, 0.1 * duration);
 }

 function fadeIn(el, time) {
     el.style.opacity = 0
     el.style.display = ''
     var last = +new Date()
     var tick = function() {
         el.style.opacity = +el.style.opacity + (new Date() - last) / time
         last = +new Date()
         if (+el.style.opacity < 1) {
             (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16)
         }
     };

     tick();
 }


 function fadeOut(el, time) {
     el.style.opacity = 1
     var last         = 0
     var tick         = function() {
         el.style.opacity = +el.style.opacity - last
         last             = last + 0.001
         if (+el.style.opacity > 0) {
             (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16)
         } else if (+el.style.opacity <= 0) {
             el.style.display = 'none'
         }
     };

     tick()
 }

 function sleep(ms) {
     return new Promise(resolve => setTimeout(resolve, ms));
 }

 new EventListenerService()
 
