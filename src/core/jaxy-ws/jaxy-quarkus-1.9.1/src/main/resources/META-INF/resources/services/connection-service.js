class ServiceConnection {

    static instance 
    
    static maxLines = 100

    constructor()   {

        console.log("Service Connection" )

        if (ServiceConnection.instance)  {
            return ServiceConnection.instance
        }

        window.addEventListener("onDisconnect"       , event => this.on_disconnect(event)      )

        window.addEventListener("close_logs"         , event => this.close_logs(event)         )

        window.addEventListener("status"             , event => this.get_status(event)         )

        window.addEventListener("run-synthesis"      , event => this.run_synthesis(event)      )

        window.addEventListener("submit-extraction"  , event => this.submit_extraction(event)  )

        window.addEventListener("cancel-all-jobs"    , event => this.cancel_all_jobs(event)    )

        window.addEventListener("cancel-current-job" , event => this.cancel_current_job(event) )

        window.addEventListener("clean-output-ttl"   , event => this.clean_output_ttl(event)   )
        
        window.addEventListener ("max-lines-event"   , event => this.set_max_lines(event)      )

        window.addEventListener ("open-remote-file"  , event => this.open_remote_file(event)   )

        window.addEventListener ("update-remote-file"  , event => this._update_remote_file(event))
       
        window.addEventListener ("list-remote-files" , event => this.list_remote_files(event)  )
       
        window.addEventListener ("clear-localstorage" , event => this.clearLocalStorage(event) )
       
        window.addEventListener ("store-state-in-localstorage" , event => this.storeStateInLocalStorage(event)  )

        ServiceConnection.instance           = this

        ServiceConnection.CONNECTION_STATE   = ""

        ServiceConnection.reader             = null

    }

    getConnectionState() {
        return ServiceConnection.CONNECTION_STATE
    }

    setConnectionState(statConnection) {

        ServiceConnection.CONNECTION_STATE = statConnection
    }

    connect(endpoint, username, password) {

        ServiceConnection.CONNECTION_STATE = ""; // OK - KO

        this.endpoint = endpoint // "https://sse.now.sh" ;
        this.username = username
        this.password = password

        var myHeaders = new Headers()

        var timestamp = ~~(+new Date() / 1000)

        var token = getAuthHeader(username, password, timestamp)

        var splitedToken = token.split(" ")

        var authPath = splitedToken[0] + "/" + splitedToken[2] + "/" + splitedToken[1]


        myHeaders.append("API-key-Token", token)

        var myInit = {
            method: 'GET'      ,
            headers: myHeaders ,
            cache: 'default'
        };

        console.log(" Check Auth : " + endpoint + '/' + authPath )

        var myRequest = new Request(endpoint + '/' + authPath, myInit) 

        const decoder = new TextDecoder('utf-8')

        fetch(myRequest).then(function(response)     {

            console.log("response.status from server : " + response.status)

            var statusCode = +response.status

            if (response.status !== 200)     {

                response.text()
                        .then((response) =>  {

                                window.dispatchEvent(  new CustomEvent( onRecievedDataLogsEvent, {
                                                                        bubbles:  true,
                                                                        composed: true,
                                                                        detail: {
                                                                        message: "\nError ( " + statusCode + " ) " + response
                                                                        }
                                })) ;

                                new Audio('assets/sound/error.mp3').play()
                                
                                ServiceConnection.CONNECTION_STATE = "KO"
                                
                                throw response
                        } )

            } else {

                    ServiceConnection.CONNECTION_STATE = "OK"

                    new Audio('assets/sound/log-in.mp3').play()
                    
                    window.dispatchEvent(  new CustomEvent( "list-remote-files", {
                                                            bubbles: true ,
                                                            composed: true
                                        } ) ) ;

                    window.dispatchEvent(  new CustomEvent( "clear-localstorage", {
                                                            bubbles: true      ,
                                                            composed: true
                                        } ) ) ;

                    localStorage.removeItem('file-name')
            }             

        }).catch(function(err) { // fallback mechanism

            console.log("ERROR-Connextion: " + err )

            new Audio('assets/sound/error.mp3').play()
            
            window.dispatchEvent(  new CustomEvent( "open-tab", {
                                                    bubbles: true,
                                                    composed: true,
                                                    detail: {
                                                      buttonID: "buttonLogsId",
                                                      contentID: "contentLogId"
                                                    }
            }));
            
            /* Send data to textarea componenent wich is listening on the event $onRecievedDataLogsEvent */
            window.dispatchEvent(new CustomEvent(  onRecievedDataLogsEvent, {
                                                   bubbles: true,
                                                   composed: true,
                                                   detail: {
                                                       message: err
                                                   }
                                                }));

        });

    }

    on_disconnect() {

        console.log(" + on Disconnect " + " // CONNECTION_STATE == " + ServiceConnection.CONNECTION_STATE )

        new Audio('assets/sound/log-out.mp3').play()

        if (ServiceConnection.CONNECTION_STATE === "OK") {

            window.dispatchEvent(new Event("clear-logs", {
                bubbles:  true,
                composed: true
            }));

            ServiceConnection.CONNECTION_STATE = ""

            window.dispatchEvent(  new CustomEvent( "clear-localstorage", {
                                                    bubbles:  true      ,
                                                    composed: true
                                                   } ) ) ;

            localStorage.removeItem('file-name')

            window.dispatchEvent(  new CustomEvent( "disconnect"   , {
                                                    bubbles:  true ,
                                                    composed: true
            } ) ) ;

            console.log("See you soon !")

            this.close_logs()

        }
    }

    on_message(message) {

        const log = JSON.parse(message.body)

        /* Send data to textarea componenent wich is listening on the event $onRecievedDataLogsEvent */
        window.dispatchEvent(new CustomEvent(  onRecievedDataLogsEvent, {
                                               bubbles:  true,
                                               composed: true,
                                               detail: {
                                                 message: log.message
                                               }
                                             })
        ) ;

    }

    ///////////////////////////////
    //////////////////////////////

    get_logs() {

        if (ServiceConnection.CONNECTION_STATE !== "OK") {

            Swal.fire( "Not Already Connected !! "    ,
                       'Missing_Connection_Exception' ,
                       'warning'
            );

            return ;

        }

        window.dispatchEvent( new CustomEvent("open-tab", {
                              bubbles:  true,
                              composed: true,
                              detail: {
                                  buttonID:  "buttonLogsId",
                                  contentID: "contentLogId"
                              }
        }));


        if ( ServiceConnection.reader != null ) {

           Swal.fire ( { 
               title: 'Logs Already Opened ! ' ,
               text: "Would you like to close and re-open Logs ? ",
               icon: 'warning',
               showCancelButton: true,
               confirmButtonColor: '#3085d6',
               cancelButtonColor:  '#d33',
               confirmButtonText:  'Yes, Close and re-open Logs'
           }

           ).then((result) => {
               
               if (result.value) {
                   
                   console.log("Close and re-open logs")

                   this.close_logs()
                   
                   window.dispatchEvent( new Event( "clear-logs", { bubbles:  true,
                                                                    composed: true
                                                                  } 
                                                  ) 
                                       ) ;
                   
                   this.request_logs()
                    
                   return                     
               } 
           })
            
           return 
        }


        window.dispatchEvent(new Event("clear-logs", {
            bubbles:  true,
            composed: true
        }));

        this.request_logs()
    }

    
    request_logs() {
             
        var myHeaders = new Headers()

        var timestamp = ~~(+new Date() / 1000)

        var token = getAuthHeader(this.username, this.password, timestamp)
  
        myHeaders.append("API-key-Token", token)

        var myInit = {
            method: 'GET'      ,
            headers: myHeaders ,
            cache:  'default'
        };

        var myRequest = new Request(this.endpoint + '/coby_logs', myInit)

        var decoder = new TextDecoder('utf-8')

        fetch(myRequest).then(function(response) {

            if (response.status !== 200 ) {

                var statusCode = response.status 
               
                response.text().then((response) => {

                     window.dispatchEvent( new CustomEvent( onRecievedDataLogsEvent, {  bubbles:  true ,
                                                                                        composed: true ,
                                                                                        detail: {
                                                                                            message: "\n Error ( " + 
                                                                                                     statusCode    + " ) " + 
                                                                                                     response
                                                                                        }
                                                                                     } 
                                                          ) 
                                          )  ; 
             
                     new Audio('assets/sound/log-in.mp3').play()
                        
                     Swal.fire(  response                 ,
                                'Something went wrong ! ' ,
                                'error'
                               )
               }   )  ;       
               
               return ;
            }

            let timerInterval

            Swal.fire ( {
                       title: 'Log Connection - OK'        ,
                       html: '<h5>Retrieving Logs...</h5>' ,
                       timer: 500 ,
                       timerProgressBar: true ,
                       onBeforeOpen: () =>    {
                           Swal.showLoading()
                           timerInterval = setInterval(() => {
                           const content = Swal.getContent()
                           if (content) {
                               const b = content.querySelector('b')
                               if (b) {
                               b.textContent = Swal.getTimerLeft()
                               }
                           }
                           }, 100 )
                       } ,
                       onClose: () => {
                           clearInterval(timerInterval)
                       }
            }).then((result) => {
                   if (result.dismiss === Swal.DismissReason.timer) {
                       console.log('Retrieving Logs...')
                   }
            })
               
            ServiceConnection.reader = response.body.getReader()

            console.log(" ServiceConnection.reader - 0 " )

            function read() {

                return ServiceConnection.reader.read()
                
                       .then( ( { value, done } ) => {

                           var currentLines       = decoder.decode(value).split(/\r|\r\n|\n/ )
                           
                           var countCurrentLines  = 0
                           var deleteAllLines     = false 
                           
                           if( ServiceConnection.maxLines > 0 ) {                                
                           
                                   countCurrentLines  = currentLines.length
                                                                       
                                   if( countCurrentLines >= ServiceConnection.maxLines ) {
                                   
                                       // DELETE ALL LINES FROM TextAra
                                       deleteAllLines  = true
                                   }
                                   
                                   if ( countCurrentLines > ServiceConnection.maxLines ) {
                                   
                                       var toDel  = countCurrentLines - ServiceConnection.maxLines 

                                       for ( let i = 0; i <  toDel ; i++ ) {
                                           currentLines.shift() 
                                       }
                                   }
                           }
                           
                           if ( currentLines != " " ) {  // PING SERVER -> IGNORE 
                          
                               window.dispatchEvent(  new CustomEvent( onRecievedDataLogsEvent , 
                                                                       { bubbles:  true        ,
                                                                         composed: true        ,
                                                                         detail:   {
                                                                           message:        currentLines.join('\n') ,
                                                                           totalLines:     countCurrentLines       ,
                                                                           deleteAllLines: deleteAllLines                                                                        
                                                                       }
                               } ) ) ;
                               
                           }

                           if ( done ) {
                               console.log('Service Log Done')
                               // bacause The Current Connection is Closed,
                               // update READER
                            
                               if (ServiceConnection.reader != null) {
            
                                 try {
                                       ServiceConnection.reader.cancel()
                                       ServiceConnection.reader = null
                                       
                                       window.dispatchEvent( new CustomEvent( onRecievedDataLogsEvent, {  bubbles:  true,
                                                                                                          composed: true,
                                                                                                          detail: {
                                                                                                              message: " Logs Connection Closed !"
                                                                                                          }
                                                                                                        } ) 
                                                           ) ;
                                            
                                 } catch (err) {
                                 }
                              }
                   
                            return 
                           }
                        
                           read() ;
                        
                } ) ;
            }

            read() ;

        }).catch(function(err) {

            document.getElementById("logs-id").value = " " + err ;
        });
        
    }
         
         
    request_logs_sse() {
             
        var myHeaders = new Headers()

        var timestamp = ~~(+new Date() / 1000)

        var token = getAuthHeader(this.username, this.password, timestamp)
  
        myHeaders.append("API-key-Token", token)

        var myInit = {
            method: 'GET'      ,
            headers: myHeaders ,
            cache:  'default'
        };

      
        var eventSourceInitDict = { headers: { 'API-key-Token': token }} ;
       
        var events = new  window.EventSourcePolyfill( this.endpoint + '/coby_logs_sse',  eventSourceInitDict )  ;
     
        events.onopen = (e) => {
          console.log("Connection on LogService Opened") ;
          
        }
        
        events.onmessage = ( {data} ) => {
            

             window.dispatchEvent(  new CustomEvent( onRecievedDataLogsEvent ,  { bubbles: true  ,
                                                                                 composed: true  ,
                                                                                 detail:  {
                                                                                   message:        currentLines      ,
                                                                                   totalLines:     countCurrentLines ,
                                                                                   deleteAllLines: deleteAllLines                                                                        
                                                                                 }
               } ) ) ;
        
       }
      
       events.onerror = ( e ) =>  {
          alert("Error : " + e )  ;
          events.close()         ;
        }
    
    }
    
    
    close_logs() {

        if (ServiceConnection.CONNECTION_STATE !== "OK") {

            Swal.fire( "Not Already Connected !! "    ,
                       'Missing_Connection_Exception' ,
                       'warning'
            );

            return;

        }

        console.log(" ServiceConnection.reader - 1 " )

        if (ServiceConnection.reader != null) {
            
            try {
                ServiceConnection.reader.cancel()
                ServiceConnection.reader = null
                
            } catch (err) {

                window.dispatchEvent (
                    new CustomEvent  ( onRecievedDataLogsEvent, {
                                           bubbles: true,
                                           composed: true,
                                           detail: {
                                               message: err
                                           }
                                     }) )  ;
            }
        }

        window.dispatchEvent(new CustomEvent( onRecievedDataLogsEvent, {
                                              bubbles: true,
                                              composed: true,
                                              detail: {
                                                  message: "Logs Connection Closed !"
                                              }
                                            } ) ) ;

        Swal.fire( "Logs Connection Closed !" ,
                   'Ok'                       ,
                   'success'
        ) ;

    }


    get_jobs() {

        if (ServiceConnection.CONNECTION_STATE !== "OK") {

            Swal.fire( "Not Already Connected !! "    ,
                       'Missing_Connection_Exception' ,
                       'warning'
            );

            return;
        }

        console.log("Get Jobs - Function : get_jobs ")

        window.dispatchEvent(new Event( "clear-jobs", 
                                        {
                                           bubbles: true ,
                                           composed: true
                                        }
                             ) ) ;

        var myHeaders = new Headers()

        var timestamp = ~~(+new Date() / 1000)

        var token = getAuthHeader( this.username, this.password, timestamp )

        myHeaders.append("API-key-Token", token);

        var myInit = {
            method: 'GET'      ,
            headers: myHeaders ,
            cache:  'default'
        };

        var myRequest = new Request(this.endpoint + '/coby_jobs', myInit)

        fetch(myRequest).then(response => response.text())
            .then(function(response) {

                window.dispatchEvent( 
                        new CustomEvent( onRecievedDataJobsEvent, {
                                         bubbles: true,
                                         composed: true,
                                         detail: {
                                             message: response
                                         }
                }));
                
            }).catch(function(err) {

                window.dispatchEvent(new CustomEvent( onRecievedDataJobsEvent, {
                                                      bubbles:  true,
                                                      composed: true,
                                                      detail: {
                                                          message: err
                                                      }
                } ) ) ;
            });
    }

    get_status() {

        if (ServiceConnection.CONNECTION_STATE !== "OK") {

            Swal.fire( "Not Already Connected !! "    ,
                       'Missing_Connection_Exception' ,
                       'warning'
            );

            return;
        }

        console.log("Get Jobs - Function : get_status")

        var myHeaders = new Headers()

        var timestamp = ~~(+new Date() / 1000 )

        var token = getAuthHeader(this.username, this.password, timestamp)

        myHeaders.append("API-key-Token", token)

        var myInit = {
            method: 'GET'      ,
            headers: myHeaders ,
            cache:  'default'
        };


        var myRequest = new Request(this.endpoint + '/coby_status', myInit)

        fetch(myRequest).then(response => response.text())
                        .then(function(response) {

                               Swal.fire ( response         ,
                                           'Click to close' ,
                                           'success'
                               )
                               
         }).catch(function(err) {

                               Swal.fire (  err             ,
                                           'Click to close' ,
                                           'error'
                               )
         }) ;
    }

    run_synthesis() {

        if (ServiceConnection.CONNECTION_STATE !== "OK") {

            Swal.fire( "Not Already Connected !! "    ,
                       'Missing_Connection_Exception' ,
                       'warning'
            ) ;

            return ;

        }

        console.log("coby_synthesis..")

        var myHeaders = new Headers()

        var timestamp = ~~(+new Date() / 1000 )

        var token = getAuthHeader( this.username, this.password, timestamp )

        myHeaders.append("API-key-Token", token )

        var myInit = {
            method: 'GET'      ,
            headers: myHeaders ,
            cache: 'default'
        }

        var SI_NAME = document.querySelector("#article > coby-command-container > input").value

        var myRequest = new Request( this.endpoint + '/coby_synthesis?' + SI_NAME, myInit )

        fetch(myRequest).then ( response => {
            
             if ( response.status !== 200 ) {

                                          var statusCode = response.status 

                                          response.text().then( (res) => {
                                              
                                              
                                              window.dispatchEvent( new CustomEvent( onRecievedDataLogsEvent, { bubbles:  true ,
                                                                                                                composed: true ,
                                                                                                                detail: {
                                                                                                                 message: "\n Error ( "      + 
                                                                                                                          statusCode + " ) " + res
                                                                                                                }
                                                                                                              } ) 
                                                                   )  ;
                                 
                                              new Audio('assets/sound/error.mp3').play()
               
                                              Swal.fire(  res           ,
                                                          'Something went wrong ! ' ,
                                                          'error'
                                                       )
                                               
                                              return ;
                                              
                                          }) ;
                                          
                                           
            }
            
            return response.text()
            
        }).then ( function(response ) {
                                
                                   Swal.fire (  response        ,
                                               'Click to close' ,
                                               'success'
                                   )

                                   window.dispatchEvent( new CustomEvent( "open-tab", 
                                                                          {
                                                                             bubbles:  true,
                                                                             composed: true,
                                                                             detail: {
                                                                                 buttonID:  "buttonLogsId",
                                                                                 contentID: "contentLogId"
                                                                             }
                                                                          }  ) 
                                   ) ;

        }).catch(function(err) {

                new Audio('assets/sound/error.mp3').play()
          
                Swal.fire(  err             ,
                           'Click to close' ,
                           'error'
                )
        } ) ;
    }

    submit_extraction() {

        if (ServiceConnection.CONNECTION_STATE !== "OK") {

            Swal.fire( "Not Already Connected !! "    ,
                       'Missing_Connection_Exception' ,
                       'warning'
            );

            return ;

        }

        console.log("coby_Extraction..")

        var QUERY = document.querySelector("#contentSubmissionJob > coby-job-submission")
                            .shadowRoot.querySelector("div.submitContainerId > coby-textarea")
                            .shadowRoot.querySelector("#_my_id").value

        if (QUERY == null || QUERY == "") {


            Swal.fire( "Query Can't be Empty !"  ,
                       'Missing_Query_Exception' ,
                       'warning'
            );

            return;
        }

        console.log("Query :: " + this.endpoint + '/coby?' + encodeURIComponent( QUERY ) ) 

        var JOB_ID = document.querySelector("#contentSubmissionJob > coby-job-submission")
                             .shadowRoot.querySelector("div.submitContainerCommandId > input").value

        if (JOB_ID == null || JOB_ID == "")  {

            Swal.fire( "JOB_ID Not Provided !"    ,
                       'Missing_Job_Id_Exception' ,
                       'warning'
            );

            return;
        }

        var myHeaders = new Headers()

        var timestamp = ~~(+new Date() / 1000) 

        var token = getAuthHeader(this.username, this.password, timestamp)

        myHeaders.append("API-key-Token", token + " " + JOB_ID)

        var myInit = {
            method: 'GET'      ,
            headers: myHeaders ,
            cache: 'default'
        };

        var myRequest = new Request(this.endpoint + '/coby?' + encodeURIComponent(QUERY), myInit)

        fetch(myRequest).then(response => response.text())
                        .then(function(response)      {

                if (ServiceConnection.reader != null) {

                    window.dispatchEvent(new CustomEvent( "open-tab", {
                                                           bubbles:  true,
                                                           composed: true,
                                                           detail: {
                                                               buttonID:  "buttonLogsId",
                                                               contentID: "contentLogId"
                                                           }
                                                        }  ) 
                                         ) ;
                }

                Swal.fire(  response            ,
                           'JOB_ID : ' + JOB_ID ,
                           'success'
                ) ;

         }).catch(function(err) {

             Swal.fire (  err            ,
                         'Click to close',
                         'error'
             )
         } ) ;
    }


    cancel_current_job() {

        if (ServiceConnection.CONNECTION_STATE !== "OK") {

            Swal.fire( "Not Already Connected !! "   ,
                       'Missing_Connection_Exception',
                       'warning'
            );

            return;
        }

        Swal.fire ( { 
               title: 'Are you sure ? ' ,
               text: "This will cancel The Current Job !",
               icon: 'warning',
               showCancelButton: true,
               confirmButtonColor: '#3085d6',
               cancelButtonColor:  '#d33',
               confirmButtonText:  'Yes, cancel the current Job !'
           }).then((result) => {
               
           if (result.value) {
               
               console.log("Cancel Current Job - Function : cancel_current_job")

               var myHeaders = new Headers()

               var timestamp = ~~(+new Date() / 1000)

               var token = getAuthHeader(this.username, this.password, timestamp)

               myHeaders.append("API-key-Token", token);

               var myInit = {
                   method: 'GET'      ,
                   headers: myHeaders ,
                   cache: 'default'
               };

               var myRequest = new Request(this.endpoint + '/coby_cancel', myInit)

               fetch(myRequest).then(response => response.text())
                               .then(function(response)         {

                       Swal.fire  ( response         ,
                                    'Click to close' ,
                                    'success'
                       ) ;

               }).catch(function(err) {

                       Swal.fire ( err              ,
                                   'Click to close' ,
                                   'error' 
                       )
               } ) ;

               Swal.fire ( 'Cancel !'                    ,
                           'The Current Job Will be Canceled, Wait..' ,
                           'warning'
               )

           }
        })
    }

    cancel_all_jobs() {

        if (ServiceConnection.CONNECTION_STATE !== "OK") {

            Swal.fire( "Not Already Connected !! "    ,
                       'Missing_Connection_Exception' ,
                       'error'
            );

            return;

        }

        Swal.fire({
            
               title: 'Are you sure ?',
               text: "This wil cancel All Jobs !",
               icon: 'warning',
               showCancelButton: true       ,
               confirmButtonColor: '#3085d6',
               cancelButtonColor:  '#d33'   ,
               confirmButtonText:  'Yes, cancel all Jobs !'
           
           }).then((result) => {
               
               if (result.value) {

                   console.log("Cancel All Jobs - Function : cancel_all_jobs")

                   var myHeaders = new Headers()

                   var timestamp = ~~(+new Date() / 1000)

                   var token = getAuthHeader(this.username, this.password, timestamp)

                   myHeaders.append("API-key-Token", token )

                   var myInit = {
                       method: 'GET'      ,
                       headers: myHeaders ,
                       cache: 'default'
                   };
                   
                   var myRequest = new Request(this.endpoint + '/coby_cancel_all', myInit);

                   fetch(myRequest).then(response => response.text())
                   
                                   .then(  function(response) {

                                           Swal.fire(  response         ,
                                                       'Click to close' ,
                                                       'success'
                                       )

                   }).catch(function(err) {

                           Swal.fire (  err             ,
                                       'Click to close' ,
                                       'error' 
                                     )
                   } ) ;
                       
                   Swal.fire ( 'Cancel !'                          ,
                               'All Jobs Will be Canceled, Wait..' ,
                               'warning'
                   )
               }
      
           } )
    
    }

    clean_output_ttl() {

        if (ServiceConnection.CONNECTION_STATE !== "OK") {

            Swal.fire( "Not Already Connected !! "    ,
                       'Missing_Connection_Exception' ,
                       'warning'
            );

            return;
        }

        console.log("Clean All Output TTL - Function : clean_output_ttl")

        var myHeaders = new Headers()

        var timestamp = ~~(+new Date() / 1000)

        var token = getAuthHeader(this.username, this.password, timestamp)

        myHeaders.append("API-key-Token", token)

        var myInit =           {
            method: 'GET'      ,
            headers: myHeaders ,
            cache: 'default'
        };

        var myRequest = new Request(this.endpoint + '/coby_clean_output', myInit )

        fetch(myRequest).then(response => response.text())
        
                        .then(function(response) {

                               Swal.fire ( response         ,
                                           'Click to close' ,
                                           'success'
                               )

        }).catch(function(err) {

              Swal.fire (  err             ,
                          'Click to close' ,
                          'error'
              )
              
        } ) ;
    }
    
    set_max_lines( event )  {
        
       if( ServiceConnection.maxLines >= 0 ) {
       
           console.log( "Set max Lines to : " +  event.detail.message ) 
                        
           ServiceConnection.maxLines = event.detail.message 
       }
        
    }
    
    open_remote_file( event ) {
     
         var remote_file = event.detail.file 
         
         console.log( "File to Open : " +  remote_file ) 
                   
         if (ServiceConnection.CONNECTION_STATE !== "OK") {

            Swal.fire( "Not Already Connected !! "    ,
                       'Missing_Connection_Exception' ,
                       'warning'
            );

            return ;

        }

        console.log("Open Remote File " + remote_file )

        var myHeaders = new Headers()

        var timestamp = ~~(+new Date() / 1000) 

        var token = getAuthHeader(this.username, this.password, timestamp)

        myHeaders.append("API-key-Token", token   )         
        myHeaders.append("file-name", remote_file )

        var myInit = {
            method: 'GET'      ,
            headers: myHeaders ,
            cache: 'default'
        };

        var myRequest = new Request(this.endpoint + '/filemanager', myInit)

        fetch(myRequest).then(response => response.text())
                        .then(function(contentFile)      {

               window.dispatchEvent(new CustomEvent( "display-content-file", {
                                                           bubbles:  true,
                                                           composed: true,
                                                           detail: {
                                                               content_file: contentFile
                                                           }
                                                        }  ) 
                                         ) ;

               localStorage.setItem('file-name' , remote_file )
               // localStorage.setItem('coby_file' , contentFile )
               // if( remote_file.includes(".graphml")) {
               //    localStorage.setItem('coby_file' , contentFile )
               // }                

         }).catch(function(err) {

             Swal.fire (  err            ,
                         'Click to close',
                         'error'
             )

             localStorage.removeItem('file-name')

         } ) ;          
         
    }
    
    _update_remote_file( event ) {
     
       var fileName    = localStorage.getItem('file-name')
       var contentFile = ace.edit("ace-tree-view-editor" ).getValue()
       update_remote_file( this.username, this.password, this.endpoint, ServiceConnection.CONNECTION_STATE , 
                           fileName , contentFile )        
  }

    list_remote_files( event ) {

        if (ServiceConnection.CONNECTION_STATE !== "OK") {

            Swal.fire( "Not Already Connected !! "    ,
                       'Missing_Connection_Exception' ,
                       'warning'
            );

            return ;
        }

        var myHeaders = new Headers()

        var timestamp = ~~(+new Date() / 1000) 

        var token = getAuthHeader(this.username, this.password, timestamp)

        myHeaders.append("API-key-Token", token   )         

        var myInit = {
            method: 'GET'      ,
            headers: myHeaders ,
            cache: 'default'
        };

        var myRequest = new Request(this.endpoint + '/listfiles', myInit)

        fetch(myRequest).then(response => response.text())
                        .then(function(response)      {

               window.dispatchEvent(new CustomEvent( "display-list-files", {
                                                           bubbles:  true,
                                                           composed: true,
                                                           detail: {
                                                               list_files: response
                                                           }
                                                        }  ) 
                                         ) ;
                

         }).catch(function(err) {

             Swal.fire (  err            ,
                         'Click to close',
                         'error'
             )
         } ) ;          
         
    }

    storeStateInLocalStorage() {

       console.log(" Save State in LocalStorage "           )
       //localStorage.setItem('file-name',  "coby_graph_name" )
       localStorage.setItem('username' , this.username      )
       localStorage.setItem('password' , this.password      )
       localStorage.setItem('endpoint' , this.endpoint      )
       localStorage.setItem('connection-state',  ServiceConnection.CONNECTION_STATE )
    }   

    clearLocalStorage() {

       console.log(" Clear LocalStorage "         )
       //localStorage.removeItem('file-name'      )
       localStorage.removeItem('username'         )
       localStorage.removeItem('password'         )
       localStorage.removeItem('endpoint'         )
       localStorage.removeItem('connection-state' )
       localStorage.removeItem('coby_file'        ) 
    }   
}

function update_remote_file( userName, password, endpoint, connecState , fileName, fileContent ) {

 //  var userName = this.username ? this.username : localStorage.getItem('username' )
 //  var password = this.password ? this.password : localStorage.getItem('password' )
 //  var endpoint = this.endpoint ? this.endpoint : localStorage.getItem('endpoint' )
 //  var connecState =  ServiceConnection.CONNECTION_STATE ? ServiceConnection.CONNECTION_STATE : 
 //                     localStorage.getItem('connection-state' )

   if ( connecState !== "OK") {

       Swal.fire( "Not Already Connected !! "    ,
                  'Missing_Connection_Exception' ,
                  'warning'
       );

       return ;
   }

   var myHeaders = new Headers()

   var timestamp = ~~(+new Date() / 1000) 

   var token = getAuthHeader( userName , password, timestamp)

   myHeaders.append("API-key-Token", token       )
   myHeaders.append("file-name", fileName        )
   myHeaders.append("Content-Type", "text/plain" )

   var myInit = {
       method: 'POST'     ,
       headers: myHeaders ,
       cache: 'default'   ,
       body: fileContent
   };
   
   console.log(" Call Endpoint : " + endpoint + '/filemanager' )
   
   var myRequest = new Request( endpoint + '/filemanager', myInit)

   fetch(myRequest).then(response => response.text())
                   .then(function(response)         {
    
       // Swal.fire( response  )

       Swal.fire (  '<span style="font-size:smaller;overflow-y: auto !important;">' + response + '</span>' ,
                    'Click to close'                                           ,
                   'info'
       )

    }).catch(function(err) {

       Swal.fire ( "err"           ,
                  'Click to close' ,
                  'error'
       )

    } ) ;        
 }

function getAuthHeader(login, password, timeStamp) {

  return hash( login, password, "SHA2", "PLAIN", "MD5", "PLAIN", timeStamp )
}

