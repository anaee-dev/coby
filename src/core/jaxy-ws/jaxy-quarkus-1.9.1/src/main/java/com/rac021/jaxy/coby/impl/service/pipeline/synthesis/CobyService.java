
package com.rac021.jaxy.coby.impl.service.pipeline.synthesis ;

/**
 *
 * @author ryahiaoui
 */

import javax.ws.rs.GET ;
import javax.inject.Inject ;
import java.net.URLDecoder ;
import javax.ws.rs.Produces ;
import javax.inject.Singleton ;
import javax.ws.rs.HeaderParam ;
import java.util.logging.Level ;
import javax.ws.rs.core.UriInfo ;
import java.util.logging.Logger ;
import javax.ws.rs.core.Context ;
import javax.ws.rs.core.Response ;
import io.quarkus.runtime.Startup ;
import javax.annotation.PostConstruct ;
import com.rac021.jaxy.api.security.Policy ; 
import com.rac021.jaxy.api.security.Secured ;
import com.rac021.jaxy.coby.impl.utils.Writer ;
import com.rac021.jaxy.coby.impl.utils.TokenManager ;
import com.rac021.jaxy.api.qualifiers.ServiceRegistry ;
import com.rac021.jaxy.api.exceptions.BusinessException ;
import com.rac021.jaxy.coby.impl.scheduler.CobyScheduler ;
import static com.rac021.jaxy.api.logger.LoggerFactory.getLogger ;
import static com.rac021.jaxy.coby.impl.scheduler.CobyScheduler.JOBS ;
import com.rac021.jaxy.coby.impl.service.configuration.CobyConfiguration ;

/**
 *
 * @author R.Yahiaoui
 */

@ServiceRegistry("coby_synthesis")
@Secured(policy = Policy.CustomSignOn )
@Singleton
@Startup

public class CobyService    {
 
    @Inject 
    CobyConfiguration configuration ;

    private static final Logger LOGGER = getLogger() ;
    
    @PostConstruct
    public void init() {
    }

    public CobyService() {
    }
   
    @GET
    @Produces( { "xml/plain" , "json/plain" , "json/encrypted" , "xml/encrypted" } )
    public Response getResource ( @HeaderParam("API-key-Token") String token        ,
                                  @HeaderParam("keep")          String filterdIndex , 
                                  @Context UriInfo uriInfo ) throws BusinessException, Exception {    
       
        if( ! Writer.existFile( configuration.getCobyPipelineScript()) ) {
           
           LOGGER.log(Level.INFO, " The Script [{0}] + Not found ! ", configuration.getCobyPipelineSynthesisScript());
           
           return Response.status( Response.Status.OK )
                                           .entity( " \n Script Not found ! \n \n " +
                                                    " Path : " + configuration.getCobyPipelineScript() )
                                           .build() ;
        }
        
        String encodedQuery = uriInfo.getRequestUri().getQuery()   ;
        
        if( encodedQuery == null      || 
            encodedQuery.isEmpty()    ||
            encodedQuery.equals( "*")  )
            encodedQuery  = "ALL_SI"   ;  // ALL SI
        
        /*
        
        if( encodedQuery == null || encodedQuery.trim().isEmpty()) {
            
            return Response.status(Response.Status.OK)
                           .entity("\n Empty Queries not Accepted  \n  " )
                           .build() ;                 
        }
        
        */
        
        String login = TokenManager.getLogin(token) ;
        
        String decodedQuery = URLDecoder.decode( encodedQuery.trim(), "UTF-8" ) ;
        
        if( decodedQuery.contains("$")  || 
            decodedQuery.contains("@")  || 
            decodedQuery.contains("^")  || 
            decodedQuery.contains("'")  || 
            decodedQuery.contains("`") ) {

             return Response.status(Response.Status.OK)
                            .entity("\n  Please, modify your Request. \n "
                                   + " Some Sharacters Are Not Authorized \n "
                                   + " List --> $ @ ^ ' ` \n \n " )
                            .build() ;
        }
             
        String query = CobyScheduler.SYNTHESIS_OP  + " " +
                       login                       + " " +
                       CobyScheduler.JOB_SYNTHESIS + " " +
                       decodedQuery ;    //cpichot
          
        query = cleanQuery(query)   ;
         
        if( Writer.isFullStack(JOBS.size()) ) {
                     
                    return Response.status(Response.Status.OK)
                             .entity("\n Full Stack ( 50 Jobs waiting ). \n"
                                     + " Please re-try later & \n "
                                     + " Consider Emptying the Data Generation Directory \n "
                                     + " You can use the Service : [ coby_jobs ] to consult waiting jobs " )
                             .build() ;
         } else {
                 
             boolean releasedExecutorService = CobyScheduler.executorService.getActiveCount() == 0 ;
             
             LOGGER.log( Level.INFO, " Register Query : {0}", query ) ;
             
             JOBS.add( query ) ;
           
             if( releasedExecutorService )    {
               
              return Response.status(Response.Status.OK)
                             .entity(" \n JOB Launched. \n \n "
                                  + " You can see LOGS using the Service : [ coby_logs ] \n " )
                             .build() ;
             } else {
              
              if( JOBS.size() == 1 || JOBS.isEmpty() ) {
               
                    return Response.status(Response.Status.OK)
                                   .entity(" \n JOB Launched. \n \n "
                                        + " You can see LOGS using the Service : [ coby_logs ] \n " )
                                   .build() ;
              } else {
                      return Response.status ( Response.Status.OK )
                                     .entity(" \n Job Will Be Processed As Soon As Possible .. \n " )
                                     .build() ;
              }
            }         
        }
    }

    public static String cleanQuery ( String query ) {
        
      return query.replace("$", "") 
                  .replace("@", "") 
                  .replace("^", "") 
                  .replace("`", "") 
                  .replace("'", "") ;
    }
    
}

