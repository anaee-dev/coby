
package com.rac021.jaxy.coby.impl.service.semantic ;

import java.util.Set ;
import com.fasterxml.jackson.annotation.JsonIgnore ;
import com.fasterxml.jackson.annotation.JsonProperty ;

/**
 *
 * @author ryahiaoui
 */
 public class SemanticDescription {
       
    @JsonProperty( value = "SI", index = 0 )
    public  String  si       ;
    
    @JsonProperty(  value = "Class", index = 1 )
    public  String  clazz    ;

    @JsonProperty( value = "Variables", index = 2 )
    public  Set<Variable> variables   ;
    
    @JsonProperty(  value = "Select_Vars", index = 3 )
    public  Set<String>   selectVars  ;
    
    public SemanticDescription() {    }
    
    public SemanticDescription( String        si             ,
                                String        variable_class ,
                                Set<Variable> variables      , 
                                Set<String>   select_vars    )  {
        
        this.si         = si             ;
        this.clazz      = variable_class ;
        this.variables  = variables      ;
        this.selectVars = select_vars    ;
    }

    @JsonIgnore 
    public String getVariable_class() {
        return clazz ;
    }

    @Override
    public String toString() {
        return si + " - " + clazz + " - " + variables + " - " + selectVars ;
    }
 }
