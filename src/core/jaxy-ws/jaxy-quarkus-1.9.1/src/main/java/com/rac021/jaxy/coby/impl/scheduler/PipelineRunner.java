
package com.rac021.jaxy.coby.impl.scheduler ;

/**
 *
 * @author ryahiaoui
 */

import java.io.File ;
import java.util.Objects ;
import java.io.IOException ;
import java.util.concurrent.Callable ;
import com.rac021.jaxy.coby.impl.utils.Writer ;
import static java.util.Collections.singletonMap ;
import com.rac021.jaxy.coby.impl.utils.TokenManager ;
import com.rac021.jaxy.coby.impl.process_tree_killer.ProcessTree ;

 

public class PipelineRunner implements Callable {
 
    private final String COBY_PIPELINE_SCRIPT   ;
    private final String LOGGER_FILE            ;
    private final String LOGIN                  ;
    private final String JOB                    ; //cpichot jv 2020
    private final String QUERY                  ;
 
    private final int CRUNCHIFY_RUN_EVERY_M_SECONDS ;
    
    private boolean   synthesisExtraction   = false ;
    
    private Process   process                       ;
      
    public final static String ENV_PROCESS_BUILDER_KEY = "COBY_ORCHETRATOR" ;
    
    public final static String ENV_PROCESS_BUILDER_VAL = "KILLE_ME"         ;
   
    public PipelineRunner( String  coby_ipeline_script           ,
                           String  query                         , 
                           String  logger_file                   , 
                           int     crunchify_run_every_m_seconds ,
                           boolean synthesisExtraction  )  throws Exception {
            
        Objects.requireNonNull ( coby_ipeline_script ) ;
        Objects.requireNonNull ( query )               ;
        Objects.requireNonNull ( logger_file )         ;
            
        this.synthesisExtraction           = synthesisExtraction              ; 
                   
        this.COBY_PIPELINE_SCRIPT          = coby_ipeline_script              ;
        this.LOGIN                         = TokenManager.getLogin(query)     ;
        this.JOB                           = TokenManager.extractJob(query)   ; //cpichot
        this.QUERY                         = TokenManager.extractQuery(query) ;
        this.LOGGER_FILE                   = logger_file                      ;
        this.CRUNCHIFY_RUN_EVERY_M_SECONDS = crunchify_run_every_m_seconds    ;
    }
 
    @Override
    public Integer call() throws Exception {
			
        /* CALL PIPELINE --> */
               
        if( LOGIN == null || QUERY.isEmpty() ) return 21 ;
                
        ProcessBuilder builder     = null                ;
                 
        try {
               if ( Writer.existFile ( LOGGER_FILE  ) )  {
                    Writer.clearFile ( LOGGER_FILE  ) ;
               } else {
                    Writer.createFile(LOGGER_FILE )   ;
               }
                       
               String[] cmd  = null        ;
                       
               if( synthesisExtraction )   {
                           
                        cmd = new String[] { "bash"               ,
                                             COBY_PIPELINE_SCRIPT ,
                                             QUERY      //cpichot
                                           } ;
               } else {
                           
                        cmd = new String[] { "bash"               ,
                                             COBY_PIPELINE_SCRIPT ,
                                             LOGIN                ,
                                             QUERY                ,
                                             JOB                  , //cpichot
                                           } ;
               }
                       
               System.out.println(" *** CMD : " +  String.join(" ", cmd) ) ;

               builder = new ProcessBuilder( cmd )                ;
               builder.redirectOutput ( new File( LOGGER_FILE ) ) ;
               builder.redirectError  ( new File( LOGGER_FILE ) ) ;
                        
               builder.environment().put( ENV_PROCESS_BUILDER_KEY , 
                                          ENV_PROCESS_BUILDER_VAL ) ;
                        
               process = builder.start() ; 

               process.waitFor()         ;
               process.destroy()         ;
                        

        } catch ( IOException | InterruptedException x )       {
                   
            ProcessTree.get()
                       .killAll( singletonMap ( ENV_PROCESS_BUILDER_KEY , 
                                                ENV_PROCESS_BUILDER_VAL ) ) ;
            
            if( process != null && process.isAlive() )         {
                        

                process.destroy()                              ;
                process.destroyForcibly()                      ;
            }

            System.err.println(" PipelineRunner Interrupted ") ;
                    
            return 1                                           ;
        }
                
        return 0   ;
    }
}
