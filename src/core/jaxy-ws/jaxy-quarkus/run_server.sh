#!/bin/bash

 CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
 cd $CURRENT_PATH

 help() {
 
    echo
    echo " Total Arguments : One                       "
    echo 
    echo "   - debug       :  Start jaxy in debug mode "
    echo 
    echo " Sample Cmd : ./run.sh  debug                "
    echo
    exit ;
 }
 
 GET_ABS_PATH() {
   # $1 : relative filename
   echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
 }

 while [[ "$#" > "0" ]] ; do
 
  case $1 in
   
      debug)  DEBUG="-Xdebug -Xrunjdwp:transport=dt_socket,address=11555,server=y,suspend=y "
      
      ;; 
      
      help)  help
  esac
  
  shift
  
 done 

 ## POLICY AUTH :
 ##  - CustomSignOn ( DB_AUTH                    )
 ##  - FileSignOn   ( uses users.properties file )
 ##  - Public       ( no auth                    )
 ##  - SSO          ( keycloak auth              )
 POLICY="FileSignOn"
 
######################################
##### SERVER CONFIG ##################
######################################

 ## SERVER PORT ######################

 HTTP_PORT=${HTTP_PORT:-"8181"}
 
 HTTPS_PORT=${HTTPS_PORT:-"8585"}

 #####################################
 ## DB CONFIGURATION #################
 ## IF POLICY = CustomSignOn #########
 
 DB_URL_AUTHENTICATION=${DB_URL_AUTHENTICATION:-"jdbc:postgresql://localhost:2345/coby-db-auth"}
 
 DB_LOGIN=${DB_LOGIN:-"postgres"}      # coby_user
 
 DB_PASSWORD=${DB_PASSWORD:-"admin"}   # coby_password

 ####################################
 
 KEYSTORE_FILE="server.keystore"
 
 RAND_PASSWORD=`date +%s | sha256sum | base64 | head -c 32 ; echo`
 
 COBY_QUARKUS_JAR_NAME="coby-quarkus-1.0.jar"
 
 LIST_CPU="0-7" #  0 ( Only one core ) Or  0,1 ( core 0 and core 1 ) Or 0-4 ( for core 0 to core 4 ) 
 
 if [ -f $KEYSTORE_FILE ]; then 
   rm -f $KEYSTORE_FILE
 fi
 
 echo ; echo
 
 keytool -genkeypair   -storepass $RAND_PASSWORD -keyalg RSA    \
         -keysize 2048 -dname "CN=server"        -alias server  \
         -ext "SAN:c=DNS:localhost,IP:127.0.0.1" -keystore $KEYSTORE_FILE

 echo

######################################
 
######################################
##### COBY CONFIG ####################
######################################
 
 SCRIPT_PATH="../pipeline/scripts"

 SI_PATHS="../pipeline/SI"
 
 VALIDATED_SI_NAME="validated_semantic_si.csv"
 CSV_SEP=";"

 CSV_SEP=${CSV_SEP:-";"}
 
 INTRA_SEPARATORS=${INTRA_SEPARATORS:-" -intra_sep , "}
 
 COLUMNS_TO_VALIDATE=" -column 0 -column 1 -column 2 -column 4 -column 6 -column 7 -column 8 -column 10 "
  
 PREFIX_FILE=$( GET_ABS_PATH "$SI_PATHS/ontology/prefix.txt" )
 
 ONTOLOGY=$( GET_ABS_PATH "$SI_PATHS/ontology/ontology.owl" )
  
 if [ -f "$PREFIX_FILE" ] ; then 
      rm -f "$PREFIX_FILE"
 fi

 $SCRIPT_PATH/03_extract_prefixs_from_owl.sh ontology="$ONTOLOGY" prefixFile="$PREFIX_FILE"

 # Specify Default SI ( si=WhichSI )
 DEFAULT_SI=`ls $SI_PATHS --ignore "ontology"  --ignore "*.*" | head -1`
 
 $SCRIPT_PATH/01_use_si.sh  si="$DEFAULT_SI"
 
 ######################################
 ######################################
 
 for SI in `ls "$SI_PATHS" --ignore "ontology" --ignore "SI.txt" `;   do

   if [ -f "$SI_PATHS/$SI/csv/semantic_si.csv" ] ; then    
   
     CSV_FILE=$( GET_ABS_PATH "$SI_PATHS/$SI/csv/semantic_si.csv" )     
     VALIDATED_CSV_FILE_WITH_FULL_URI=$( GET_ABS_PATH "$SI_PATHS/$SI/csv/validated_semantic_si.csv" )
    
     if [ -f "$VALIDATED_CSV_FILE_WITH_FULL_URI" ] ; then 
       rm -f "$VALIDATED_CSV_FILE_WITH_FULL_URI"
     fi    
     
     $SCRIPT_PATH/04_corese_clone_valide_csv.sh csv="$CSV_FILE"                         \
                                                out="$VALIDATED_CSV_FILE_WITH_FULL_URI" \
                                                csv_sep="$CSV_SEP"                      \
                                                intra_separators="$INTRA_SEPARATORS"    \
                                                prefix_file="$PREFIX_FILE"              \
                                                owl="$ONTOLOGY"                         \
                                                columns="$COLUMNS_TO_VALIDATE"          \
                                                enable_full_uri 
                                               
     if [ ! -f "$VALIDATED_CSV_FILE_WITH_FULL_URI" ] ; then
        echo
        echo " Errors were detected in the validation of the csv files "
        echo
        exit 
     fi
     
   fi  
 
 done 
 
 jobs &>/dev/null
 
 sudo fuser -k $HTTP_PORT/tcp 
 sudo fuser -k $HTTPS_PORT/tcp 
 
 # quarkus.http.ssl.certificate.trust-store-password=password
 
 # http.http2=false ( can be used in java 11+ )
  
 echo ; echo " Authentication Policy : $POLICY " ; echo

 if [ "$POLICY" == "CustomSignOn" ]; then # DB_AUTH
 
     echo " Coby Authentication DataBase :$DB_URL_AUTHENTICATION " 
     echo

     java $DEBUG                                                            \
          -Dquarkus.http.http2=false                                        \
          -Dquarkus.datasource.url="$DB_URL_AUTHENTICATION"                 \
          -Dquarkus.datasource.username="$DB_LOGIN"                         \
          -Dquarkus.datasource.password="$DB_PASSWORD"                      \
          -Dquarkus.http.port=$HTTP_PORT                                    \
          -Dquarkus.http.ssl-port=$HTTPS_PORT                               \
          -Dquarkus.http.ssl.certificate.key-store-file=$KEYSTORE_FILE      \
          -Dquarkus.http.ssl.certificate.key-store-password=$RAND_PASSWORD  \
          -Dquarkus.http.insecure-requests=redirect                         \
          -jar $COBY_QUARKUS_JAR_NAME  &
     
 else  
  
     java $DEBUG                                                            \
          -Dquarkus.http.http2=false                                        \
          -Dquarkus.http.port=$HTTP_PORT                                    \
          -Dquarkus.http.ssl-port=$HTTPS_PORT                               \
          -Dquarkus.http.ssl.certificate.key-store-file=$KEYSTORE_FILE      \
          -Dquarkus.http.ssl.certificate.key-store-password=$RAND_PASSWORD  \
          -Dquarkus.http.insecure-requests=redirect                         \
          -DPolicy="$POLICY"                                                \
          -jar $COBY_QUARKUS_JAR_NAME  &
 fi
             
 new_job_started="$(jobs -n)"
 
 if [ -n "$new_job_started" ];then
    PID=$!
 else
    PID=
 fi
 
 taskset -cp $LIST_CPU $PID
 
 tail -f /dev/null
