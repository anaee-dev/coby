 class AceJs  extends HTMLElement {

   static maxLines  = 100 // 0 TO KEEP ALL LINES 
  
   constructor() { 
      super()
      this._id            = "ace-js-id" 
      this.eventType      = "coby-textarea-event-type" 
      this._style         = ""       
      this.eventTypeClear = "clear"       

    }

   connectedCallback() { 

     if( this.hasAttribute('_id') ) {
         this._id = this.getAttribute('_id')
     }
    
     if( this.hasAttribute('eventType') ) {
         this.eventType = this.getAttribute('eventType')
     }

     if( this.hasAttribute('eventTypeClear') ) {
          this.eventTypeClear = this.getAttribute('eventTypeClear')
     }
     
     if( this.hasAttribute('_style') ) {
         this._style += this.getAttribute('_style')
     }

     if( this.hasAttribute('placeholder') ) {
         this.placeholder = this.getAttribute('placeholder')
     }
   
     this.innerHTML =  `

            <div id="ace-js-div-id" style="width:100%; height: 100%; position:relative; overflow: overlay; background:gray; ">
            
                 <div class="totor" id="${this._id}" style="width: 100%;height: 100%;">\n</div>
                   
            </div>
            
     ` ;
   
     
     const container = this.querySelector( '#' + this._id )
    
     var   editor    = ace.edit( container )
    
     editor.setFontSize("17px")
      
     editor.setTheme("ace/theme/monokai")
     editor.getSession().setMode("ace/mode/javascript")
  
     editor.setOptions ( {
            readOnly: false ,
            highlightActiveLine: false,
            highlightGutterLine: false
     })
        
     
     var session = editor.session
     
     window.addEventListener( this.eventType ,function( event )  {
        
         // if( event.detail.message == " " ) return ; // PING SERVER -> IGNORE 
                             
         if( AceJs.maxLines == 0 ) {
             
               session.insert( {
                        row: session.getLength(), // or you can use "pos.row"
                        column: 0 ,
               }, "" + event.detail.message + " \n" ) 
               
               editor.gotoLine( session.getLength(), 0 , true )
         }
         
         else {
         
             var deleteAllLines = event.detail.deleteAllLines
             
             var totalNewLines  = event.detail.totalLines
        
             if ( deleteAllLines == true ) { // Delete All Lines from textArea 
                   
                  editor.setValue("")
                   
                  session.insert( {
                        row: session.getLength(), // or you can use "pos.row"
                        column: 0 ,
                  }, "" + event.detail.message + " \n" ) 
                        
                  editor.gotoLine( session.getLength(), 0 , true )
                 
             }  else if( AceJs.maxLines > 0 )   {

                  var totalLinesInAceJs         =  editor.getSelectionRange().end.row

                  var totalLinesToDellFromAceJs = ( totalLinesInAceJs + totalNewLines ) - AceJs.maxLines 

                  if ( totalLinesToDellFromAceJs > 0 ) {
                   
                       for ( let i = 0; i <  totalLinesToDellFromAceJs ; i++ ) {
                         
                           // editer.setValue("" , 0 )
                           
                           editor.session.replace   ( {
                              start: {row: 0, column: 0 },
                              end:   {row: 1, column: 0 }
                           }, "" )
                       }
                  }
                  
                  session.insert( {
                         row: session.getLength(), // or you can use "pos.row"
                         column: 0 ,
                  }, "" + event.detail.message + '\n' ) 
              
                  editor.gotoLine( session.getLength(), 0 , true )
             }
         }
         
     }   ) ;
     
   
     window.addEventListener( this.eventTypeClear /*onClearLogs */,function(event) {
       
         editor.setValue("")
     }) ;
          
     window.addEventListener ( "max-lines-event" , function( event )  {

        if( event.detail.message == 0 ) {
        
            console.log( "Set max Lines to : Unlimited " ) 
                         
            AceJs.maxLines = event.detail.message 
        }
        
        else if ( event.detail.message > 0 ) {
        
            console.log( "Set max Lines to : " +  event.detail.message ) 
                         
            AceJs.maxLines = event.detail.message 
        }
        
     }  ) ;

      
   }
 
 }

 customElements.define( "ace-js", AceJs )

