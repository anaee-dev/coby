 class Textarea  extends HTMLElement {

   static maxLines  = 100 // // 0 TO KEEP ALL LINES 
  
   constructor() { 
      super()
      this.attachShadow({ 'mode': 'open' })
      this._id            = "coby-textarea-id" 
      this._name          = "coby-textarea-name" 
      this.eventType      = "coby-textarea-event-type" 
      this.readonly       = "" 
      this._style         = ""       
      this.eventTypeClear = "clear" 
      this.placeholder    = " Undefined"

    }

   connectedCallback() { 

     if( this.hasAttribute('_id') ) {
         this._id = this.getAttribute('_id')
     }
     
     if( this.hasAttribute('_name') ) {
         this._name = this.getAttribute('_name')
     }

     if( this.hasAttribute('eventType') ) {
         this.eventType = this.getAttribute('eventType')
     }

      if( this.hasAttribute('eventTypeClear') ) {
          this.eventTypeClear = this.getAttribute('eventTypeClear')
     }
     
     if( this.hasAttribute('_style') ) {
         this._style += this.getAttribute('_style')
     }

     if( this.hasAttribute('readonly') ) {
	     this.readonly = "readonly" 
     }

     if( this.hasAttribute('placeholder') ) {
         this.placeholder = this.getAttribute('placeholder')
     }

     this.shadowRoot.innerHTML =  `

                <link href="components/base/textarea/coby-textarea.css" rel="stylesheet"/>

                <textarea class="textarea" 
                          name="${this._name}" 
                          id="${this._id}"
                          placeholder="${this.placeholder}" 
                          style="${this._style} "
                          ${this.readonly} 
                          autocomplete="off" 
                          autocorrect="off" 
                          autocapitalize="off" 
                          spellcheck="false"
                          rows="5" ></textarea>
     ` ;
   
     const textArea = this.shadowRoot.querySelector( 'textarea' )

     window.addEventListener( this.eventType ,function(event)   {

         var deleteAllLines = event.detail.deleteAllLines
         var totalNewLines  = event.detail.totalLines
         
         if( Textarea.maxLines == 0 ) {
              textArea.value += event.detail.message + '\n'
         }
         
         else {
         
             if ( deleteAllLines == true ) { // Delete All Lines from textArea 
                  textArea.value =  "" 
                  textArea.value = event.detail.message + '\n'
                 
             }  else if( Textarea.maxLines > 0 ) {
    
                  var currentLinesInTextArea = textArea.value.split(/\r|\r\n|\n/ )
                  var totalLinesInTextArea   = currentLinesInTextArea.length
                
                  var totalLinesToDellFromTextArea =  ( totalLinesInTextArea + totalNewLines ) - Textarea.maxLines 
    
                  if ( totalLinesToDellFromTextArea > 0 ) {
                   
                       for ( let i = 0; i <  totalLinesToDellFromTextArea ; i++ ) {
                             currentLinesInTextArea.shift() 
                       }
                  }
                                
                  textArea.value = currentLinesInTextArea.join('\n') + event.detail.message + '\n'
             }
         }
         
         textArea.scrollTop = textArea.scrollHeight
        
     } ) ;
     
   
     window.addEventListener( this.eventTypeClear /*onClearLogs */,function(event) {
        textArea.value = "" 
     }) ;
          
     window.addEventListener ( "max-lines-event" , function( event )  {

        if( event.detail.message == 0 ) {
        
            console.log( "Set max Lines to : Unlimited " ) 
                         
            Textarea.maxLines = event.detail.message 
        }
        
        else if ( event.detail.message > 0 ) {
        
            console.log( "Set max Lines to : " +  event.detail.message ) 
                         
            Textarea.maxLines = event.detail.message 
        }
        
     }) ;
      
      
   }
 
 }

 customElements.define("coby-textarea", Textarea )

