 class Tab  extends HTMLElement {

   constructor() { 
      super() 
     // this.attachShadow({ 'mode': 'open' }) 
      this.tabClass                      = "tab"
      this.tabContentClass               = "tabcontent"
      this.tabLinksClass                 = "tfablinks"
      this.cobyTextAreaComponentTag      = "coby-textarea"
      this.cobyJobSubmissionComponentTag = "coby-job-submission"
      this.cobyTextAreaComponentAce      = "ace-js"
      
      this.cobyTreeViewComponent         = "coby-treeview"
      
   }

   connectedCallback() { 
 
     this.innerHTML =  `
            
            <link href="components/composite/tab/coby-tab.css" rel="stylesheet" />          
            
            <!-- Tab links -->

            <div class="${this.tabClass}">
                <!-- <button class="${this.tabLinksClass}" id="defaultOpen"> Output </button>  -->   
            </div>

            <div class="tabContentContainer" >
               <!-- 
                <div id="output" class="${this.tabContentClass}"> 
                    <${this.cobyTextAreaComponentTag}
                    _id="_my_id" 
                    _name="_my_name" 
                    eventType="ondata"></${this.cobyTextAreaComponentTag}>
                </div>  -->
            </div>
     ` ;

     this._appendComponent( this.cobyTextAreaComponentAce , 
                            "buttonLogsId"                , 
                            "contentLogId"                , 
                            "logs"                        , 
                            "textAreaLogsId"              , 
                            onRecievedDataLogsEvent       , 
                            'background: linear-gradient(#3e3d3d, #000000);resize: none' , 
                            ""                  , 
                            onClearLogsEvent    ,
                            "width: 100%;height: 100%;" ) ;
                    
     this._appendComponent( this.cobyTextAreaComponentTag , 
                            "buttonJobsId"                , 
                            "contentJobsId"               ,
                            "Jobs"                        ,
                            "textAreaJobsId"              , 
                            onRecievedDataJobsEvent       , 
                            'background: linear-gradient(#3e3d3d, #000000);resize: none;' , 
                            "readonly"                    ,
                            onClearJobsEvent              ,
                            ""                         )  ;

     this._appendComponent( this.cobyJobSubmissionComponentTag, 
                            "buttonSubmissionJobId"   , 
                            "contentSubmissionJob"    , 
                            "Submission / Extraction" , 
                            "cobyJobSubmissionId"     , 
                            onSubmitJobEvent          ,
                            ""                        ,
                            ""                        ,
                            ""                        ,
                            ""                     )  ;


     this._appendComponent( this.cobyTreeViewComponent, 
                            "buttonViewTreeId"        , 
                            "contentViewTreeId"       , 
                            "Tree View "              , 
                            "cobyTreeViewId"          , 
                            "onRecievTreeViewEvent"   ,
                             'background: red' , 
                            ""                        ,
                            ""                        ,
                            "background: antiquewhite; width:100% ; overflow: overlay;" )  ;


    // this._appendComponent( this.cobyPipelineBuilderTag, "buttonPipelineBuilderId", "contentPipelineBuilderId", "PipelineBuilder", "cobyPipelineBuilderId", onSubmitJobEvent ) ;

     this.querySelector( "#" + "buttonLogsId").click()  // set first textArea as defaultSelection

     this._addSpinner() 

     window.addEventListener( "open-tab" , event => this.openTabEvent(event) )   

     
     _attachScript('components/base/ace-js/ace-js.js') ; 
     
     _attachScript('components/base/textarea/coby-textarea.js') ; 
     _attachScript('components/base/button/coby-button.js') ; 
     _attachScript('components/composite/job-submission/coby-job-submission.js') ; 
  
   }   

  _appendComponent( componentTag, buttonID, contentID, title , textAreaId, eventType, style, attribute , eventTypeClear , componentStyle ) {

    this.querySelector( "." + this.tabClass )
        .appendChild( this._createTabButton( buttonID , title ) )

     this.append( this._createContentTab( componentTag, contentID , textAreaId, eventType, style, attribute , eventTypeClear , componentStyle ) )
    
    this.querySelector( "#" + buttonID )
        .onclick = _ => this.openTab( buttonID , contentID )

  }

  _createTabButton(id, title) {

    var tabButton       =  document.createElement("button") 
    tabButton.className = this.tabLinksClass 
    tabButton.id        = id 
    tabButton.append(title)
    return    tabButton      
  }

   _createContentAce( componentTag, id, textAreaId, eventType, style, attribute , eventTypeClear ) {

    var tabContent       =  document.createElement("div")
    tabContent.className = this.tabContentClass 
    tabContent.id        = id 
    tabContent.append ( this._createComponent( componentTag, textAreaId, textAreaId, eventType , style, attribute, eventTypeClear  ) ) 
    return tabContent
  }
  
  _createContentTab( componentTag, id, textAreaId, eventType, style, attribute , eventTypeClear, componentStyle ) {

    var tabContent       =  document.createElement("div")
    tabContent.className = this.tabContentClass 
    tabContent.id        = id 
    tabContent.append ( this._createComponent( componentTag, textAreaId, textAreaId, eventType , style, attribute, eventTypeClear, componentStyle  ) ) 
    return tabContent
  }

  _createComponent( componentTag, id, name, eventType, style, attribute , eventTypeClear , componentStyle ) {

    var component =  document.createElement(componentTag ) 
    component.setAttribute( "_id"       , id ) 
    component.setAttribute( "_name"     , name ) 
    component.setAttribute( "eventType" , eventType ) 
    component.setAttribute( "_style"    , style ) 
    component.setAttribute( "style"     , componentStyle ) 
    
    if( eventTypeClear !== '' )
    component.setAttribute( "eventTypeClear" ,eventTypeClear ) ;

    if( attribute !== '' )
    component.setAttribute( attribute , attribute ) ;
    
    return component
  }

  _addSpinner() {

    var divSpinner =  document.createElement("div" ) 
    divSpinner.setAttribute( "id" , "divSpinnerId" ) 

    var label      =  document.createElement("label")
    label.setAttribute( "id" , "spinnerLabelId" ) 
    label.setAttribute( "for" , "spinnerId" ) 
    label.innerHTML=" Max lines to keep "
    divSpinner.appendChild( label )

    var spinner    =  document.createElement("input")
    spinner.setAttribute( "id"    , "spinnerId" ) 
    spinner.setAttribute( "type"  , "number"    ) 
    spinner.setAttribute( "min"   , "0"         ) 
    spinner.setAttribute( "max"   , "1000000"   ) 
    spinner.setAttribute( "size"  , "10"        ) 
    spinner.setAttribute( "value" , "100"       )
    spinner.setAttribute( "step"  , "10"        )

    var button    =  document.createElement("button")
    button.setAttribute( "type" , "button" ) 
    button.setAttribute( "id" , "spinnerButtonOkId" ) 
    button.innerHTML="ok" 

    divSpinner.appendChild( spinner ) 
    divSpinner.append( button )   
    
    spinner.addEventListener ( "keyup", (event) => { update_max_lines_to_keep(  event.key , spinner ) } )

    button.onclick = _ => update_max_lines_to_keep( "Enter" , spinner ) 
                               
    this.querySelector( "." + this.tabClass )
        .appendChild( divSpinner ) ;

    this.querySelector( "#" + "spinnerButtonOkId").style.padding = "1px"
    this.querySelector( "#" + "spinnerButtonOkId").style.marginLeft = "7px"
    this.querySelector( "#" + "spinnerButtonOkId").style.marginBottom = "2px"

    this.querySelector( "#" + "spinnerButtonOkId").style.color = "lightblue"
    this.querySelector( "#" + "spinnerButtonOkId").style.backgroundColor = "black"
    this.querySelector( "#" + "spinnerButtonOkId").style.fontWeight = "bold"

    this.querySelector( "#" + "spinnerLabelId").style.marginRight = "8px"
    this.querySelector( "#" + "spinnerLabelId").style.marginBottom = "2px"
    this.querySelector( "#" + "spinnerLabelId").style.backgroundColor = "bisque"

    this.querySelector( "#" + "spinnerId").style.marginBottom = "3px"
    this.querySelector( "#" + "spinnerId").style.backgroundColor = "aqua"

    this.querySelector( "#" + "divSpinnerId").style.float = "right"
    this.querySelector( "#" + "divSpinnerId").style.marginRight = "1em"
    this.querySelector( "#" + "divSpinnerId").style.display = "flex"
    this.querySelector( "#" + "divSpinnerId").style.transform = "translateY(50%)"

  }

  /* TAB */

   openTab( tabLinkClicked, type) {

    // Declare all variables
    var i, tabcontent, tablinks

    // Get all elements with class="tabcontent" and hide them
    tabcontent = this.querySelectorAll("." + this.tabContentClass);
    for (i = 0; i < tabcontent.length; i++) {
       tabcontent[i].style.display = "none" 
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = this.querySelectorAll("." + this.tabLinksClass )
    for ( i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "")
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    this.querySelector("#" + type).style.display = "flex"
    /*evt.currentTarget.className += " active"; */
    this.querySelector("#" + tabLinkClicked).className += " active"
   }
   
   openTabEvent(event) {
    
      var  buttonID  = event.detail.buttonID  
      var  contentID = event.detail.contentID 
      
      console.log("OpenTab : " + buttonID + " // " + contentID ) 
      
      this.openTab( buttonID , contentID ) 
   }

 }

 function update_max_lines_to_keep( codeKey , spinner ) {

  if ( codeKey !== "Enter" ) return 

  if ( spinner.value < 0 ) spinner.value  = 0 

  window.dispatchEvent( new CustomEvent ( "max-lines-event" , 
                                 {  bubbles:  true   , 
                                    composed: true   ,
                                    detail: {
                                      message: spinner.value,
                                    } 
                                 } 
  ) ) ;

  let timerInterval

  var message = spinner.value > 0 ? spinner.value : "Unlimited"
  
        Swal.fire ( {
        title: 'Set Max-Lines to : '  +    message          ,
        html: '<h5>Will close in <b></b> milliseconds</h5>' ,
        timer: 1200 ,
        timerProgressBar: true ,
        onBeforeOpen: () =>    {
            Swal.showLoading()
            timerInterval = setInterval(() => {
            const content = Swal.getContent()
            if (content) {
                const b = content.querySelector('b')
                if (b) {
                b.textContent = Swal.getTimerLeft()
                }
            }
            }, 100 )
        } ,
        onClose: () => {
            clearInterval(timerInterval)
        }
        }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
            console.log('Close by the timer')
        }
        })
 }

 function _attachScript(url) {

  var alreadyLoaded = false 
  var scripts = document.getElementsByTagName('script')
  var stringScripts = "" 
  for ( let script of scripts ) {
     stringScripts += script.outerHTML + "\n" 
     if ( script.outerHTML.includes(url)) {
       alreadyLoaded = true 
       break 
     }
  }
  
  if(alreadyLoaded ) return
  
  let script= document.createElement('script') 
  script.type= 'text/javascript'
  script.src= url 
  document.body.appendChild(script) 
}

 customElements.define("coby-tab", Tab ) 

