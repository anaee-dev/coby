
package com.rac021.jaxy.security.provider ;

import com.rac021.jaxy.api.security.Db ;
import com.rac021.jaxy.api.security.ISignOn ;
import javax.enterprise.context.ApplicationScoped ;
import com.rac021.jaxy.api.exceptions.BusinessException ;

/**
 *
 * @author ryahiaoui
 */


@Db
@ApplicationScoped

public class DbSingOn implements ISignOn {

    @Override
    public boolean checkIntegrity(String token, Long expiration ) throws BusinessException {
       throw new BusinessException("Not supported yet.") ;
    }

    @Override
    public boolean checkIntegrity(String login, String timeStamp, String signature) throws BusinessException {
       throw new BusinessException("Not supported yet.") ;
    }
}
