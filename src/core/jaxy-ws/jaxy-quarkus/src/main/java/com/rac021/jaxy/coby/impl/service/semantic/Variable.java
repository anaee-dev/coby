
package com.rac021.jaxy.coby.impl.service.semantic ;

import com.fasterxml.jackson.annotation.JsonProperty ;

/**
 *
 * @author ryahiaoui
 */

public class Variable {
    
    @JsonProperty(  value = "entity", index = 0 )
    private String entity    ;
    
    @JsonProperty(  value = "variable", index = 1 )
    private String variable ;
    
    @JsonProperty(  value = "categoty", index = 2 )
    private String category  ;
    
    public Variable() { }
    public Variable( String standardName , 
                     String entity       ,
                     String categories ) {
        
        this.variable  = standardName ;
        this.entity    = entity       ;
        this.category  = categories   ;
    }

   
    
    
}
