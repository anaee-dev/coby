
package com.rac021.jaxy.coby.impl.service.logs.sse ;

/**
 *
 * @author ryahiaoui
 */

import javax.ws.rs.GET ;
import java.util.Timer ;
import java.nio.file.Path ;
import java.nio.file.Paths ;
import javax.inject.Inject ;
import java.util.TimerTask ;
import javax.ws.rs.sse.Sse ;
import javax.ws.rs.Produces ;
import javax.inject.Singleton ;
import javax.ws.rs.core.Context ;
import io.quarkus.runtime.Startup ;
import javax.ws.rs.core.MediaType ;
import java.util.concurrent.Future ;
import javax.ws.rs.sse.SseEventSink ;
import javax.ws.rs.sse.SseBroadcaster ;
import java.util.concurrent.Executors ;
import org.apache.commons.io.input.Tailer ;
import com.rac021.jaxy.api.security.Policy ;
import com.rac021.jaxy.api.security.Secured ;
import java.util.concurrent.ExecutorService ;
import java.util.concurrent.ThreadPoolExecutor ;
import java.util.concurrent.ExecutionException ;
import org.apache.commons.io.input.TailerListener ;
import com.rac021.jaxy.api.qualifiers.ServiceRegistry ;
import org.apache.commons.io.input.TailerListenerAdapter ;
import com.rac021.jaxy.coby.impl.service.configuration.CobyConfiguration ;


/**
 *
 * @author R.Yahiaoui
 */

@ServiceRegistry("coby_logs_sse")
@Secured(policy = Policy.CustomSignOn )
@Startup
@Singleton
public class CobyService    {
    
    @Inject
    CobyConfiguration configuration ;
      
    private static ExecutorService executorService = Executors.newFixedThreadPool(5) ;
  
    public CobyService() {
    }
   
    @GET
    @Produces( MediaType.SERVER_SENT_EVENTS )
    public void getResource ( @Context Sse sse                  ,
                              @Context SseEventSink eventSink   )  {

         System.out.println( "\nStreamLoger Service... "  )        ;
         Thread currentThread = Thread.currentThread()             ; 
         
         SseBroadcaster broadcaster =  sse.newBroadcaster()        ;

         Path logFile = Paths.get( configuration.getLoggerFile() ) ;
       
         TailerListener listener = new TailerListenerAdapter()     {
       
            @Override
            public void handle( String line ) {
              broadcaster.broadcast( sse.newEvent( line ) )        ;
           }
         } ;
         
         Tailer tailer = new Tailer( logFile.toFile()                         , 
                                     listener                                 , 
                                     configuration.getFrequencyUpdateTimeMs() ,
                                     false                                    ,
                                     8096                                  ) ;
       
         broadcaster.register( eventSink ) ;
         
         broadcaster.onClose( evSink ->    {
          
            System.out.println( "\nClose Connection.. \n " ) ;
  
            if ( ! evSink.isClosed() ) evSink.close()        ;
            if ( ! currentThread.isInterrupted() )
                   currentThread.interrupt()                 ;
         
         } ) ;
         
         broadcaster.onError( ( evSink , throwable ) ->  {
             
            System.out.println( "\nClient Error : "      + 
                                throwable.getMessage()   + 
                                " \n" )                  ;

            if ( ! evSink.isClosed() ) evSink.close()    ;

            if( ! currentThread.isInterrupted() )
                  currentThread.interrupt()              ;
             
         } ) ;
         
         TimerTask myTask = new TimerTask() {
             
            @Override
            public void run() {

               broadcaster.broadcast( 
                       sse.newEvent ( "ping",  "-" ) ) ;
            }
         } ;

         Timer timer = new Timer()                     ;
        
         timer.schedule(myTask, 8000, 8000 )           ;
       
         Future tail = executorService.submit(tailer)  ;
   
         try {

           tail.get() ;
           
         } catch ( InterruptedException | ExecutionException ex ) {
          
            System.out.println( "\nTail Execution Exception : "   + 
                                ex.getCause().getMessage() + "\n" ) ;
         
         } finally {
             
           System.out.println( "StremLogger Clean " )  ;
           
           timer.cancel()                              ;
           
           if( tailer != null ) tailer.stop()          ;
           
           if( ! tail.isCancelled()) tail.cancel(true) ;

           ((ThreadPoolExecutor) executorService )
                                .remove( tailer  )     ;
           
           broadcaster.close()                         ;

         }
         
         System.out.println( "Service LOG Closed " )   ;

    }
    
}