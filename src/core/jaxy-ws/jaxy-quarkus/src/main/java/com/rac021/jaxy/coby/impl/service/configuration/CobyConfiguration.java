
package com.rac021.jaxy.coby.impl.service.configuration ;

import java.util.Properties ;
import javax.inject.Singleton ;
import java.util.logging.Level ;
import java.io.FileInputStream ;
import java.nio.charset.Charset ;
import java.util.logging.Logger ;
import java.io.InputStreamReader ;
import io.quarkus.runtime.Startup ;
import javax.annotation.PostConstruct ;
import org.eclipse.microprofile.config.inject.ConfigProperty ;

/**
 *
 * @author ryahiaoui
 */

@Singleton
@Startup

public class CobyConfiguration {
 
    @ConfigProperty( name = "configFile"                        , 
                     defaultValue="coby_config.properties"      ) 
    private final String  configFile = "coby_config.properties" ; 
    
    private       String  loggerFile                   = null ;

    private       String  cobyPipelineScript           = null ;
    
    private       String  cobyPipelineSynthesisScript  = null ;

    private       String  cobyIs                       = null ;
    
    private       String  csvSep                       = null ;
   
    private       String  outputDataFolder             = null ;
    
    private       Integer columnVariableAnaeeName      = null ;
    
    private       Integer columnVariableLocalName      = null ;
    
    private       Integer columnVariableCategories     = null ;
  
    private       int     totalExtractionsPerUser      = 1    ;
    
    private int           frequencyUpdateTimeMs        = 1    ;
    
    @PostConstruct
    public void init()  {
        
        Properties prop = new Properties();
	
	try ( FileInputStream input = new FileInputStream(configFile) )       {
         
          prop.load(new InputStreamReader( input, Charset.forName("UTF-8")) ) ;

          cobyPipelineSynthesisScript  = prop.getProperty( "coby_synthesis_script" ).replaceAll(" +", " ").trim() ;
          
          cobyPipelineScript           = prop.getProperty( "coby_script" ).replaceAll(" +", " ").trim()     ;
          
          loggerFile                   = prop.getProperty( "logger_file" ).replaceAll(" +", " ").trim()     ;  
          
          frequencyUpdateTimeMs        = Integer.parseInt( prop.getProperty("frequency_logs_update_time_ms" )
                                                               .replaceAll(" +", " ").trim())               ;
          
          cobyIs                       = prop.getProperty( "coby_is" ).replaceAll( " +", " " ).trim()       ; 

          csvSep                       = prop.getProperty( "csv_sep" ).replaceAll(" +", " ").trim()         ;
          
          outputDataFolder             = prop.getProperty( "coby_output_data_folder" )
                                             .replaceAll(" +", " ").trim()                                  ; 
          
          columnVariableAnaeeName      = Integer.parseInt(prop.getProperty("column_variable_anaee_name")
                                                .replaceAll(" +", " ").trim())                              ;
          
          columnVariableLocalName      = Integer.parseInt(prop.getProperty("column_variable_local_name")
                                                .replaceAll(" +", " ").trim())  ;
          
          columnVariableCategories     = Integer.parseInt(prop.getProperty("column_variable_categories")
                                                .replaceAll(" +", " ").trim())                              ;
          
          totalExtractionsPerUser      = Integer.parseInt(prop.getProperty("total_extractions_per_user")
                                                .replaceAll(" +", " ").trim())                              ;
          
          System.out.println("                                                                          " ) ;
          System.out.println("                                                                          " ) ;
          System.out.println(" COBY CONFIGURATION  **************************************               " ) ;
          System.out.println("                                                                          " ) ;
          System.out.println("     -->  cobyIs  = " + cobyIs                                              ) ;
          System.out.println("     -->  coby_script               = " + cobyPipelineScript                ) ;
          System.out.println("     -->  coby_synthesis_script     = " + cobyPipelineSynthesisScript       ) ;
          System.out.println("     -->  outputDataFolder          = " + outputDataFolder                  ) ;
          System.out.println("     -->  loggerFile                = " + loggerFile                        ) ;
          System.out.println("     -->  frequencyUpdateTimeMs     = " + frequencyUpdateTimeMs + " ( ms )" ) ;
          System.out.println("     -->  columnVariable_AnaeeName  = " + columnVariableAnaeeName           ) ;
          System.out.println("     -->  columnVariable_LocalName  = " + columnVariableLocalName           ) ;
          System.out.println("     -->  csv_Sep                   = " + csvSep                            ) ;
          System.out.println("     -->  totalExtractionsPerUser   = " + totalExtractionsPerUser           ) ;
          System.out.println("                                                                          " ) ;
          System.out.println(" **********************************************************               " ) ;
          System.out.println("                                                                          " ) ;
               
	} catch( Exception ex ) {
            Logger.getLogger(CobyConfiguration.class.getName()).log(Level.SEVERE, null, ex) ;
            System.exit( 2 ) ;
        }
    }

    public String getLoggerFile() {
        return loggerFile ;
    }

    public String getCobyPipelineScript() {
        return cobyPipelineScript ;
    }
    
    public String getCobyPipelineSynthesisScript() {
        return cobyPipelineSynthesisScript ;
    }

    public int getFrequencyUpdateTimeMs()  {
        return frequencyUpdateTimeMs ;
    }

    public String getCobyIs() {
        return cobyIs ;
    }

    public String getCsvSep() {
        return csvSep ;
    }

    public Integer getColumnVariableAnaeeName() {
        return columnVariableAnaeeName ;
    }

    public Integer getColumnVariableLocalName() {
        return columnVariableLocalName ;
    }

    public String getOutputDataFolder()     {
        return outputDataFolder ;
    }

    public int getTotalExtractionsPerUser() {
        return totalExtractionsPerUser ;
    }

    public Integer getColumnVariableCategories() {
        return columnVariableCategories ;
    }
    
}
