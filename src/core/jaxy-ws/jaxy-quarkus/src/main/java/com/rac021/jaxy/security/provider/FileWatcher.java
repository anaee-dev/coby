
package com.rac021.jaxy.security.provider ;

import java.nio.file.* ;
import org.slf4j.Logger ;
import java.io.IOException ;
import org.slf4j.LoggerFactory ;
import java.util.concurrent.TimeUnit ;

public class FileWatcher {
    
	private static final Logger log = LoggerFactory.getLogger(FileWatcher.class ) ;

	private Thread thread              ;
	private WatchService watchService  ;

	public interface Callback {
	      void run() throws Exception  ;
	}

	public static void onFileChange(Path file, Callback callback) throws IOException {
		FileWatcher fileWatcher = new FileWatcher()                           ;
		fileWatcher.start(file, callback )                                    ;
		Runtime.getRuntime().addShutdownHook( new Thread(fileWatcher::stop) ) ;
	}

	public void start(Path file, Callback callback) throws IOException {
            
		watchService = FileSystems.getDefault().newWatchService()  ;
		Path parent = file.toAbsolutePath().getParent()                       ;
		parent.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY  ) ;
		
                log.info(" Watch file : " + file ) ;

		thread = new Thread(() -> {
                    
			while ( true )    {
                            
				WatchKey wk = null ;

				try {
					wk = watchService.take()           ;
					TimeUnit.MILLISECONDS.sleep( 500 ) ; 
                                        
					for (WatchEvent<?> event : wk.pollEvents()) {
                                            
						Path changed = parent.resolve((Path) event.context())         ;
						if (Files.exists(changed) && Files.isSameFile(changed, file)) {
							log.info("File change event : " + changed           ) ;
							callback.run()                                        ;
							break                                                 ;
						}
					}

				} catch (InterruptedException e)    {
					log.info("Ending my watch"  )      ;
					Thread.currentThread().interrupt() ;
					break                              ;
				} catch (Exception e) {
					log.error("Error while reloading ", e ) ;
				} finally {
					if (wk != null)    {
						wk.reset() ;
					}
				}
			}
		} ) ;
                
		thread.start() ;
	}

	public void stop()       {

		thread.interrupt() ;

		try {
			watchService.close() ;

		} catch (IOException e )     {
			log.info("Error closing watch service", e ) ;
		}
	}

}