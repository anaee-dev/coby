
package com.rac021.jaxy.coby.impl.service.jobs ;

/**
 *
 * @author ryahiaoui
 */

import javax.ws.rs.GET ;
import javax.ws.rs.Produces ;
import javax.inject.Singleton ;
import javax.ws.rs.HeaderParam ;
import javax.ws.rs.core.UriInfo ;
import javax.ws.rs.core.Context ;
import javax.ws.rs.core.Response ;
import io.quarkus.arc.Unremovable ;
import java.util.stream.Collectors ;
import javax.annotation.PostConstruct ;
import com.rac021.jaxy.api.security.Policy ;
import com.rac021.jaxy.api.security.Secured ;
import com.rac021.jaxy.coby.impl.utils.TokenManager ;
import com.rac021.jaxy.api.qualifiers.ServiceRegistry ;
import com.rac021.jaxy.api.exceptions.BusinessException ;
import static com.rac021.jaxy.coby.impl.scheduler.CobyScheduler.JOBS ;

/**
 *
 * @author R.Yahiaoui
 */

@ServiceRegistry("coby_jobs")
@Secured(policy = Policy.CustomSignOn )
@Singleton
@Unremovable
public class CobyService    {
 
    @PostConstruct
    public void init() {
    }

    public CobyService() {
    }
    
    @GET
    @Produces( { "xml/plain" , "json/plain" , "json/encrypted" , "xml/encrypted" } )
    public Response list ( @HeaderParam("API-key-Token") String token ,
                           @HeaderParam("keep") String filterdIndex   , 
                           @Context UriInfo uriInfo ) throws BusinessException {    
    
         String s         = " EMPTY JOB LIST \n "        ;
         
         String login     = TokenManager.getLogin(token) ;
           
         if( JOBS != null && ! JOBS.isEmpty()) {
            s= JOBS.stream()
                   .filter( job -> job != null)
                   .filter( job ->  login.equalsIgnoreCase("admin") || job.trim().startsWith(login) )
                   .map(Object::toString )
                   .collect(Collectors.joining(" \n -------------------------- \n ")) ;
         }
         return Response.status(Response.Status.OK)
                        .entity( "\n " + s )
                        .build() ;
    }
    
}

