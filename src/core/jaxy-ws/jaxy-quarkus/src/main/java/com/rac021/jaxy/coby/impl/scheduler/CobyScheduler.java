
package com.rac021.jaxy.coby.impl.scheduler ;

import java.util.List ;
import javax.inject.Inject ;
import java.util.ArrayList ;
import javax.inject.Singleton ;
import java.util.logging.Level;
import java.util.logging.Logger ;
import java.util.concurrent.Future ;
import java.util.concurrent.TimeUnit ;
import io.quarkus.scheduler.Scheduled ;
import javax.annotation.PostConstruct ;
import com.rac021.jaxy.coby.impl.utils.Writer ;
import java.util.concurrent.ThreadPoolExecutor ;
import java.util.concurrent.LinkedBlockingQueue ;
import com.rac021.jaxy.coby.impl.utils.TokenManager ;
import static com.rac021.jaxy.api.logger.LoggerFactory.getLogger ;
import com.rac021.jaxy.coby.impl.service.configuration.CobyConfiguration ;


/**
 *
 * @author ryahiaoui
 */

@Singleton
public class CobyScheduler {
    
    @Inject 
    CobyConfiguration configuration       ;
    
    public static int MAX_EXTRACTIONS = 1 ;
    
    public static final ThreadPoolExecutor executorService = new ThreadPoolExecutor( 1                            , 
                                                                                     1                            , 
                                                                                     60L                          , 
                                                                                     TimeUnit.SECONDS             , 
                                                                                     new LinkedBlockingQueue(1) ) ;
    public static List<String>    JOBS                               ;

    public static Future<Integer> SUBMITTED_JOB                      ;
    
    public static String          jobOwner      = null               ;
     
    private static final Logger   LOGGER        = getLogger()        ;
    
    public static final String    SYNTHESIS_OP  = "##SYNTHESIS_OP##" ;
    
    public static final String    JOB_SYNTHESIS = "Job-Synthesis"    ;
   

    @PostConstruct
    public void method()       {
        
        JOBS = new ArrayList<>()                                     ;
        MAX_EXTRACTIONS = configuration.getTotalExtractionsPerUser() ;
        System.out.println(" Wating for process " )                  ;
        System.out.println(" Interval time -> "   +  configuration.getFrequencyUpdateTimeMs() + " ( ms ) ") ;
        System.out.println(" Total Extraction per User -> " +  MAX_EXTRACTIONS                            ) ;
    }
    
    @Scheduled( every="5s" )
    public void popQuery() throws Exception {
      
       if( ! Writer.existFile( configuration.getCobyPipelineScript()) ) {
           
           LOGGER.log( Level.SEVERE                          , 
                       " The Script [{0}] + Not found ! "    , 
                       configuration.getCobyPipelineScript() ) ;
           
       }
       
       if ( ! Writer.existFile( configuration.getCobyPipelineSynthesisScript()) ) {
           
           LOGGER.log( Level.SEVERE                                   ,
                       " The Script [{0}] + Not found ! "             ,
                       configuration.getCobyPipelineSynthesisScript() ) ;
       }
       
       else if( ! JOBS.isEmpty() )   {
         
             if ( executorService.getActiveCount() == 0 )  {
       
                   String query  = JOBS.get(0)             ;

                   String script = configuration.getCobyPipelineScript()    ;
                   
                   boolean synthesisExtraction = false ;
                   
                   if( query.trim().toUpperCase().startsWith(SYNTHESIS_OP)) {
                       
                       synthesisExtraction = true                                    ; 
                       
                       query  = query.replace(CobyScheduler.SYNTHESIS_OP, "").trim() ;
                       
                       script = configuration.getCobyPipelineSynthesisScript()       ;
                   }

                   String login  =  TokenManager.getLogin( query )  ;
                 
                   if (  synthesisExtraction  ||
                         Writer.getTotalDirectories( TokenManager.buildOutputFolder( configuration.getOutputDataFolder() , 
                                                                                     login )  )  <  MAX_EXTRACTIONS   ) {
                   
                        LOGGER.log( Level.INFO , "Call Service : {0}", script )                                                            ; 
                                               
                        JOBS.remove(0)                                                                                                     ;
                        
                        jobOwner      = login                                                                                              ;
                       
                        SUBMITTED_JOB = executorService.submit(new PipelineRunner( script                                                  , 
                                                                                   query                                                   , 
                                                                                   TokenManager.builPathLog( configuration.getLoggerFile() , 
                                                                                                             login                       ) ,
                                                                                   configuration.getFrequencyUpdateTimeMs(), synthesisExtraction ) ) ;
                        /* Wait for processing */
                        SUBMITTED_JOB.get() ;
                        jobOwner  =   null  ;
                   }
                   
                   else if ( query != null ) {
                       /* Rotate Job */
                       JOBS.remove(0)        ;
                       JOBS.add( query )     ;
                   }
             }
       }
    }
    
}
