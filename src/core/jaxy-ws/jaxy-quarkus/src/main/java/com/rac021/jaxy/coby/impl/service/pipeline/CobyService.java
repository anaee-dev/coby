
package com.rac021.jaxy.coby.impl.service.pipeline ;

/**
 *
 * @author ryahiaoui
 */

import javax.ws.rs.GET ;
import javax.inject.Inject ;
import java.net.URLDecoder ;
import javax.ws.rs.Produces ;
import javax.inject.Singleton ;
import javax.ws.rs.HeaderParam ;
import javax.ws.rs.core.UriInfo ;
import javax.ws.rs.core.Context ;
import javax.ws.rs.core.Response ;
import io.quarkus.runtime.Startup ;
import javax.annotation.PostConstruct ;
import com.rac021.jaxy.api.security.Policy ;
import com.rac021.jaxy.api.security.Secured ;
import com.rac021.jaxy.coby.impl.utils.Writer ;
import com.rac021.jaxy.coby.impl.utils.TokenManager ;
import com.rac021.jaxy.api.qualifiers.ServiceRegistry ;
import com.rac021.jaxy.api.exceptions.BusinessException ;
import com.rac021.jaxy.coby.impl.scheduler.CobyScheduler ;
import static com.rac021.jaxy.coby.impl.scheduler.CobyScheduler.JOBS ;
import com.rac021.jaxy.coby.impl.service.configuration.CobyConfiguration ;
import static com.rac021.jaxy.coby.impl.scheduler.CobyScheduler.executorService ;

/**
 *
 * @author R.Yahiaoui
 */

@ServiceRegistry("coby")
@Secured(policy = Policy.CustomSignOn )
@Singleton
@Startup

public class CobyService    {
 
    @Inject 
    CobyConfiguration configuration ;

    @PostConstruct
    public void init() {
    }

    public CobyService() {
    }
   
    @GET
    @Produces( { "xml/plain" , "json/plain" , "json/encrypted" , "xml/encrypted" } )
    public Response getResource ( @HeaderParam("API-key-Token") String token        ,
                                  @HeaderParam("keep")          String filterdIndex , 
                                  @Context UriInfo uriInfo ) throws BusinessException, Exception {    
       
       if( ! Writer.existFile( configuration.getCobyPipelineScript() ) && 
           ! Writer.existFile( configuration.getCobyPipelineSynthesisScript()) ) {
           
           System.err.println("                                                                                                              " ) ;
           System.err.println(" No Script Orchestrator Provided !!                                                                           " ) ;
           System.err.println(" Must Provide At Least One Orchestrator Script :                                                              " ) ;
           System.err.println("  --> Script Orchestrator [ " +  configuration.getCobyPipelineScript()           + " ] + Not found !          " ) ;
           System.err.println("  --> Script Orchestrator [ " +  configuration.getCobyPipelineSynthesisScript()  + " ] + Not found !          " ) ;
           System.err.println("                                                                                                              " ) ;
           
           return Response.status( Response.Status.OK)
                          .entity( "\n No Orchestrator Script found ! \n \n "                      +
                                   "  -> Path : " + configuration.getCobyPipelineScript()          + "\n" +
                                   "  -> Path : " + configuration.getCobyPipelineSynthesisScript() + "\n" +
                                   " Please, provide at least one Orchestrator Script \n" )
                          .build() ;
        }
        
        String encodedQuery = uriInfo.getRequestUri().getQuery()   ;
        
        if( encodedQuery == null || encodedQuery.trim().isEmpty()) {
                            
            return Response.status(Response.Status.OK)
                           .entity("\n Empty Queries not Accepted  \n  " )
                           .build() ;                 
        }
        
        String login = TokenManager.getLogin(token) ;
        
        String job = TokenManager.getJob(token) ;  // cpichot jv2020
        
        String login_query = login + " " + job + " " + URLDecoder.decode( encodedQuery.trim(), "UTF-8" ) ; //cpichot
            
        if( login_query.contains("$")  || 
            login_query.contains("@")  || 
            login_query.contains("^")  || 
            login_query.contains("'")  || 
            login_query.contains("`") ) {

             return Response.status(Response.Status.OK)
                            .entity("\n  Please, modify your Request. \n "
                                   + " Some Sharacters Are Not Authorized \n "
                                   + " List --> $ @ ^ ' ` \n \n " )
                            .build() ;
        }
        
        login_query = cleanQuery(login_query)         ;
         
        if (  executorService.getActiveCount() == 0 ) {
   
             if ( Writer.getTotalDirectories( TokenManager.buildOutputFolder( configuration.getOutputDataFolder() , 
                                                                              login )  ) >= CobyScheduler.MAX_EXTRACTIONS ) {
             
                 if( Writer.isFullStack(JOBS.size()) ) {
                     
                    return Response.status(Response.Status.OK)
                             .entity("\n Full Stack ( 50 Jobs waiting ). \n"
                                     + " Please re-try later & \n "
                                     + " Consider Emptying the Data Generation Directory \n "
                                     + " You can use the Service : [ coby_jobs ] to consult waiting jobs " )
                             .build() ;
                 }
                 
                 JOBS.add(login_query ) ;
                 
                return Response.status( Response.Status.OK )
                               .entity("\n JOB Registered BUT .. \n  "
                                     + "Data has already been generated. \n "
                                     + " Please transfer them to free the generation directory  .. \n \n "
                                     + " You can use the Service : [ coby_clean_output ] to clean the Directory " )
                               .build() ;                 
             }
             
             boolean releasedExecutorService = CobyScheduler.executorService.getActiveCount() == 0 ;
             
             JOBS.add(login_query)         ;
           
             if( releasedExecutorService ) {
               
              return Response.status(Response.Status.OK)
                             .entity(" \n JOB Launched. \n \n "
                                  + " You can see LOGS using the Service : [ coby_logs ] \n " )
                             .build() ;
             } else {
                return Response.status ( Response.Status.OK )
                               .entity(" \n Job Will Be Processed As Soon As Possible .. \n " )
                               .build() ;
             }
         } 
         
         else  {
             
              if(  Writer.isFullStack(JOBS.size() )) {
                     
                    return Response.status(Response.Status.OK)
                             .entity("\n Full Stack ( 50 Jobs waiting ). \n"
                                     + " Please re-try later & \n "
                                     + " Consider Emptying the Data Generation Directory \n "
                                     + " You can use the Service : [ coby_jobs ] to consult waiting jobs " )
                             .build() ;
              }
              
              JOBS.add(login_query)   ;
              
              if( JOBS.size() == 1 || JOBS.isEmpty() ) {
               
                    return Response.status(Response.Status.OK)
                                   .entity(" \n JOB Launched. \n \n "
                                        + " You can see LOGS using the Service : [ coby_logs ] \n " )
                                   .build() ;
              } else {
                      return Response.status ( Response.Status.OK )
                                   .entity(" \n Job Will Be Processed As Soon As Possible ..   \n " )
                                   .build() ;
              }
         }
  
    }

    public static String cleanQuery ( String query ) {
        
      return query.replace("$", "") 
                  .replace("@", "") 
                  .replace("^", "") 
                  .replace("`", "") 
                  .replace("'", "") ;
    }
    
}

