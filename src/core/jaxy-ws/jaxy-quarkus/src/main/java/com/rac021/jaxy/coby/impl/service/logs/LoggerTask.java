
package com.rac021.jaxy.coby.impl.service.logs ;

/**
 *
 * @author ryahiaoui
 */

public interface LoggerTask {
    
   public void setLoggerFile(String LOGGER_FILE) ;

   public void setTailDelayMillis(int tailDelayMillis ) ;

}
