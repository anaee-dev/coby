
package com.rac021.jaxy.coby.impl.service.clean.output ;

/**
 *
 * @author ryahiaoui
 */

import javax.ws.rs.GET ;
import javax.inject.Inject ;
import javax.ws.rs.Produces ;
import javax.inject.Singleton ;
import javax.ws.rs.HeaderParam ;
import javax.ws.rs.core.UriInfo ;
import javax.ws.rs.core.Context ;
import javax.ws.rs.core.Response ;
import io.quarkus.arc.Unremovable;
import javax.annotation.PostConstruct ;
import com.rac021.jaxy.api.security.Policy ;
import com.rac021.jaxy.api.security.Secured ;
import com.rac021.jaxy.coby.impl.utils.Writer ;
import com.rac021.jaxy.coby.impl.utils.TokenManager ;
import com.rac021.jaxy.api.qualifiers.ServiceRegistry ;
import com.rac021.jaxy.coby.impl.service.configuration.CobyConfiguration ;

/**
 *
 * @author R.Yahiaoui
 */

@ServiceRegistry("coby_clean_output")
@Secured(policy = Policy.CustomSignOn )
@Singleton
@Unremovable
public class CobyService    {
 
    @Inject
    CobyConfiguration configuration ;
    
    @PostConstruct
    public void init() {
    }

    public CobyService() {
    }
    
    @GET
    @Produces( {  "xml/plain" , "json/plain" , "json/encrypted" , "xml/encrypted"   } )
    public Response cancel ( @HeaderParam("API-key-Token") String token  , 
                             @HeaderParam("keep") String filterdIndex    , 
                             @Context UriInfo uriInfo ) throws Exception {    
         
        String login     = TokenManager.getLogin(token) ;
        String path_data = TokenManager.buildOutputFolder(configuration.getOutputDataFolder(), login ) ;
        
        Writer.removeDirectory( path_data ) ;
        return Response.status(Response.Status.OK)
                       .entity("\n Coby Output Directory Cleaned \n" )
                       .build() ;      
    }
  
}
