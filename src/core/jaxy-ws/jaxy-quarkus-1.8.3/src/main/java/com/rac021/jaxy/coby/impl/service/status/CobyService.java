
package com.rac021.jaxy.coby.impl.service.status ;

/**
 *
 * @author ryahiaoui
 */

import javax.ws.rs.GET ;
import javax.ws.rs.Produces ;
import javax.inject.Singleton ;
import javax.ws.rs.HeaderParam ;
import javax.ws.rs.core.UriInfo ;
import javax.ws.rs.core.Context ;
import javax.ws.rs.core.Response ;
import io.quarkus.runtime.Startup ;
import javax.annotation.PostConstruct ;
import com.rac021.jaxy.api.security.Policy ;
import com.rac021.jaxy.api.security.Secured ;
import com.rac021.jaxy.api.qualifiers.ServiceRegistry ;
import static com.rac021.jaxy.coby.impl.scheduler.CobyScheduler.executorService ;

/**
 *
 * @author R.Yahiaoui
 */

@ServiceRegistry("coby_status")
@Secured(policy = Policy.CustomSignOn )
@Singleton
@Startup

public class CobyService {
 
    @PostConstruct
    public void init()   {
    }

    public CobyService() {
    }
    
    @GET
    @Produces( {  "xml/plain" , "json/plain" , "json/encrypted" , "xml/encrypted"  } )
    public Response list ( @HeaderParam("keep") String filterdIndex , 
                           @Context UriInfo uriInfo ) {    
         
        if ( executorService.getActiveCount() == 0 ) {

          return Response.status(Response.Status.OK)
                         .entity(" \n COBY RUNNING : NO \n " )
                         .build() ;
         } 
         
         else  {
            return Response.status ( Response.Status.OK )
                           .entity(" \n COBY RUNNING : YES \n " )
                           .build() ;
         }
    }
}
