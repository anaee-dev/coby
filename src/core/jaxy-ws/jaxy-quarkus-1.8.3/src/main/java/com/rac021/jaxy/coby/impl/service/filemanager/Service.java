
package com.rac021.jaxy.coby.impl.service.filemanager ;

import java.io.File ;
import javax.ws.rs.GET ;
import javax.ws.rs.POST ;
import java.io.IOException ;
import java.nio.file.Files ;
import java.nio.file.Paths ;
import javax.inject.Inject ;
import javax.ws.rs.Produces ;
import javax.ws.rs.Consumes ;
import javax.inject.Singleton ;
import java.io.BufferedWriter ;
import javax.ws.rs.HeaderParam ;
import javax.ws.rs.core.UriInfo ;
import javax.ws.rs.core.Context ;
import javax.ws.rs.core.Response ;
import javax.ws.rs.core.MediaType ;
import io.quarkus.arc.Unremovable ;
import javax.annotation.PostConstruct ;
import com.rac021.jaxy.api.security.Policy ;
import com.rac021.jaxy.api.security.Secured ;
import com.rac021.jaxy.api.qualifiers.ServiceRegistry ;
import com.rac021.jaxy.api.exceptions.BusinessException ;
import com.rac021.jaxy.coby.impl.service.configuration.CobyConfiguration ;

/**
 *
 * @author R.Yahiaoui
 */

@ServiceRegistry("filemanager")
@Secured(policy = Policy.CustomSignOn )
@Singleton
@Unremovable
public class Service    {
 
    @Inject 
    CobyConfiguration configuration       ;
   
    @PostConstruct
    public void init()  { }

    public Service()    { }
    
    @GET
    @Produces( { "xml/plain" , "json/plain" , "json/encrypted" , "xml/encrypted" } )
    public Response readContentFile ( @HeaderParam("API-key-Token") String token       ,
                                      @HeaderParam("file-name")     String filteName   , 
                                      @Context UriInfo uriInfo ) throws BusinessException, IOException {    
    
        String pathFile = configuration.getCobyIs() + File.separator + filteName.replace( "..", "" ) ;
        
        System.out.println( " File : " + pathFile  ) ;
        
        if( Files.exists( Paths.get( pathFile)) && ! Files.isDirectory( Paths.get(pathFile) )) {
            
            return Response.status( Response.Status.OK )
                           .entity( new String(Files.readAllBytes(Paths.get(  pathFile ))) )
                           .build() ;
        }
        return Response.status( Response.Status.OK )
                       .entity( "\n---------------------------------------------------+\n" +
                                "---------------------------------------------------\n"    +
                                "The selected File does not exist or is a Directory\n"     +
                                "---------------------------------------------------\n"    +
                                "---------------------------------------------------\n\n"  +
                                "-> Refresh The TreeView And Retry !" )
                       .build() ;
    }
    
    @POST
    @Produces( { "xml/plain" , "json/plain" , "json/encrypted" , "xml/encrypted" } )
    @Consumes({ MediaType.TEXT_PLAIN })
    public Response updateContentFile ( @HeaderParam("API-key-Token") String token          ,
                                        @HeaderParam("file-name")     String filteName      ,
                                        String                        contentFile           ,
                                        @Context UriInfo uriInfo ) throws BusinessException , IOException {
    
        String pathFile = configuration.getCobyIs() + File.separator + filteName.replace( "..", "" ) ;
        
        System.out.println( " File : " + pathFile  ) ;
        
        if( Files.exists( Paths.get( pathFile)) && ! Files.isDirectory( Paths.get(pathFile) )) {
            
            try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(pathFile))) {
                writer.write( contentFile ) ;
            }
            
            return Response.status( Response.Status.OK )
                           .entity( "Updated File :\n " + filteName )
                           .build() ;
        }
        return Response.status( Response.Status.OK )
                       .entity( "The selected File does not exist or is a Directory" )
                       .build() ;
    }
    
}

