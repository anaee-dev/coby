
package com.rac021.jaxy.coby.impl.service.semantic ;

import java.util.Set ;
import javax.xml.bind.annotation.XmlType ;
import javax.xml.bind.annotation.XmlElement ;
import javax.xml.bind.annotation.XmlAccessType ;
import javax.xml.bind.annotation.XmlRootElement ;
import javax.xml.bind.annotation.XmlAccessorType ;

/**
 *
 * @author ryahiaoui
 */

 @XmlRootElement(name="SemanticDescription")
 @XmlAccessorType(XmlAccessType.FIELD)
 @XmlType(propOrder = { "is", "variable_class", "variables" , "select_vars" } )
 public class SemanticDescription {
        
    @XmlElement(name="SI")
    public  String        is             ;
    
    @XmlElement(name="CLASS")
    public  String        variable_class ;

    @XmlElement(name="Variables")
    public  Set<Variable> variables      ;
    
    @XmlElement(name="Select_Vars")
    public  Set<String>   select_vars    ;

    
    public SemanticDescription() {
    }
    public SemanticDescription( String        si             ,
                                String        variable_class ,
                                Set<Variable> variables      , 
                                Set<String>   select_vars    )  {
        
        this.is              = si             ;
        this.variable_class  = variable_class ;
        this.variables       = variables      ;
        this.select_vars      = select_vars   ;
    }

    public String getVariable_class() {
        return variable_class;
    }

    @Override
    public String toString() {
        return is + " - " + variable_class + " - " + variables + " - " + select_vars ;
                
    }
    
 }
