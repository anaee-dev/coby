
package com.rac021.jaxy.coby.impl.service.cancel.all ;

/**
 *
 * @author ryahiaoui
 */

import javax.ws.rs.GET ;
import javax.ws.rs.Produces ;
import javax.inject.Singleton ;
import javax.ws.rs.HeaderParam ;
import javax.ws.rs.core.UriInfo ;
import javax.ws.rs.core.Context ;
import javax.ws.rs.core.Response ;
import io.quarkus.arc.Unremovable ;
import java.util.concurrent.TimeUnit ;
import javax.annotation.PostConstruct ;
import com.rac021.jaxy.api.security.Policy ;
import com.rac021.jaxy.api.security.Secured ;
import static java.util.Collections.singletonMap ;
import com.rac021.jaxy.coby.impl.utils.TokenManager ;
import com.rac021.jaxy.api.qualifiers.ServiceRegistry ;
import com.rac021.jaxy.coby.impl.scheduler.CobyScheduler ;
import com.rac021.jaxy.coby.impl.process_tree_killer.ProcessTree ;
import static com.rac021.jaxy.coby.impl.scheduler.CobyScheduler.JOBS ;
import static com.rac021.jaxy.coby.impl.scheduler.CobyScheduler.SYNTHESIS_OP ;
import static com.rac021.jaxy.coby.impl.scheduler.CobyScheduler.SUBMITTED_JOB ;
import static com.rac021.jaxy.coby.impl.scheduler.PipelineRunner.ENV_PROCESS_BUILDER_KEY ;
import static com.rac021.jaxy.coby.impl.scheduler.PipelineRunner.ENV_PROCESS_BUILDER_VAL ;

/**
 *
 * @author R.Yahiaoui
 */

@ServiceRegistry("coby_cancel_all")
@Secured(policy = Policy.CustomSignOn )
@Singleton
@Unremovable
public class CobyService    {
 
    @PostConstruct
    public void init() {
    }

    public CobyService() {
    }
    
    @GET
    @Produces( {  "xml/plain" , "json/plain" , "json/encrypted" , "xml/encrypted"  } )
    public Response cancel ( @HeaderParam("API-key-Token") String token        ,
                             @HeaderParam("keep")          String filterdIndex , 
                             @Context                      UriInfo uriInfo     ) throws Exception {    
         
        String login     = TokenManager.getLogin(token ) ;
         
        if( JOBS != null && ! JOBS.isEmpty() )       {

            JOBS.removeIf( q -> q.trim()
                                 .startsWith(login) || 
                                q.trim()
                                 .startsWith( SYNTHESIS_OP + " " + login ) ) ;
        }
        
        if( SUBMITTED_JOB != null ) {
          
           if( CobyScheduler.jobOwner == null ) {
               
               return Response.status( Response.Status.OK )
                              .entity(" \n All jobs Cleaned & Canceled ... \n " )
                              .build() ;
           }
           
           else if( CobyScheduler.jobOwner.equals(login )) {
               
                SUBMITTED_JOB.cancel( true )   ;

                ProcessTree.get().killAll(singletonMap( ENV_PROCESS_BUILDER_KEY , 
                                                        ENV_PROCESS_BUILDER_VAL ) ) ;
              
               TimeUnit.MILLISECONDS.sleep( 100 ) ; 

                return Response.status(Response.Status.OK)
                               .entity(" \n All jobs Cleaned & Canceled ... \n " )
                               .build() ;
           }
        }
        
        return Response.status(Response.Status.OK)
                       .entity(" \n  All jobs Cleared from the Queue \n "
                               + " No Current Job for the user [ " + login + " ] "
                               + " submited to Cancel \n " )
                       .build() ;
    }   
}

