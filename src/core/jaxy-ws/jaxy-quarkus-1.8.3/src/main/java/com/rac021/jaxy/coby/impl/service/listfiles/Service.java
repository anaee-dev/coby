
package com.rac021.jaxy.coby.impl.service.listfiles ;

/**
 *
 * @author ryahiaoui
 */

import java.io.File ;
import java.util.Map ;
import javax.ws.rs.GET ;
import java.util.HashMap ;
import java.nio.file.Path ;
import java.io.IOException ;
import java.nio.file.Files ;
import java.nio.file.Paths ;
import javax.inject.Inject ;
import javax.ws.rs.Produces ;
import java.nio.file.WatchKey ;
import javax.inject.Singleton ;
import javax.ws.rs.HeaderParam ;
import java.nio.file.LinkOption ;
import java.nio.file.WatchEvent ;
import javax.ws.rs.core.UriInfo ;
import javax.ws.rs.core.Context ;
import java.util.logging.Level ;
import java.util.logging.Logger ;
import javax.ws.rs.core.Response ;
import java.nio.file.FileSystems ;
import io.quarkus.arc.Unremovable ;
import java.nio.file.WatchService ;
import java.util.concurrent.TimeUnit ;
import javax.annotation.PostConstruct ;
import javax.enterprise.event.Observes ;
import com.rac021.jaxy.api.security.Policy ;
import javax.enterprise.context.Initialized ;
import com.rac021.jaxy.api.security.Secured ;
import java.nio.file.StandardWatchEventKinds ;
import javax.enterprise.context.ApplicationScoped ;
import com.rac021.jaxy.api.qualifiers.ServiceRegistry ;
import com.rac021.jaxy.api.exceptions.BusinessException ;
import com.rac021.jaxy.coby.impl.service.configuration.CobyConfiguration ;

/**
 *
 * @author R.Yahiaoui
 */

@ServiceRegistry("listfiles")
@Secured(policy = Policy.CustomSignOn )
@Singleton
@Unremovable
public class Service    {
 
    @Inject 
    CobyConfiguration configuration         ;
   
    private static final Map<WatchKey, Path> keyPathMap = new HashMap<>() ;

    private static String jsonPath = ""     ;
    
    private static String SI_PATH = "../SI" ;
    
    public Service()    { }
   
    @PostConstruct
    public void init() throws Exception     {
     
       SI_PATH = configuration.getCobyIs()  ;
    }
 
    @GET
    @Produces( { "xml/plain" , "json/plain" , "json/encrypted" , "xml/encrypted" } )
    public Response listFiles ( @HeaderParam("API-key-Token") String token       ,
                                @Context UriInfo uriInfo ) throws BusinessException, IOException {    

        return Response.status( Response.Status.OK )
                        .entity(  jsonPath )
                        .build() ;
    }
    
    
    private static void registerDir (Path path, WatchService watchService) throws IOException {

       if (!Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS)) {
           return ;
       }

       System.out.println("Registering : " + path ) ;

       WatchKey key = path.register( watchService                         ,
                                     StandardWatchEventKinds.ENTRY_CREATE ,
                                     StandardWatchEventKinds.ENTRY_DELETE ) ;
       
       keyPathMap.put( key , path )                                         ;

       for (File f : path.toFile().listFiles()  ) {
           registerDir(f.toPath(), watchService ) ;
       }
    }

    private static void startListening (WatchService watchService) throws Exception {
       
       while (true) {
           
           WatchKey queuedKey = watchService.take() ;
           
           for ( WatchEvent<?> watchEvent : queuedKey.pollEvents() ) {

               if ( watchEvent.kind() == StandardWatchEventKinds.ENTRY_CREATE ||
                    watchEvent.kind() == StandardWatchEventKinds.ENTRY_DELETE ) {
                   
                    System.out.printf( "\nEvent... kind=%s, count=%d, context=%s Context type=%s%n",
                                       watchEvent.kind()    ,
                                       watchEvent.count()   , 
                                       watchEvent.context() ,
                                      ((Path) watchEvent.context()).getClass() ) ;

                    //do something useful here
                    getTreeDirectoryAsJson( SI_PATH ) ;

                    //this is not a complete path
                    Path path = (Path) watchEvent.context()     ;
                    //need to get parent path
                    Path parentPath = keyPathMap.get(queuedKey) ;
                    //get complete path
                    path = parentPath.resolve( path )           ;
 
                    registerDir( path, watchService )           ;
               }
           }
           
           if(!queuedKey.reset() )           {
               keyPathMap.remove(queuedKey ) ;
           }
           
           if(keyPathMap.isEmpty() )         {
               break ;
           }
       }
   }
    
    private static void getTreeDirectoryAsJson( String maindirpath ) {
        
        File maindir = new File( maindirpath ) ; 
           
        if(maindir.exists() && maindir.isDirectory()) { 

            File arr[] = maindir.listFiles()   ; 
              
            jsonPath = "" ;
            System.out.println( "\nRebuild Directory Tree..." ) ;

            recursiveTraverse( arr , 0 , 0 )   ;
            
            jsonPath = jsonPath.trim()         ;
            
            if ( jsonPath.endsWith( ",")) {
               jsonPath = jsonPath.substring(0, jsonPath.length()-1 ) ;
             }
            
            jsonPath = "[ " + jsonPath + " ] " ;
       }  
    }
    
    static void recursiveTraverse( File[] arr,int index,int level )  { 
        
         // terminate condition 
         if(index == arr.length)  return; 
           
         // tabs for internal levels 
         for (int i = 0; i < level; i++) 
             System.out.print("\t") ; 
           
         // for files 
         if(arr[index].isFile())    {
             
            jsonPath +=
                     "{\n" +
                     " \"text\" : \"" + arr[index].getName()    + "\",\n" +
                     " \"state\" : { \"selected\" : false },\n" +
                     " \"icon\" : \"jstree-file\"\n"            +
                     "  } , "                                   ; 
         }
         // for sub-directories 
         else if(arr[index].isDirectory())  { 
             
             jsonPath += 
                     " { \"text\" : \"" + arr[index].getName() + "\",\n" +
                     " \"state\" : { \"opened\" : false },\n"  +
                     " \"children\" : ["     ;
             
             // recursion for sub-directories 
             recursiveTraverse(arr[index].listFiles(), 0, level + 1 ) ; 
             
             if ( jsonPath.endsWith( "} , ")) {
                jsonPath = jsonPath.replaceAll("\\} , $","} "       ) ;
             }
             
             jsonPath += " ] } , " ;
         } 
            
         // recursion for main directory 
         recursiveTraverse(arr,++index, level ) ; 
    } 
    
    /** Force Init ApplicationScoped at deployement time .
     * @param init
     * @throws java.lang.Exception */
    
    public void init( @Observes 
                      @Initialized(ApplicationScoped.class ) Object init ) throws Exception {
        
          System.out.println( "\n +++ Listen Files Changes  : " + SI_PATH   )    ;
        
          getTreeDirectoryAsJson( SI_PATH )                                      ;
          
          WatchService watchService = FileSystems.getDefault().newWatchService() ;
            
          registerDir(Paths.get( SI_PATH ) , watchService ) ;

           new Thread(() ->    {
               try {
                   startListening(watchService ) ;
               } catch ( Exception ex ) {
                   Logger.getLogger( Service.class.getName() )
                         .log( Level.SEVERE, null, ex )      ;
                   throw new RuntimeException (    ex )      ;
               }
           }).start() ;
           
           while (jsonPath == null || jsonPath.isEmpty() )   {
               TimeUnit.MILLISECONDS.sleep( 200 )            ;
           }
    }
}
