 class TreeView  extends HTMLElement {

   constructor() { 
      super()

      this._id            = "coby-textarea-id" 
      this._name          = "coby-textarea-name" 
      this.eventType      = "coby-textarea-event-type" 
      this.readonly       = "" 
      this._style         = ""       
      this.eventTypeClear = "clear"       

    }

   connectedCallback() { 

     if( this.hasAttribute('_id') ) {
         this._id = this.getAttribute('_id')
     }
     
     if( this.hasAttribute('_name') ) {
         this._name = this.getAttribute('_name')
     }

     if( this.hasAttribute('eventType') ) {
         this.eventType = this.getAttribute('eventType')
     }

      if( this.hasAttribute('eventTypeClear') ) {
          this.eventTypeClear = this.getAttribute('eventTypeClear')
     }
     
     if( this.hasAttribute('_style') ) {
         this._style += this.getAttribute('_style')
     }

     if( this.hasAttribute('readonly') ) {
	     this.readonly = "readonly" 
     }

     if( this.hasAttribute('placeholder') ) {
         this.placeholder = this.getAttribute('placeholder')
     }

     this.innerHTML =  `

                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />

                <style>
                
                        .styled {
                            border: 0;
                            line-height: 2;
                            /*padding: 0 20px;*/
                            width: 100%;
                            /*font-size: 1rem;*/
                            text-align: center;
                            color: #fff;
                            text-shadow: 1px 1px 1px #000;
                            border-radius: 10px;
                            background-image: linear-gradient(to top left,
                                                            rgba(0, 0, 0, .2),
                                                            rgba(0, 0, 0, .2) 30%,
                                                            rgba(0, 0, 0, 0));
                            box-shadow: inset 2px 2px 3px rgba(255, 255, 255, .6),
                                        inset -2px -2px 3px rgba(0, 0, 0, .6);
                        }

                        .red {
                           background-color: rgb(255, 4, 4);
                        }
                        
                        .green {
                           background-color: #61b961;
                        }
                        
                        .refresh-blue {
                           background-color: rgb(4, 167, 255);
                           height: 2.5em;
                        }
                        
                        
                        .styled:hover {
                            background-color: #698cf1;
                        }

                        .styled:active {
                            box-shadow: inset -2px -2px 3px rgba(255, 255, 255, .6),
                                        inset 2px 2px 3px rgba(0, 0, 0, .6);
                        }                         
                        
                        #documentReaderID.fullscreen {
                            z-index: 9999;
                            width: 100%;
                            height: 94.6%;
                            position: fixed;
                            top: 0;
                            left: 0;
                            margin-top: 0em;
                            margin-left: 0px;
                            box-shadow: inset 0 0 0 5em #53a7ea;
                            transition: all .05s linear;              
                        }

                        #documentReaderID {
                            width: 70.5% ; 
                            height: 91%; 
                            background: #ded3c4 ;
                            margin-left: 1em; 
                            margin-bottom: 0.5em;
                            border-radius: 8px;                            
                            float: right;
                            margin-right: 1em;  
                            margin-top: -3em; 
                        }
                        
                        a.maximize {
                            float: right;
                            margin-left:1em;
                            margin-right: 1em;
                        }
                        
                        a.download {
                            float: right;  
                        }
                        
                        a.yedEditor {
                            float: right;  
                        }

             
.scrollbar {
	margin-left: 22px;
	float: left;
	height: 300px;
	width: 60px;
	background: #F5F5F5;
	overflow-y: scroll;
	margin-bottom: 25px;
}

#wrapperTree::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 3px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
	border-radius: 10px;
}

#wrapperTree::-webkit-scrollbar
{
	width: 7px;
    height: 7px;
	background-color: #F5F5F5;
}

#wrapperTree::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	background-image: -webkit-gradient(linear,
									   left bottom,
									   left top,
									   color-stop(0.44, rgb(122,153,217)),
									   color-stop(0.72, rgb(73,125,189)),
									   color-stop(0.86, rgb(28,58,148)));
}

/* CSS */    
.jstree-node {
    font-size: 14pt;
}

.update-remote-file {
    background: url(components/base/tree-view/img/upload-to-coby.jpg) no-repeat;
    float: left;
    width: 2.3em;
    height: 2.3em;
    background-position: 10% 10%;
    background-repeat: no-repeat;
    background-size: cover;
    float: right;
}

.upload-file {
    background: url(components/base/tree-view/img/add-file.ico) no-repeat;
    float: left;
    width: 2em;
    height: 2em;
    background-position: 10% 10%;
    background-repeat: no-repeat;
    background-size: cover;
    float: right;
    margin-right: 0.2em
}
                </style>
                
                <body>

                <table style="width: 28.5%; padding-left: 1em; " >
                
                 <tbody style="width: 100%; margin-left: -0.5em; margin-top:1em;" >
                 
                    <tr style="display: inline-table; " >
                    
                        <td style="padding-right:1em ; width: 30%;">
                             
                             <button id="refresh-list-id"
                                    type="button" 
                                    class="favorite styled refresh-blue">
                                    <img class="icon" src="https://htmlacademy.ru/assets/icons/reload-6x-white.png" style=" height: 1em; width: 1em;" >&nbsp; - SI -
                            </button>
                        
                        </td>
                        
                                        <!--
                                        <td style="padding-right:1em ; width: 30%;">
                                        
                                            <button type="button"  
                                                    id="read-file-id"
                                                    class="favorite styled green" >
                                                    Read
                                            </button>
                                        
                                        </td> 
                                        -->
                         
                                        <!--
                                        <td>
                                            <button  id="Update-List-Id"
                                                    type="button"
                                                    class="favorite styled red" >
                                                    Update
                                            </button>
                                        </td>
                                        -->
                        
                                        <!--
                                        <td style="padding-left: 1em;" >
                                        
                                            <button type="button"  class="favorite styled refresh-blue" style="color: white;background: #6d6d69;" >Upload</button>

                                        </td>                        
                                        -->
                        
                         <td >
                              <div id="update-remote-file-id" class="update-remote-file" title="Update Remote File"></div>
                         </td> 
                         
                        <td style="width: 10%;">
                              <div id="upload-file-id" class="upload-file" title="Upload File" ></div>
                        </td> 
                        
                    </tr>
                
                 </tbody>
                 
                </table>   
               
                <div id="wrapperTree" style=" width:20%; height:54%; float:left; margin-left:0.8em; position:absolute; background: #0efe4d26; background: #1522101c; border-radius: 0.1em; border-left: none; " class="scrollbar" >
                    
                    <div id="treeviewDivId" style="padding:10px"></div>
                
                </div>
                    
                </div>
         
                <div id="documentReaderID" >
         
                     <div style="margin:5px" >

                        <a href="#" class="maximize"><img id="min_max" src="components/base/tree-view/img/max.png" alt="Maximize" width="20px" height="18px" /></a>
                        
                        <a href="#" class="download" title="Download File" ><img id="download" src="components/base/tree-view/img/save.ico" alt="Download" width="20px" height="20px" /></a>
                        
                        <a href="#" class="yedEditor" style="float:right; padding-right: 1em;" ><img src="components/base/tree-view/img/yed.png" alt="yed Web Editor" width="60px" height="24px" /></a>

                        <b><span id="selected-file-name" style="float:left; padding-left: 1em; padding-right: 1em; color:blue" >no_file_selected</span></b>

                     </div>
                     
                     <ace-js _id="ace-tree-view-editor" 
                            eventType="display-content-file-event" 
                            eventTypeClear="clear-content-file-event" 
                            style=" width: 100%; height: 100%; background: #0a275a; "></ace-js>
                </div>
               
                <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script> -->

                <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script> -->

                <script>
                
               
                </script>
                
                </body>

     ` ;     
    
     document.getElementById("update-remote-file-id").onclick = function() {

        window.dispatchEvent( new CustomEvent ( "update-remote-file"  ,  {  bubbles: true   , 
                                                                            composed: true  } ) )
     }

     document.getElementById("upload-file-id").onclick = function() {

        Swal.fire(  "Warning"                ,
                    ' Not Implemented Yet !' ,
                    'warning'                )
     }
      
     document.getElementById("download").onclick = function() {

        if ( localStorage.getItem('file-name') == null )      {

            Swal.fire(  "Please, reselect the File for thie Operation ! " ,
                        'Something went wrong ! '                         ,
                        'warning' )

            return 
        }

        var fileName    = localStorage.getItem('file-name') 
        var contentFile = ace.edit("ace-tree-view-editor" ).getValue()

        contentFile.substring( contentFile.lastIndexOf("/") + 1, contentFile.length )

        download( contentFile, fileName, "text/plain"     ) 

     }

     document.getElementById("refresh-list-id").onclick = function()  { 

         window.dispatchEvent(  new CustomEvent( "list-remote-files", { bubbles: true ,
                                                                        composed: true
                                } ) ) ;
     }
                                   
                                   
      $(document).on('click', '.yedEditor', function (event) { 

            window.dispatchEvent( new CustomEvent ( "store-state-in-localstorage"  ,  {  bubbles: true   , 
                                                                                         composed: true  } 
            ) )   

            if ( localStorage.getItem('file-name') == null ) {

                Swal.fire(  "Please, reselect the File before openning yed ! " ,
                    'Something went wrong ! '                                  ,
                    'warn' )

                return 
            }

            if( localStorage.getItem('file-name').includes(".graphml")) {
                
                var contentFile = ace.edit("ace-tree-view-editor" ).getValue()
                localStorage.setItem('coby_file' , contentFile    )

             } else {
                storeEmptyGraphInLocalStorage()
             }

            event.preventDefault()
            $('#modal-iframe').iziModal('open')

       } ) ;
     
       $('a.maximize').click(function(e) {
           
            $('#documentReaderID').toggleClass('fullscreen') ;
           
            var iconType =  $('#min_max').attr('src')

            if( iconType.endsWith('min.png') ) {    
                 $('#min_max').attr('src',"components/base/tree-view/img/max.png")
            } else {
                $('#min_max').attr('src',"components/base/tree-view/img/min.png")
            }
            
            ace.edit("ace-tree-view-editor" ).resize() 
            // ace.edit("ace-tree-view-editor" ).renderer.updateFull()
        });
  
       /*
       $('body').on('click', 'button.modal-close', {}, function() {
           alert("close")
            window.parent.$('#modal-iframe').iziModal('close', {
                transition: 'bounceOutDown' // Options
            });
        });
*/

       
       // var files = [] 
        
        /*
        document.getElementById ( "read-file-id" )
                .onclick = _ => {
        
                 window.dispatchEvent( new CustomEvent ( "clear-content-file-event"  ,  {  bubbles: true   , 
                                                                                           composed: true  ,
                                                                                           detail: {
                                                                                              message: "clear"
                                                                                         } } ) )  
                 
                 window.dispatchEvent( new CustomEvent ( "open-remote-file"  ,  {  bubbles: true   , 
                                                                                   composed: true  ,
                                                                                   detail: {
                                                                                        file: files.join(', ')
                                                                                   } } ) )                  
        }
        
        window.addEventListener ( "display-content-file" , function( event )  {

             window.dispatchEvent( new CustomEvent ( "display-content-file-event"  ,  {  bubbles: true   , 
                                                                                         composed: true  ,
                                                                                         detail: {
                                                                                            message: event.detail.content_file
                                                                                       } } ) )        
        }) ;
     
       */
          
        window.addEventListener ( "display-list-files" , function( event )  {

             var listFiles = JSON.parse( event.detail.list_files ) 
             
             console.log( listFiles )
             $('#treeviewDivId').jstree(true).settings.core.data = listFiles ;
             $('#treeviewDivId').jstree(true).refresh();
             //  $('#treeviewDivId').jstree(true).redraw(true);
                    
        }) ;
        
        /*
        function uiGetParents(loSelectedNode) {
            
            try {
                var lnLevel = loSelectedNode.node.parents.length;
                var lsSelectedID = loSelectedNode.node.id;
                var loParent = $("#" + lsSelectedID);
                var lsParents =  loSelectedNode.node.text + '/';
                for (var ln = 0; ln <= lnLevel -1 ; ln++) {
                    var loParent = loParent.parent().parent();
                    if (loParent.children()[1] != undefined) {
                        lsParents += loParent.children()[1].text + "/";
                    }
                }
                if (lsParents.length > 0) {
                    lsParents = lsParents.substring(0, lsParents.length - 1);
                }
                
                files.push(lsParents.split('/').reverse().join('/'));

            }
            catch (err) {
                alert('Error when generating Path : ' + err);
            }
        }
    */
        
        $(function () {
                          
             $("#modal-iframe").iziModal({
                    iframe: true,
                    iframeHeight: 830,
                    iframeURL: "./yed/web/editor/index.html#file=coby-localstorage",
                    openFullscreen: true,
                    width: 1300,
                    // padding: 20,
                    bodyOverflow:true,
                    restoreDefaultContent:false,
                    fullscreen:true,
                    navigateCaption:false,
                    transitionIn:"bounceInUp",
                    transitionOut:"bounceOutDown",
                    top: 1,
                    bottom: 3,
                    borderBottom: true,
                    padding: 0,
                    radius: 3,
                    title: 'Coby Yed Graph Editor',
                    headerColor: '#88A0B9',
                    background: '#263d53e6',
                    theme: 'green',  // light
                    
                    closeOnEscape: true,

                    onClosed: function(){

                        window.dispatchEvent( new CustomEvent ( "clear-localstorage"  ,  {  bubbles: true   , 
                                                                                            composed: true  } ) )  
                    }                        
               });
     
               /*
                  $('#treeviewDivId').on('select_node.jstree', function (e, data) {

                      files.splice(0, files.length )
                      var loMainSelected = data
                      uiGetParents(loMainSelected)
                  });                  
                */  
                  
                  $('#treeviewDivId').on("dblclick.jstree", function (eVENT , data) {
                      
                        pre_open_remote_file( $(this).jstree())
                  }); 
                  
                  
                  $('#treeviewDivId').on("keydown.tree", '.jstree-anchor', function (e) {
                   
                        switch (e.which) {
                            case 13: // enter
                                pre_open_remote_file( $(this).jstree())
                                break
                            default:                            
                                return true
                        }
                        e.stopImmediatePropagation()
                        return false
                  });
                  
                  window.addEventListener ( "disconnect" , function( event )  {

                    var EMPTY_DATA = [
                            
                        {
                           "text" : "no_file_selected"      ,
                           "state" : { "selected" : false } ,
                           "icon" : "jstree-file"
                        }
                    ] 

                    $('#treeviewDivId').jstree(true).settings.core.data = EMPTY_DATA ;
                    $('#treeviewDivId').jstree(true).refresh()                       ;

                    window.dispatchEvent( new CustomEvent ( "clear-content-file-event"  ,  {  bubbles: true   , 
                                                                                              composed: true  } ) )

                    document.getElementById("selected-file-name").textContent = "no_file_selected"
                    
                 }) ;
            
                  window.addEventListener ( "display-content-file" , function( event )  {

                    window.dispatchEvent( new CustomEvent ( "display-content-file-event"  ,  {  bubbles: true   , 
                                                                                                composed: true  ,
                                                                                                detail: {
                                                                                                    message: event.detail.content_file
                                                                                            } } ) )        
                 }) ;
            
                    
                  /*
                    
                   $('#treeviewDivId').on('changed.jstree', function (e, data) {

                        var i, j 
                        
                        files.splice(0, files.length )
                        
                        for(i = 0, j = data.selected.length; i < j; i++) {
                            files.push(data.instance.get_node(data.selected[i]).text);
                        }
                   })
                   */
  
                    // data format demo
                    $('#treeviewDivId').jstree({
                        'core' : {
                            'multiple': false,
                            "themes" : {
                               "stripes" : true 
                             } ,
                            'data' : [
                            
                                {
                                   "text" : "no_file_selected",
                                   "state" : { "selected" : false },
                                   "icon" : "jstree-file"
                                }
                            ]
                        }
                    } ) ;
                    
        } ) ;

   }
 
 }
 
 function pre_open_remote_file( tree ) {

    window.dispatchEvent( new CustomEvent ( "clear-content-file-event"  ,  {  bubbles: true   , 
        composed: true  ,
        detail: {
          message: "clear"

    } } ) )

    var node = tree.get_node( event.target ) 	
    var nodePath = tree.get_path(node).join("/")

    document.getElementById("selected-file-name").textContent = nodePath.substring( nodePath
                                                                        .lastIndexOf( "/") + 1 ,
                                                                                      nodePath.length ) 

    console.log("NodePath ( File to open ) : " + nodePath )

    // files.splice(0, files.length )
    // var loMainSelected = node
    // uiGetParents(loMainSelected)
    // alert(files.join(', '))

    window.dispatchEvent( new CustomEvent ( "open-remote-file"  ,  {  bubbles: true   , 
                                                                      composed: true  ,
                                                                      detail: {
                                                                        file: nodePath
                                                                   } } ) ) 

    // Do my action
 }

 function storeEmptyGraphInLocalStorage() {

    console.log(" Save Empty Graph in LocalStorage " )
    localStorage.setItem('coby_file',  emptyGraph    )
    localStorage.setItem('file-name',  "coby_graph"  )
 }

   emptyGraph = `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
 <!--Created by yFiles for HTML 2.3.0.2-->
 <graphml xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml.html/2.0/ygraphml.xsd " xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:demostyle="http://www.yworks.com/yFilesHTML/demos/FlatDemoStyle/1.0" xmlns:icon-style="http://www.yworks.com/yed-live/icon-style/1.0" xmlns:bpmn="http://www.yworks.com/xml/yfiles-bpmn/2.0" xmlns:demotablestyle="http://www.yworks.com/yFilesHTML/demos/FlatDemoTableStyle/1.0" xmlns:uml="http://www.yworks.com/yFilesHTML/demos/UMLDemoStyle/1.0" xmlns:compat="http://www.yworks.com/xml/yfiles-compat-arrows/1.0" xmlns:GraphvizNodeStyle="http://www.yworks.com/yFilesHTML/graphviz-node-style/1.0" xmlns:VuejsNodeStyle="http://www.yworks.com/demos/yfiles-vuejs-node-style/1.0" xmlns:explorer-style="http://www.yworks.com/data-explorer/1.0" xmlns:y="http://www.yworks.com/xml/yfiles-common/3.0" xmlns:x="http://www.yworks.com/xml/yfiles-common/markup/3.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
     <key id="d0" for="node" attr.type="int" attr.name="zOrder" y:attr.uri="http://www.yworks.com/xml/yfiles-z-order/1.0/zOrder"/>
     <key id="d1" for="node" attr.type="boolean" attr.name="Expanded" y:attr.uri="http://www.yworks.com/xml/yfiles-common/2.0/folding/Expanded">
         <default>true</default>
     </key>
     <key id="d2" for="node" attr.type="string" attr.name="url"/>
     <key id="d3" for="node" attr.type="string" attr.name="description"/>
     <key id="d4" for="node" attr.name="NodeLabels" y:attr.uri="http://www.yworks.com/xml/yfiles-common/2.0/NodeLabels"/>
     <key id="d5" for="node" attr.name="NodeGeometry" y:attr.uri="http://www.yworks.com/xml/yfiles-common/2.0/NodeGeometry"/>
     <key id="d6" for="all" attr.name="UserTags" y:attr.uri="http://www.yworks.com/xml/yfiles-common/2.0/UserTags"/>
     <key id="d7" for="node" attr.name="NodeStyle" y:attr.uri="http://www.yworks.com/xml/yfiles-common/2.0/NodeStyle"/>
     <key id="d8" for="node" attr.name="NodeViewState" y:attr.uri="http://www.yworks.com/xml/yfiles-common/2.0/folding/1.1/NodeViewState"/>
     <key id="d9" for="edge" attr.type="string" attr.name="url"/>
     <key id="d10" for="edge" attr.type="string" attr.name="description"/>
     <key id="d11" for="edge" attr.name="EdgeLabels" y:attr.uri="http://www.yworks.com/xml/yfiles-common/2.0/EdgeLabels"/>
     <key id="d12" for="edge" attr.name="EdgeGeometry" y:attr.uri="http://www.yworks.com/xml/yfiles-common/2.0/EdgeGeometry"/>
     <key id="d13" for="edge" attr.name="EdgeStyle" y:attr.uri="http://www.yworks.com/xml/yfiles-common/2.0/EdgeStyle"/>
     <key id="d14" for="edge" attr.name="EdgeViewState" y:attr.uri="http://www.yworks.com/xml/yfiles-common/2.0/folding/1.1/EdgeViewState"/>
     <key id="d15" for="port" attr.name="PortLabels" y:attr.uri="http://www.yworks.com/xml/yfiles-common/2.0/PortLabels"/>
     <key id="d16" for="port" attr.name="PortLocationParameter" y:attr.uri="http://www.yworks.com/xml/yfiles-common/2.0/PortLocationParameter">
         <default>
             <x:Static Member="y:FreeNodePortLocationModel.NodeCenterAnchored"/>
         </default>
     </key>
     <key id="d17" for="port" attr.name="PortStyle" y:attr.uri="http://www.yworks.com/xml/yfiles-common/2.0/PortStyle">
         <default>
             <x:Static Member="y:VoidPortStyle.Instance"/>
         </default>
     </key>
     <key id="d18" for="port" attr.name="PortViewState" y:attr.uri="http://www.yworks.com/xml/yfiles-common/2.0/folding/1.1/PortViewState"/>
     <key id="d19" attr.name="SharedData" y:attr.uri="http://www.yworks.com/xml/yfiles-common/2.0/SharedData"/>
     <data key="d19">
         <y:SharedData/>
     </data>
     <graph id="G" edgedefault="directed">
         <data key="d6">
             <y:Json>{"version":"2.0.0","theme":{"name":"light","version":"1.0.0"}}</y:Json>
         </data>
     </graph>
 </graphml>`

 

//download.js v3.0, by dandavis; 2008-2014. [CCBY2] see http://danml.com/download.html for tests/usage
// v1 landed a FF+Chrome compat way of downloading strings to local un-named files, upgraded to use a hidden frame and optional mime
// v2 added named files via a[download], msSaveBlob, IE (10+) support, and window.URL support for larger+faster saves than dataURLs
// v3 added dataURL and Blob Input, bind-toggle arity, and legacy dataURL fallback was improved with force-download mime and base64 support

// data can be a string, Blob, File, or dataURL

						 
function download( data, strFileName, strMimeType ) {
	
	var self = window, // this script is only for browsers anyway...
		u = "application/octet-stream", // this default mime also triggers iframe downloads
		m = strMimeType || u, 
		x = data,
		D = document,
		a = D.createElement("a"),
		z = function(a){return String(a);},
		
		
		B = self.Blob || self.MozBlob || self.WebKitBlob || z,
		BB = self.MSBlobBuilder || self.WebKitBlobBuilder || self.BlobBuilder,
		fn = strFileName || "download",
		blob, 
		b,
		ua,
		fr;

	if(String(this)==="true"){ //reverse arguments, allowing download.bind(true, "text/xml", "export.xml") to act as a callback
		x=[x, m] ;
		m=x[0]   ;
		x=x[1]   ; 
	}
	
	//go ahead and download dataURLs right away
	if(String(x).match(/^data\:[\w+\-]+\/[\w+\-]+[,;]/)){
		return navigator.msSaveBlob ?  // IE10 can't do a[download], only Blobs:
			navigator.msSaveBlob(d2b(x), fn) : 
			saver(x) ; // everyone else can save dataURLs un-processed
	}//end if dataURL passed?
	
	try{
	
		blob = x instanceof B ? 
			x : 
			new B([x], {type: m}) ;
	}catch(y){
		if(BB){
			b = new BB();
			b.append([x]);
			blob = b.getBlob(m); // the blob
		}
		
	}
	
	function d2b(u) {
		var p= u.split(/[:;,]/),
		t= p[1],
		dec= p[2] == "base64" ? atob : decodeURIComponent,
		bin= dec(p.pop()),
		mx= bin.length,
		i= 0,
		uia= new Uint8Array(mx);

		for(i;i<mx;++i) uia[i]= bin.charCodeAt(i);

		return new B([uia], {type: t});
	 }
	  
	function saver(url, winMode) {
		
		if ('download' in a) { //html5 A[download] 			
			a.href = url;
			a.setAttribute("download", fn) ;
			a.innerHTML = "downloading..." ;
			D.body.appendChild(a) ;
			setTimeout(function() {
				a.click() ;
				D.body.removeChild(a) ;
				if(winMode===true){setTimeout(function(){ self.URL.revokeObjectURL(a.href);}, 250 ) ; }
			}, 66);
			return true;
		}
		
		//do iframe dataURL download (old ch+FF):
		var f = D.createElement("iframe") ;
		D.body.appendChild(f) ;
		if(!winMode){ // force a mime that will download:
			url="data:"+url.replace(/^data:([\w\/\-\+]+)/, u) ;
		}
		 
	
		f.src = url;
		setTimeout(function(){ D.body.removeChild(f); }, 333);
		
	}//end saver 
		
	if (navigator.msSaveBlob) { // IE10+ : (has Blob, but not a[download] or URL)
		return navigator.msSaveBlob(blob, fn);
	} 	
	
	if(self.URL) { // simple fast and modern way using Blob and URL:
		saver(self.URL.createObjectURL(blob), true) ;
	}else {
		// handle non-Blob()+non-URL browsers:
		if(typeof blob === "string" || blob.constructor===z ) {
			try {
			      return saver( "data:" +  m   + ";base64,"  +  self.btoa(blob) ) ; 
			}catch(y){
			      return saver( "data:" +  m   + "," + encodeURIComponent(blob) ) ; 
			}
		}
		
		// Blob but not URL:
		fr=new FileReader()    ;
                
		fr.onload=function(e)  {
		    saver(this.result) ; 
		} ;
		
                fr.readAsDataURL(blob) ;
	}	
	return true;
} /* end download() */


 customElements.define("coby-treeview", TreeView )

