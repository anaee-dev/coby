#!/bin/bash

    # The Characters : $ @ ^ ` " are not authorized in this script 
 
    set -e 

    trap notify ERR

    function notify {
      echo "$(caller): ${BASH_COMMAND}"
    }

    export TERM=xterm-256color 

    SCRIPT_PATH="../scripts"

    CURRENT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    cd $CURRENT_PATH
    
    # COBY_OUTPUT_PATH="/var/doi-nfs-export"
    
    COBY_OUTPUT_PATH="/var/coby_export"  # c pichot - r.yahiaoui
    
    if [ ! -d $COBY_OUTPUT_PATH ] ; then   # r.yahiaoui
    
        if mkdir -p "$COBY_OUTPUT_PATH" ; then  # r.yahiaoui
           echo " COBY_OUTPUT_PATH : $COBY_OUTPUT_PATH Created !"
        else
           echo " COBY_OUTPUT_PATH [ $COBY_OUTPUT_PATH ] Creation Failure ! Permission Not Granted ! " 
        exit
        fi
    fi

    if [ -w "$COBY_OUTPUT_PATH" ] ;  then
        echo "Write permission is granted on $COBY_OUTPUT_PATH"
    else
        echo "Write permission is NOT granted on $COBY_OUTPUT_PATH"
        exit
    fi
    
    ################################################################
    # Arbo SI Configuration Ex
    ################################################################
    #
    #   + si_name
    #     - connection.txt
    #     + csv
    #       - semantic_si.csv
    #     + input
    #       + shared
    #         + Directory_01
    #           - mod.graphml
    #       + variables
    #         + variable_01
    #           - variable_01.graphml 
    #           - class.txt 
    #           - sparql.txt
    #         + variable_02
    #           - variable_02.graphml s
    #           - class.txt 
    #           - sparql.txt
    #
    ################################################################
    ################################################################
    
    ##############################################################################################
    ##############################################################################################

    # Example Test For this script :

    # QUERY=" SI =  SOERE OLA ; SOERE FORET & CLASS = meteo ; physico chimie & variable = http://www.anaee-france.fr/ontology/anaee-france_ontology#WaterPH & localSiteName = Léman ; TOTO & site = http://www.anaee-france.fr/ontology/anaee-france_ontology#GenevaLake & year = 2011_2017 & localVariableName = Azote ammoniacal ; pH ; azote nitreux & SELECT_VARS = variable ; localVariableName ; anaeeVariableName ; anaeeUnitName ; site ; infraName ;  year ; category ; unit ; anaeeSiteName "  
     
    # QUERY=" SI =  SOERE OLA ; SOERE FORET & CLASS = meteo ; physico chimie & localSiteName = Léman ; TOTO & year = 2011_2017 & localVariableName = Azote ammoniacal ; pH ; azote nitreux & SELECT_VARS = localVariableName ; anaeeVariableName ; anaeeUnitName; site ; infraName ;  year ; category ; unit ; anaeeSiteName " 
    
    # QUERY=" SI =  SOERE OLA ; SOERE FORET & CLASS = meteo ; physico chimie & localSiteName = Léman ; TOTO & year = 2011_2012 & localVariableName = Azote Ammoniacal & SELECT_VARS = anaeeUnitName; site ; infraName ;  year ; category ; unit ; anaeeSiteName " 
      
    # QUERY='SI = SOERE OLA ; SOERE FORET & CLASS = meteo ; physico chimie & site = Bourget ; Léman & SELECT_VARS = anaeeUnitName; site ; infraName ; year ; category ; unit ; anaeeSiteName '

    # QUERY=" SI =  SOERE OLA ; SOERE FORET & CLASS = meteo ; physico chimie & year = 1970_2012_close & SELECT_VARS = anaeeUnitName; site ; infraName ; year ; category ; unit ; anaeeSiteName 
       
   ##############################################################################################
   ##############################################################################################
       
    EXIT() {
     if [  $PPID = 0  ] ; then exit  ; fi
     # parent_script=`ps -ocommand= -p $PPID | awk -F/ '{print $NF}' | awk '{print $1}'`
     parent_script=`ps -o comm= $PPID`
     if [ $parent_script = "bash" -o  $parent_script = "fish"  ] ; then
         echo; echo -e " \e[90m exited by : $0 \e[39m " ; echo
         exit 2
     else
         if [ $parent_script != "java" ] ; then 
            echo ; echo -e " \e[90m exited by : $0 \e[39m " ; echo
            kill -9 `ps --pid $$ -oppid=`;
            exit 2
         fi
         echo " Coby Exited "
         exit 2
     fi
    }

    trim() {
       local trimmed="$1"
       # Strip leading space.
       trimmed="${trimmed## }"
       # Strip trailing space.
       trimmed="${trimmed%% }"
       echo "$trimmed"
    }
    
    if [ $# = 0 -o "$1" = "help"  -o "$1" = "h"  ]  ;  then
        echo 
        #cpichot        echo "  Two ARGS required :                       "
        echo "  Two (or 3) ARGS required :                                " #cpichot
        #cpichot        echo '   -> ( $1 : LOGIN ) & ( $2 : QUERY       ) '
        echo '   -> ( $1 : LOGIN ) & ( $2 : QUERY       ) & ( $3 : JOB )  ' #cpichot
        echo '   -> ( $1 : -i    ) & ( $2 : db=postgres ) '
        echo '   -> ( $1 : -i    ) & ( $2 : db=mysql    ) '
        EXIT
    fi    
 
    if [ $# = 1 ] &&  [ "$1" = "-i" ] ; then
        echo 
        echo "  Two ARGS required for installation :      "
        echo '   -> ( $1 : -i    ) & ( $2 : db=postgres ) '
        echo '   -> ( $1 : -i    ) & ( $2 : db=mysql    ) '
        EXIT
    fi   
  
    # FOR DB_X ( have to use $LD_LIBRARY_PATH and specify the path AT THE END )
    export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$CURRENT_PATH/.pg_shared_libs"
    echo "Exported LD_LIBRARY_PATH : $LD_LIBRARY_PATH "

    ##################################
    ###                            ###
    ### CONFIGURATION ################
    ###                            ###
    ##################################

    echo 
    echo " 00 =============================== 00 "
    echo " ** =============================== ** "
    echo " ||    ____   ____  ___  _     __   || "
    echo " ||   / ___| / __ \|  _ \ \   / /   || "
    echo " ||  | | D  | |  | | |_) \ \_/ /    || "
    echo " ||  | | O  | |  | |  _<  \   /     || "
    echo " ||  | |_I_ | |__| | |_) | | |      || "
    echo " ||   \____| \____/|____/  |_| v1.7 || "
    echo " ||                                 || "
    echo " ** =============================== ** "
    echo " 00 =============================== 00 "
   
    echo 
    echo " ## Request ####################### "
    echo
    
    if [ "$1" == "-i" ] ;  then 
    
       echo "    COBY INSTALATION ..." ; echo 
       
    else   
    
       LOGIN="$1"
       QUERY="$2"      
       JOB="$3" # cpichot  
       
       if [ -z "$JOB" ] ; then
            echo " JOB_ID Not Provided. Auto-Generate One "
            echo
            JOB="JOB_EXTRACTION_"
            JOB+=`date +%d_%m_%Y__%H_%M_%S`
       fi

       echo " LOGIN : $LOGIN "
       echo " QUERY : $QUERY "
       echo " JOB   : $JOB " #cpichot
       echo " DATE  : "`  date `
       # Escape some special Characters  
       QUERY=${QUERY//[\'\"\`\$\^\@]}  
       # Trim
       QUERY=` echo -n "$QUERY" | sed 's/^ *//;s/ *$// ' `
       echo 
       
    fi
   
    echo " ################################## " ; echo 
    
    STRICT_MODE_FILTER="FALSE" # "TRUE"

    RESERVED_PARAMETERS_WORDS="CLASS , SI, CSV, SELECT_VARS"

    DATE_AS_TOKEN=`date +%d_%m_%Y__%H_%M_%S`    

    #cpichot    OUTPUT_ROOT="$COBY_OUTPUT_PATH/$LOGIN/$DATE_AS_TOKEN" 
    
    OUTPUT_ROOT="$COBY_OUTPUT_PATH/$LOGIN/$JOB/$DATE_AS_TOKEN" #cpichot

    SI_PATH="SI" 
   
    if [[ ! -d  "SI" ]] ; then
        SI_PATH="../SI" ;
    fi

    if [ ! -d "$SI_PATH" ]; then 
      echo  
      echo -e "\e[93m ERROR ### \e[32m "
      echo -e "\e[93m  =>> Missning Modelization. No [$SI_PATH] Folder Provided ### \e[32m "
      EXIT
    fi
    
    FILE_BINDER="$SI_PATH/SI.txt"
        
    # Port 
    RW="7777"
    RO="8888"
    # Database 
    DATA_BASE="postgresql" # Alternative : "mysql"
    # Extensions :
    EXT_OBDA="obda"
    EXT_GRAPH="graphml"
    # Class File ( Discriminators )
    CLASS_FILE_NAME="class.txt"
    SPARQL_FILE_NAME="sparql.txt"
    ## CSV Config
    CSV_SEP=";"
    INTRA_CSV_SEP=" -intra_sep , -intra_sep '<' -intra_sep '>' " 
    COLUMNSTO_VALIDATE=" -column 0 -column 1 -column 2 -column 4 -column 6 -column 7 -column 8 -column 10 "
    INPUT_CSV_FILE_NAME="semantic_si.csv"
    OUTPUT_VALIDE_CSV_FILE_NAME="pipeline_si.csv"
    # Connection
    CONNEC_FILE_NAME="connection.txt"
        
    # yedGen 
    YED_GEN_ONTOP_VERSION="V4" # V1 / V3 # Default Version V4
    
    # Ontop ARGS
    ONTOP_QUERY="SELECT ?S ?P ?O { ?S ?P ?O } "
    ONTOP_TTL_FORMAT="ttl"
    ONTOP_PAGE_SIZE="200000"
    ONTOP_FLUSH_COUNT="500000"
    ONTOP_FRAGMENT="1000000"
    ONTOP_XMS="16g"
    ONTOP_XMX="16g"

    ONTOP_OUT_NOT_OUT_ONTOLOGY="not_out_ontology" # OR # out_ontology
    
    ########################################
    ########################################

    ONTOP_BATCH="batch" # disable : "" 
    #ONTOP_BATCH="" 

    # Merge works only if BATCH is ENABLE 
    ONTOP_MERGE="" # For Merge : "merge" 
    
    ONTOP_LOG_LEVEL="ERROR" # OFF , ALL , DEBUG , INFO , TRACE , WARN , ERROR  ; 
        
    ONTOP_DEBUG="" # ENABLE USING "ontop_debug"
    
    ONTOP_MUST_NOT_BE_EMPTY=" (1) , (2) "

    # IMPORTANT : IF CORESE_EXTRACT_ONLY_INFERENCE = TRUE
    # YOU HAVE TO PROVIDE A CORESE SPARQL QUERY ( CORESE_QUERY )  
    #  WITH kg:entailment
    CORESE_EXTRACT_ONLY_INFERENCE="false" # true

    ########################################
    ########################################

    # Corese ARGS
    
    CORESE_OUTPUT_EXTENSION="ttl"
    CORESE_IGNORE_LINE_BREAK="corese_ignore_line_break"
    
    # Note : be careful with --> CORESE_EXTRACT_ONLY_INFERENCE 
     CORESE_QUERY=${CORESE_QUERY:-"SELECT ?S ?P ?O { ?S ?P ?O . filter( !isBlank(?S) ) . filter( !isBlank(?O) )  } "}
    # CORESE_QUERY=${CORESE_QUERY:-"SELECT ?S ?P ?O WHERE { graph kg:entailment { ?S ?P ?O } FILTER NOT EXISTS { graph ?g { ?S ?P ?O } FILTER ( ?g != kg:entailment) } }"} 
    
    CORESE_PEEK=${CORESE_PEEK:-"-peek 5 "}
    CORESE_FRAGMENT=${CORESE_FRAGMENT:-"-f 1000000 "}    
    CORESE_FORMAT=${CORESE_FORMAT:-"-F ttl "}
    CORESE_FLUSH_COUNT=${CORESE_FLUSH_COUNT:-"-flushCount 250000"}    
    CORESE_XMS=${CORESE_XMS:-"16g"}
    CORESE_XMX=${CORESE_XMX:-"16g"}

    CORESE_EXTRACT_ONLY_INFERENCE=${CORESE_EXTRACT_ONLY_INFERENCE:-"false"} # Perf Problem if Enabled
   
    # dataRiv Args -> Includes Corese 
    DATARIV_LOG_LEVEL="INFO"
    DATARIV_PAGE_SIZE="100000"
    DATARIV_FRAGMENT="700000"
    DATARIV_FLUSH_COUNT="100000"
    DATARIV_PARALLELISM="0"                                            # 0 : All Available Processors. 1 : No Parallelism. > 1 Parallelism. 
    DATARIV_INDEX_COLUMNS="index_columns"

    DATARIV_ENTAILMENT="datariv_entailment"                            # "" To Disable
    DATARIV_ENTAILMENT_RULE=""                                         # OWL_RL_FULL , STD , OWL_RL , OWL_RL_LITE // Disabled if DATARIV_ENTAILMENT Disabled
    DATARIV_ENTAILMENT_SKOLEM="datariv_entailment_skolem"              # UriFy Blank Nodes                                     
    DATARIV_ONLY_ENTAILMENT=""                                         # "datariv_only_entailment" - ( Bad Perf if enabled ! )
    
    DATARIV_ENTAILMENT_PARALLELISM="2"
    DATARIV_ENTAILMENT_PEEK="2"
    DATARIV_ENTAILMENT_RM="datariv_entailement_rm"                     # Remove All generated TTLs after loading them and before Inferance
    DATARIV_ENTAILMENT_RM_ON_LOAD="datariv_entailment_rm_on_load"      # Remove generated TTLs on load
    DATARIV_ENTAILMENT_OUT_ONTOLOGY="datariv_entailment_out_ontology"  # datariv_entailment_out_ontology

    DATARIV_ENTAILMENT_DISABLE_CACHE_GRAPH=""                          # "datariv_entailment_disable_cache_graph" 

    DATARIV_ENTAILMENT_IGNORE_BLANK_NODES="datariv_entailment_ignore_blank_nodes" # Ignore blank nodes
    
    DATARIV_DEBUG="" # datariv_debug 
    
    DATARIV_XMS="16g"
    DATARIV_XMX="16g"

    ###############################
    ###############################
    
    # VAR_PART[0]='VAR_PART_SI'
    # VAR_PART[1]='VAR_PART_CSV'
    # VAR_PART[2]='VAR_PART_SQL'
    
    DELIMITER_AT='@'
    DELIMITER_DDOT=':'
    DELIMITER_DDOT_EQ=':='
    DELIMITER_DIEZ_COMMENT='#'
     
    ROOT_PATH="${CURRENT_PATH/}"
    PARENT_DIR="$(dirname "$ROOT_PATH")"
    
#     EXTRACT_ALL_SI() {
# 
#       FOLDER="$1"
#       SELECTED_SI=()
#       OIFS=$IFS
#       IFS=$'\n'
#        
#       for SI in ` find "$FOLDER" -mindepth 1 -maxdepth 1 -type d -not -name 'ontology' `; do
#         SELECTED_SI+=($(basename "$SI"))
#       done ; 
#       
#       IFS=$OIFS
#       echo "${SELECTED_SI[@]}" 
#     }
    
     EXTRACT_ALL_SI_FROM_FILE_BINDER() {         
        FILE="$1"
        OIFS=$IFS
        SELECTED_SI=() 
        while IFS="=" read -r key val
        do    
           _key=`echo ${key} | sed -e 's/=//' | sed -e 's/^[[:space:]]*//' | xargs `
           _val=`echo ${val} | sed -e 's/=//' | sed -e 's/^[[:space:]]*//' | xargs `
           if [ ! -z "$_val" ] ; then
              SELECTED_SI+=("$_val")       
           fi
        done < $FILE        
        IFS=$OIFS
    }    
    
    EXTRACT_ALL_CLASS()  {  
       FOLDER="$1"
       _IFS=$IFS  
       for class_file_path in ` find $FOLDER/*                 \
                                     -type f                   \
                                     -name 'class.txt'         \
                                     -not                      \
                                     -path "*/ontology/class.txt" ` ;  do
         LINE_ONE=$(head -n 1  $class_file_path )                     
         LINE_TWO=$(sed -n '2p'  $class_file_path )                        
         IFS=$'='
         read -ra KEY_VALUE <<< "$LINE_ONE"
         
         CURRENT_CLAZZ=`echo -e "${KEY_VALUE[1]}" | xargs` 
         
         if [[ "$CLASSS" != *"$CURRENT_CLAZZ ;"* ]]; then         
              CLASSS+="$CURRENT_CLAZZ ; "
         fi
         
       done
       IFS=$_IFS
       echo "$CLASSS"
    }
  
#       TO_ARRAY() { 
#           LINE=$1
#           DELIMITER=$2
#           ARRAY=()
#           OIFS=$IFS
#           IFS="$DELIMITER" read -r -a array <<< "$LINE"
#           for element in "${array[@]}" ; do
#           ARRAY+=("$element")
#           done
#           IFS=$OIFS
#       }
    
    GET_VALUE_FROM_FILE_BINDER() {         
        KEY="` echo "$1" | xargs `"
        FILE="$2"
        RESULT="NULL"
        OIFS=$IFS
        while IFS=$DELIMITER_DDOT read -r key val
        do    
        _key=`echo ${key} | sed -e 's/=//' | sed -e 's/^[[:space:]]*//' | xargs `
        _val=`echo ${val} | sed -e 's/=//' | sed -e 's/^[[:space:]]*//' | xargs `
        if [ "$KEY" == "$_key" ] ; then
            RESULT="$_val"
        fi 
        done < $FILE
        IFS=$OIFS
    }    
    
    EXTRACT_VALUES_FROM_LINE() { 
        OIFS=$IFS
        key=$1
        REQUEST=$2        
        IFS=$'\n'
        RESULT=`echo "$REQUEST" | grep -shoP '^.*'$key' *'$DELIMITER_DDOT_EQ'.*?'$DELIMITER_AT'|^.*'$key' *'$DELIMITER_DDOT_EQ'.*$' \
                                | sed -e 's/^.*'$key' *'$DELIMITER_DDOT_EQ'//' | sed -e 's/'$DELIMITER_AT'//'                       \
                                | sed 's/  */ /g' `
        IFS=$OIFS
    }    
    
    EXTRACT_EXISTING_SI_FROM_QUERY() { 
        VALUES=$1
        FILE=$2
        SELECTED_SI=()
        OIFS=$IFS
        IFS=';' read -r -a selectedSIs <<< "$VALUES"
        for selectedSI in "${selectedSIs[@]}"; do   
            si=`echo ${selectedSI} | sed -e 's/^[[:space:]]*//'`  
            GET_VALUE_FROM_FILE_BINDER "$si" "$FILE"
            if [ "$RESULT" != "NULL" ] ; then 
                SELECTED_SI+=($RESULT)
            elif [ "$STRICT_MODE_FILTER" == "TRUE" ] ; then 
                echo ; echo " SI [ $selectedSI ] Not Found."
                echo " STRICT_MODE_FILTER Enabled"
                echo " Process Will Exit !"
                echo
                EXIT
            fi
        done
        IFS=$OIFS
    }
                          

    CALL_COBY() { 
       
        ##################################################
        ##  INSTALLATION  ################################
        ##################################################
        
        if [ "$2" == "" -a "$1" == "-i" ] ; then 
            echo
            echo "  -> The arg [ -i ] is used only for installation. Cmd Ex : "$0" -i db=postgresql "
            EXIT
        
        elif [ "$#" -eq 2 -a "$1" == "-i" ] ; then 
        
            if [ "$2" != "db=postgresql" -a "$2" != "db=mysql" ] ;  then 
            echo
            echo "  -> Database must be : postgresql / mysql.  Cmd Ex : "$0" -i db=postgresql "
            EXIT
            fi
            
            s_db=$2
            db="${s_db/db=/''}"
            
            $SCRIPT_PATH/00_install_libs.sh db=$db
            
            EXIT 
        fi        
       
        SI="$1"
        CLASS_VALUES="$2"
        QUERY="$3"
        
        #####################################################
        #####################################################
        # COBY PIPELINE 
        #####################################################
        #####################################################

        ./data_extractor_process.sh  ip=localhost                                                     \
                                     namespace=soere                                                  \
                                     ro=$RO                                                           \
                                     rw=$RW                                                           \
                                     si=$SI                                                           \
                                     db=$DATA_BASE                                                    \
                                     ext_obda=$EXT_OBDA                                               \
                                     ext_graph=$EXT_GRAPH                                             \
                                     class_file_name=$CLASS_FILE_NAME                                 \
                                     sparql_file_name=$SPARQL_FILE_NAME                               \
                                     csv_file_name=$INPUT_CSV_FILE_NAME                               \
                                     valide_csv_file_name=$OUTPUT_VALIDE_CSV_FILE_NAME                \
                                     csv_sep=$CSV_SEP                                                 \
                                     intra_separators="$INTRA_CSV_SEP"                                \
                                     columns="$COLUMNSTO_VALIDATE"                                    \
                                     connec_file_name=$CONNEC_FILE_NAME                               \
                                                                                                      \
                                     yed_gen_ontop_version=$YED_GEN_ONTOP_VERSION                     \
                                     ontop_log_level=$ONTOP_LOG_LEVEL                                 \
                                                                                                      \
                                     ontop_xms="$ONTOP_XMS"                                           \
                                     ontop_xmx="$ONTOP_XMX"                                           \
                                     ontop_ttl_format="$ONTOP_TTL_FORMAT"                             \
                                     ontop_batch="$ONTOP_BATCH"                                       \
                                     ontop_page_sifze="$ONTOP_PAGE_SIZE"                              \
                                     ontop_flush_count="$ONTOP_FLUSH_COUNT"                           \
                                     ontop_merge="$ONTOP_MERGE"                                       \
                                     ontop_query="$ONTOP_QUERY"                                       \
                                     ontop_fragment="$ONTOP_FRAGMENT"                                 \
                                                                                                      \
                                     strict_mode_filter="$STRICT_MODE_FILTER"                         \
                                     output_root="$OUTPUT_ROOT"                                       \
                                     "$ONTOP_OUT_NOT_OUT_ONTOLOGY"                                    \
                                     "$ONTOP_DEBUG"                                                   \
                                                                                                      \
                                     corese_xms="$CORESE_XMS"                                         \
                                     corese_xmx="$CORESE_XMX"                                         \
                                     corese_query="$CORESE_QUERY"                                     \
                                     corese_peek="$CORESE_PEEK"                                       \
                                     corese_fragment="$CORESE_FRAGMENT"                               \
                                     corese_flush_count="$CORESE_FLUSH_COUNT"                         \
                                     corese_format="$CORESE_FORMAT"                                   \
                                     corese_output_extension="$CORESE_OUTPUT_EXTENSION"               \
                                     "$CORESE_IGNORE_LINE_BREAK"                                      \
                                     class_values="$CLASS_VALUES"                                     \
                                     query_user="$QUERY"                                              \
                                     reserved_paramaters_words="$RESERVED_PARAMETERS_WORDS"           \
                                     corese_extract_only_inference="$CORESE_EXTRACT_ONLY_INFERENCE"   \
                                     must_not_be_empty="$ONTOP_MUST_NOT_BE_EMPTY"                     \
                                     "ingore_case_sensitive_filtering_variables"                      \
                                                                                                      \
                                     datariv_page_size="$DATARIV_PAGE_SIZE"                           \
                                     datariv_fragment="$DATARIV_FRAGMENT"                             \
                                     datariv_flush_count="$DATARIV_FLUSH_COUNT"                       \
                                     datariv_xms="$DATARIV_XMS"                                       \
                                     datariv_xmx="$DATARIV_XMX"                                       \
                                     datariv_log_level="$DATARIV_LOG_LEVEL"                           \
                                     datariv_parallelism=$DATARIV_PARALLELISM                         \
                                     $DATARIV_ENTAILMENT                                              \
                                     datariv_entailment_parallelism=$DATARIV_ENTAILMENT_PARALLELISM   \
                                     datariv_entailment_peek=$DATARIV_ENTAILMENT_PEEK                 \
                                     datariv_entailment_rule="$DATARIV_ENTAILMENT_RULE"               \
                                     $DATARIV_ENTAILMENT_IGNORE_BLANK_NODES                           \
                                     $DATARIV_ENTAILMENT_SKOLEM                                       \
                                     $DATARIV_ENTAILMENT_DISABLE_CACHE_GRAPH                          \
                                     $DATARIV_ENTAILMENT_RM                                           \
                                     $DATARIV_ENTAILMENT_RM_ON_LOAD                                   \
                                     $DATARIV_ENTAILMENT_OUT_ONTOLOGY                                 \
                                     $DATARIV_INDEX_COLUMNS                                           \
                                     $DATARIV_ONLY_ENTAILMENT                                         \
                                     $DATARIV_DEBUG

    
    }
    
    #######################
    ## COBY ORCHESTRATOR ##
    #######################
    
    TOTAL_ARGS=0
    MINIMUM_REQUIRED_ARGS=2
    
    if [[ "$1" == "-i" ]] ; then 
       CALL_COBY "$1" "$2"   
       EXIT
    elif [[ -z "$QUERY"  ]] ; then
       echo
       echo " EMPTY QUERY => EMPTY RESULT "
       echo
       EXIT
    fi
    
    if [[ ! -d  "$OUTPUT_ROOT"  ]] ; then
      mkdir -p "$OUTPUT_ROOT" 
    else 
      rm -rf $OUTPUT_ROOT/*.*
      rm -rf $OUTPUT_ROOT/*
    fi

    OUTPUT_ROOT=` readlink -f "$OUTPUT_ROOT" `

    QUERY=${QUERY//&/ $DELIMITER_AT }
    QUERY=${QUERY//=/ $DELIMITER_DDOT_EQ }
    
    echo
    
    EXTRACT_VALUES_FROM_LINE "SI" "$QUERY"  
    
    QUERY_SI="$RESULT"
    
    EXTRACT_EXISTING_SI_FROM_QUERY "$QUERY_SI" "$FILE_BINDER"
    
    if [ ! -z "$QUERY_SI" ] && [ -z "${SELECTED_SI[@]}" ]; then
       echo " NO S.I FOUND IN THE LIST : $QUERY_SI "
       EXIT
    fi

    if [ -z "$QUERY_SI" ]; then

      echo 
      echo " --> Search and Apply all SI "
      echo
      
      # EXTRACT_ALL_SI "../SI"
      EXTRACT_ALL_SI_FROM_FILE_BINDER "$FILE_BINDER"

      if [[ -z "${SELECTED_SI[@]}" ]] ; then
          echo " No S.I detected ! Path -> [${SELECTED_SI[@]}] "
          echo " The process Will EXIT "
          EXIT   
      else 
         TOTAL_ARGS=$[$TOTAL_ARGS +1]
      fi 
      
    else 

       TOTAL_ARGS=$[$TOTAL_ARGS +1]
    fi    
    
    EXTRACT_VALUES_FROM_LINE "CLASS" "$QUERY"  
    QUEY_CLASS_VALUES="$(trim "${RESULT}" )"
   
    CLASS_VALUES=$( EXTRACT_ALL_CLASS '../SI' )
    
    if [[ -z "$QUEY_CLASS_VALUES" ]] ; then
       
       echo " --> Search and Apply all Class values "
       echo
       
       if [[ -z "$CLASS_VALUES" ]] ; then 
           echo " No CLASS FOUND !      "
           echo " The process will EXIT "
           EXIT
       fi

    else        
       
       IFS=';' read -ra QCs <<< "$QUEY_CLASS_VALUES"
       
       ONLY_EXISTED_CLASS_VALUES=""
       
       for qc in "${QCs[@]}"; do
         
         tqc="$(trim "${qc}" )"
         
         if [[ "$CLASS_VALUES" != *"$tqc ;"* ]]; then         
                          
            if [[ "$STRICT_MODE_FILTER" == "TRUE" ]]; then   
            
               echo " CLASS K_O : $tqc "
               echo ; echo " STRICT_MODE_FILTER ENABLED - Please Provide an existing CLASS "
               echo ; echo " Process Will EXIT " 
               EXIT

            else 
         
               echo " CLASS K_O ( Will Be Ignored ) : $tqc "
               
            fi
            
         else 
             ONLY_EXISTED_CLASS_VALUES+="$tqc ; "
         fi
         
       done ;  unset IFS;

       if [ -z "$ONLY_EXISTED_CLASS_VALUES" ]; then   
           echo ; echo " No CLASS Found In the list : [ $QUEY_CLASS_VALUES ] ! Process Will Exit ! "
           EXIT
       fi
       
       QUEY_CLASS_VALUES="$ONLY_EXISTED_CLASS_VALUES"        
       TOTAL_ARGS=$[$TOTAL_ARGS +1]
    fi

    EXTRACT_VALUES_FROM_LINE "variableIRI" "$QUERY"  

    VARIABLES="$RESULT"
    if [[ ! -z "$VARIABLES" ]] ; then
       TOTAL_ARGS=$[$TOTAL_ARGS +1]  
    fi
    
    EXTRACT_VALUES_FROM_LINE "categoryIRI" "$QUERY"  
    CATEGORIES="$RESULT"
    if [[ ! -z "$CATEGORIES" ]] ; then
       TOTAL_ARGS=$[$TOTAL_ARGS +1]  
    fi
    
    
    if [ $TOTAL_ARGS -lt $MINIMUM_REQUIRED_ARGS ] ; then
         echo
         echo " ==================================================================                      "
         echo
         echo " MINIMUM_REQUIRED_ARGS not reached :                                                     "
         echo  
         echo "  -> MINIMUM Required ARGS : $MINIMUM_REQUIRED_ARGS                                      "
         echo "  -> TOTAL Recieved ARGS   : $TOTAL_ARGS                                                 "
         echo "  -> Required ARGS ( $MINIMUM_REQUIRED_ARGS )   : SI , categoryIRI , variableIRI, CLASS  "
         echo
         echo " ==================================================================                      "
         echo 
         EXIT
    fi
        
    $SCRIPT_PATH/utils/check_commands.sh java curl psql-mysql mvn awk gawk fuser lsof    


    for si in "${SELECTED_SI[@]}" ; do

       tput setaf 2
       echo
       echo -e " ##################################### "
       echo -e " ########  Treat SI  =>  [ $si ]       "
       echo -e " ##################################### "
       echo
            
       if [ -z "$QUEY_CLASS_VALUES" ]; then
       
            echo -e " ## CLASS : [ $CLASS_VALUES] "
            echo
            tput setaf 7
            sleep 0.1      
       
            CALL_COBY "$si" "$CLASS_VALUES" "$QUERY"
       else 
           
            echo -e " ## CLASS : [ $QUEY_CLASS_VALUES] "
            echo
            tput setaf 7
            sleep 0.1  

            CALL_COBY "$si" "$QUEY_CLASS_VALUES" "$QUERY"
       fi

    done
    
